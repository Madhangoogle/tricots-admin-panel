import { actionTypes } from "../actions/headers";

export const initialState = {

    fetchingHeaders : false,
    fetchHeaderSuccessful : false,
    headers : [],

    fetchingHeaderDetails : false,
    fetchHeaderDetailsSuccessful : false,
    headerDetails: null,

    creatingHeader : false,
    createHeaderSuccessful : false,
    header : null,

    updatingHeader : false,
    updateHeaderSuccessful : false,
    
    deletingHeader : false,
    deleteHeaderSuccessful : false,

	error: null,
};

function headerReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_HEADERS_REQUEST: return { ...state, ...{ fetchingHeaders: true } };
		case actionTypes.GET_HEADERS_FAILURE: return { ...state, ...{ fetchingHeaders: false, error: action.error }};
		case actionTypes.GET_HEADERS_SUCCESS: return { ...state, ...{ fetchingHeaders: false, fetchHeaderSuccessful:true,  headers: action.headers }};

        case actionTypes.GET_HEADER_DETAILS_REQUEST: return { ...state, ...{ fetchingHeaderDetails: true } };
		case actionTypes.GET_HEADER_DETAILS_FAILURE: return { ...state, ...{ fetchingHeaderDetails: false, error: action.error }};
		case actionTypes.GET_HEADER_DETAILS_SUCCESS: return { ...state, ...{ fetchingHeaderDetails: false, fetchHeaderDetailsSuccessful:true,  headerDetails: action.headerDetails }};

        case actionTypes.CREATE_HEADER_REQUEST: return { ...state, ...{ creatingHeader: true } };
		case actionTypes.CREATE_HEADER_FAILURE: return { ...state, ...{ creatingHeader: false, error: action.error }};
		case actionTypes.CREATE_HEADER_SUCCESS: return { ...state, ...{ creatingHeader: false, createHeaderSuccessful:true,  header: action.header }};

        case actionTypes.UPDATE_HEADER_REQUEST: return { ...state, ...{ updatingHeader: true } };
		case actionTypes.UPDATE_HEADER_FAILURE: return { ...state, ...{ updatingHeader: false, error: action.error }};
        case actionTypes.UPDATE_HEADER_SUCCESS: return { ...state, ...{ updatingHeader: false, updateHeaderSuccessful:true,  header: action.header }};
        
        case actionTypes.DELETE_HEADER_REQUEST: return { ...state, ...{ deletingHeader: true } };
		case actionTypes.DELETE_HEADER_FAILURE: return { ...state, ...{ deletingHeader: false, error: action.error }};
        case actionTypes.DELETE_HEADER_SUCCESS: return { ...state, ...{ deletingHeader: false, deleteHeaderSuccessful:true,  header: null }};
        
		default:return state;
	}
}

export default headerReducer;
