import { actionTypes } from "../actions/tasks";

export const initialState = {

    fetchingTasks : false,
    fetchTaskSuccessful : false,
    tasks : [],

    fetchingTaskDetails : false,
    fetchTaskDetailsSuccessful : false,
    taskDetails: null,

    creatingTask : false,
    createTaskSuccessful : false,
    task : null,

    updatingTask : false,
    updateTaskSuccessful : false,
    
    deletingTask : false,
    deleteTaskSuccessful : false,

	error: null,
};

function taskReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_TASKS_REQUEST: return { ...state, ...{ fetchingTasks: true } };
		case actionTypes.GET_TASKS_FAILURE: return { ...state, ...{ fetchingTasks: false, error: action.error }};
		case actionTypes.GET_TASKS_SUCCESS: return { ...state, ...{ fetchingTasks: false, fetchTaskSuccessful:true,  tasks: action.tasks }};

        case actionTypes.GET_TASK_DETAILS_REQUEST: return { ...state, ...{ fetchingTaskDetails: true } };
		case actionTypes.GET_TASK_DETAILS_FAILURE: return { ...state, ...{ fetchingTaskDetails: false, error: action.error }};
		case actionTypes.GET_TASK_DETAILS_SUCCESS: return { ...state, ...{ fetchingTaskDetails: false, fetchTaskDetailsSuccessful:true,  taskDetails: action.taskDetails }};

        case actionTypes.CREATE_TASK_REQUEST: return { ...state, ...{ creatingTask: true } };
		case actionTypes.CREATE_TASK_FAILURE: return { ...state, ...{ creatingTask: false, error: action.error }};
		case actionTypes.CREATE_TASK_SUCCESS: return { ...state, ...{ creatingTask: false, createTaskSuccessful:true,  taskDetails: action.taskDetails }};

        case actionTypes.UPDATE_TASK_REQUEST: return { ...state, ...{ updatingTask: true } };
		case actionTypes.UPDATE_TASK_FAILURE: return { ...state, ...{ updatingTask: false, error: action.error }};
        case actionTypes.UPDATE_TASK_SUCCESS: return { ...state, ...{ updatingTask: false, updateTaskSuccessful:true,  taskDetails: action.taskDetails }};
        
        case actionTypes.DELETE_TASK_REQUEST: return { ...state, ...{ deletingTask: true } };
		case actionTypes.DELETE_TASK_FAILURE: return { ...state, ...{ deletingTask: false, error: action.error }};
        case actionTypes.DELETE_TASK_SUCCESS: return { ...state, ...{ deletingTask: false, deleteTaskSuccessful:true,  taskDetails: null }};
        
		default:return state;
	}
}

export default taskReducer;
