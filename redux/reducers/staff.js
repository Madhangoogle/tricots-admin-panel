import { actionTypes } from "../actions/staffs";

export const initialState = {

    fetchingStaffs : false,
    fetchStaffSuccessful : false,
    staffs : [],

    fetchingStaffHistory : false,
    fetchStaffHistorySuccessfulll : false,
    staffHistory : [],

    fetchingStaffHistoryDetail : false,
    fetchStaffHistoryDetailSuccessfulll : false,
    staffHistoryDetail : [],

    creatingStaff : false,
    createStaffSuccessful : false,
    staff : null,

    fetchingStaffDetail:false,
    fetchStaffDetailSuccessfull:false,
    staffDetail: null,


    updatingStaff : false,
    updateStaffSuccessful : false,
    
    deletingStaff : false,
    deleteStaffSuccessful : false,

	error: null,
};

function staffReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_STAFFS_REQUEST: return { ...state, ...{ fetchingStaffs: true } };
		case actionTypes.GET_STAFFS_FAILURE: return { ...state, ...{ fetchingStaffs: false, error: action.error }};
		case actionTypes.GET_STAFFS_SUCCESS: return { ...state, ...{ fetchingStaffs: false, fetchStaffSuccessful:true,  staffs: action.Staffs }};

        case actionTypes.GET_STAFF_HISTORY_REQUEST: return {...state,...{fetchingStaffHistory: true}};
        case actionTypes.GET_STAFF_HISTORY_FAILURE: return {...state,...{fetchingStaffHistory: false,error:action.error}};
        case actionTypes.GET_STAFF_HISTORY_SUCCESS: return {...state,...{fetchingStaffHistory :false, fetchStaffHistorySuccessfulll:true, staffHistory:action.StaffHistory}};

        case actionTypes.GET_STAFF_HISTORY_DETAIL_REQUEST: return {...state,...{fetchingStaffHistoryDetail: true}};
        case actionTypes.GET_STAFF_HISTORY_DETAIL_FAILURE: return {...state,...{fetchingStaffHistoryDetail: false,error:action.error}};
        case actionTypes.GET_STAFF_HISTORY_DETAIL_SUCCESS: return {...state,...{fetchingStaffHistoryDetail :false, fetchStaffHistoryDetailSuccessfulll:true, staffHistoryDetail:action.historyDetail}};

        case actionTypes.CREATE_STAFF_REQUEST: return { ...state, ...{ creatingStaff: true } };
		case actionTypes.CREATE_STAFF_FAILURE: return { ...state, ...{ creatingStaff: false, error: action.error }};
		case actionTypes.CREATE_STAFF_SUCCESS: return { ...state, ...{ creatingStaff: false, createStaffSuccessful:true,  staff: action.staff }};

        case actionTypes.GET_STAFF_DETAILS_REQUEST: return { ...state, ...{ fetchingStaffDetail: true } };
		case actionTypes.GET_STAFF_DETAILS_FAILURE: return { ...state, ...{ fetchingStaffDetail: false, error: action.error }};
		case actionTypes.GET_STAFF_DETAILS_SUCCESS: return { ...state, ...{ fetchingStaffDetail: false, fetchStaffDetailSuccessfull:true,  staffDetail: action.staff }};
        
        case actionTypes.UPDATE_STAFF_REQUEST: return { ...state, ...{ updatingStaff: true } };
		case actionTypes.UPDATE_STAFF_FAILURE: return { ...state, ...{ updatingStaff: false, error: action.error }};
        case actionTypes.UPDATE_STAFF_SUCCESS: return { ...state, ...{ updatingStaff: false, updateStaffSuccessful:true,  staff: action.staff }};
        
        case actionTypes.DELETE_STAFF_REQUEST: return { ...state, ...{ deletingStaff: true } };
		case actionTypes.DELETE_STAFF_FAILURE: return { ...state, ...{ deletingStaff: false, error: action.error }};
        case actionTypes.DELETE_STAFF_SUCCESS: return { ...state, ...{ deletingStaff: false, deleteStaffSuccessful:true,  staff: null }};

		default:return state;
	}
}

export default staffReducer;
