import { actionTypes } from "../actions/charts";

export const initialState = {

    fetchingCharts : false,
    fetchChartSuccessful : false,
    charts : [],

    fetchingChartDetails : false,
    fetchChartDetailsSuccessful : false,
    chartDetails: null,

    creatingChart : false,
    createChartSuccessful : false,
    chart : null,

    updatingChart : false,
    updateChartSuccessful : false,
    
    deletingChart : false,
    deleteChartSuccessful : false,

	error: null,
};

function chartReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_CHARTS_REQUEST: return { ...state, ...{ fetchingCharts: true } };
		case actionTypes.GET_CHARTS_FAILURE: return { ...state, ...{ fetchingCharts: false, error: action.error }};
		case actionTypes.GET_CHARTS_SUCCESS: return { ...state, ...{ fetchingCharts: false, fetchChartSuccessful:true,  charts: action.charts }};

        case actionTypes.GET_CHART_DETAILS_REQUEST: return { ...state, ...{ fetchingChartDetails: true } };
		case actionTypes.GET_CHART_DETAILS_FAILURE: return { ...state, ...{ fetchingChartDetails: false, error: action.error }};
		case actionTypes.GET_CHART_DETAILS_SUCCESS: return { ...state, ...{ fetchingChartDetails: false, fetchChartDetailsSuccessful:true,  chartDetails: action.chartDetails }};

        case actionTypes.CREATE_CHART_REQUEST: return { ...state, ...{ creatingChart: true } };
		case actionTypes.CREATE_CHART_FAILURE: return { ...state, ...{ creatingChart: false, error: action.error }};
		case actionTypes.CREATE_CHART_SUCCESS: return { ...state, ...{ creatingChart: false, createChartSuccessful:true,  chart: action.chart }};

        case actionTypes.UPDATE_CHART_REQUEST: return { ...state, ...{ updatingChart: true } };
		case actionTypes.UPDATE_CHART_FAILURE: return { ...state, ...{ updatingChart: false, error: action.error }};
        case actionTypes.UPDATE_CHART_SUCCESS: return { ...state, ...{ updatingChart: false, updateChartSuccessful:true,  chart: action.chart }};
        
        case actionTypes.DELETE_CHART_REQUEST: return { ...state, ...{ deletingChart: true } };
		case actionTypes.DELETE_CHART_FAILURE: return { ...state, ...{ deletingChart: false, error: action.error }};
        case actionTypes.DELETE_CHART_SUCCESS: return { ...state, ...{ deletingChart: false, deleteChartSuccessful:true,  chart: null }};
        
		default:return state;
	}
}

export default chartReducer;
