import { actionTypes } from "../actions/orders";

export const initialState = {

    fetchingOrders : false,
    fetchOrderSuccessful : false,
    orders : [],

    fetchingOrderDetails : false,
    fetchOrderDetailsSuccessful : false,
    orderDetails: null,

    creatingOrder : false,
    createOrderSuccessful : false,
    order : null,

    updatingOrder : false,
    updateOrderSuccessful : false,
    
    deletingOrder : false,
    deleteOrderSuccessful : false,

	error: null,
};

function orderReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_ORDERS_REQUEST: return { ...state, ...{ fetchingOrders: true } };
		case actionTypes.GET_ORDERS_FAILURE: return { ...state, ...{ fetchingOrders: false, error: action.error }};
		case actionTypes.GET_ORDERS_SUCCESS: return { ...state, ...{ fetchingOrders: false, fetchOrderSuccessful:true,  orders: action.orders }};

        case actionTypes.GET_ORDER_DETAILS_REQUEST: return { ...state, ...{ fetchingOrderDetails: true } };
		case actionTypes.GET_ORDER_DETAILS_FAILURE: return { ...state, ...{ fetchingOrderDetails: false, error: action.error }};
		case actionTypes.GET_ORDER_DETAILS_SUCCESS: return { ...state, ...{ fetchingOrderDetails: false, fetchOrderDetailsSuccessful:true,  orderDetails: action.orderDetails }};

        case actionTypes.CREATE_ORDER_REQUEST: return { ...state, ...{ creatingOrder: true } };
		case actionTypes.CREATE_ORDER_FAILURE: return { ...state, ...{ creatingOrder: false, error: action.error }};
		case actionTypes.CREATE_ORDER_SUCCESS: return { ...state, ...{ creatingOrder: false, createOrderSuccessful:true,  order: action.order }};

        case actionTypes.UPDATE_ORDER_REQUEST: return { ...state, ...{ updatingOrder: true } };
		case actionTypes.UPDATE_ORDER_FAILURE: return { ...state, ...{ updatingOrder: false, error: action.error }};
        case actionTypes.UPDATE_ORDER_SUCCESS: return { ...state, ...{ updatingOrder: false, updateOrderSuccessful:true,  order: action.order }};
        
        case actionTypes.DELETE_ORDER_REQUEST: return { ...state, ...{ deletingOrder: true } };
		case actionTypes.DELETE_ORDER_FAILURE: return { ...state, ...{ deletingOrder: false, error: action.error }};
        case actionTypes.DELETE_ORDER_SUCCESS: return { ...state, ...{ deletingOrder: false, deleteOrderSuccessful:true,  order: null }};
        
		default:return state;
	}
}

export default orderReducer;
