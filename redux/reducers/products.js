import { actionTypes } from "../actions/products";

export const initialState = {

    fetchingProducts : false,
    fetchProductSuccessful : false,
    products : [],

    fetchingProductDetails : false,
    fetchProductDetailsSuccessful : false,
    productDetails: null,

    creatingProduct : false,
    createProductSuccessful : false,
    product : null,

    updatingProduct : false,
    updateProductSuccessful : false,
    
    deletingProduct : false,
    deleteProductSuccessful : false,

	error: null,
};

function productReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_PRODUCTS_REQUEST: return { ...state, ...{ fetchingProducts: true } };
		case actionTypes.GET_PRODUCTS_FAILURE: return { ...state, ...{ fetchingProducts: false, error: action.error }};
		case actionTypes.GET_PRODUCTS_SUCCESS: return { ...state, ...{ fetchingProducts: false, fetchProductSuccessful:true,  products: action.products }};

        case actionTypes.GET_PRODUCT_DETAILS_REQUEST: return { ...state, ...{ fetchingProductDetails: true } };
		case actionTypes.GET_PRODUCT_DETAILS_FAILURE: return { ...state, ...{ fetchingProductDetails: false, error: action.error }};
		case actionTypes.GET_PRODUCT_DETAILS_SUCCESS: return { ...state, ...{ fetchingProductDetails: false, fetchProductDetailsSuccessful:true,  productDetails: action.productDetails }};

        case actionTypes.CREATE_PRODUCT_REQUEST: return { ...state, ...{ creatingProduct: true } };
		case actionTypes.CREATE_PRODUCT_FAILURE: return { ...state, ...{ creatingProduct: false, error: action.error }};
		case actionTypes.CREATE_PRODUCT_SUCCESS: return { ...state, ...{ creatingProduct: false, createProductSuccessful:true,  product: action.product }};

        case actionTypes.UPDATE_PRODUCT_REQUEST: return { ...state, ...{ updatingProduct: true } };
		case actionTypes.UPDATE_PRODUCT_FAILURE: return { ...state, ...{ updatingProduct: false, error: action.error }};
        case actionTypes.UPDATE_PRODUCT_SUCCESS: return { ...state, ...{ updatingProduct: false, updateProductSuccessful:true,  product: action.product }};
        
        case actionTypes.DELETE_PRODUCT_REQUEST: return { ...state, ...{ deletingProduct: true } };
		case actionTypes.DELETE_PRODUCT_FAILURE: return { ...state, ...{ deletingProduct: false, error: action.error }};
        case actionTypes.DELETE_PRODUCT_SUCCESS: return { ...state, ...{ deletingProduct: false, deleteProductSuccessful:true,  product: null }};
        
		default:return state;
	}
}

export default productReducer;
