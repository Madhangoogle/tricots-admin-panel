import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { createMemoryHistory } from 'history'

import authReducer from './auth'
import customersReducer from './customers'
import locationsReducer from './locations'
import staffReducer from './staff'
import wasteBinReducer from './wastebins'
import taskReducer from './tasks'
import dashboardReducer from './dashboard'
import bannerReducer from './banners'
import categoryReducer from './categories'
import chartReducer from './charts'
import collectionReducer from './collections'
import feedbackReducer from './feedbacks'
import productReducer from './products'
import comboReducer from './combos'
import headerReducer from './headers'
import orderReducer from './orders'
import profileReducer from './profiles'
import PromocodeReducer from './promocodes'
import RatingReducer from './ratings'
import SubscribeReducer from './subscribes'
import { actionTypes } from '../actions/auth'

const history = createMemoryHistory()
const appReducer = combineReducers({
	router: connectRouter(history),
	auth: authReducer,
	customers: customersReducer,
	locations: locationsReducer,
	staffs: staffReducer,
	wastebins: wasteBinReducer,
	tasks: taskReducer,
	dashboard: dashboardReducer,
	banners: bannerReducer,
	categories: categoryReducer,
	charts: chartReducer,
	collections: collectionReducer,
	feedbacks: feedbackReducer,
	products: productReducer,
	combos: comboReducer,
	headers: headerReducer,
	orders: orderReducer,
	profiles: profileReducer,
	promocodes: PromocodeReducer,
	ratings: RatingReducer,
	subscribes: SubscribeReducer,
})

const rootReducer = (state, action) => {
	if (action.type === actionTypes.LOGOUT_SUCCESS) {
		return appReducer(undefined, action)
	}
	return appReducer(state, action)
}

export default rootReducer
