import { actionTypes } from "../actions/combos";

export const initialState = {

    fetchingCombos : false,
    fetchComboSuccessful : false,
    combos : [],

    fetchingComboDetails : false,
    fetchComboDetailsSuccessful : false,
    comboDetails: null,

    creatingCombo : false,
    createComboSuccessful : false,
    combo : null,

    updatingCombo : false,
    updateComboSuccessful : false,
    
    deletingCombo : false,
    deleteComboSuccessful : false,

	error: null,
};

function comboReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_COMBOS_REQUEST: return { ...state, ...{ fetchingCombos: true } };
		case actionTypes.GET_COMBOS_FAILURE: return { ...state, ...{ fetchingCombos: false, error: action.error }};
		case actionTypes.GET_COMBOS_SUCCESS: return { ...state, ...{ fetchingCombos: false, fetchComboSuccessful:true,  combos: action.combos }};

        case actionTypes.GET_COMBO_DETAILS_REQUEST: return { ...state, ...{ fetchingComboDetails: true } };
		case actionTypes.GET_COMBO_DETAILS_FAILURE: return { ...state, ...{ fetchingComboDetails: false, error: action.error }};
		case actionTypes.GET_COMBO_DETAILS_SUCCESS: return { ...state, ...{ fetchingComboDetails: false, fetchComboDetailsSuccessful:true,  comboDetails: action.comboDetails }};

        case actionTypes.CREATE_COMBO_REQUEST: return { ...state, ...{ creatingCombo: true } };
		case actionTypes.CREATE_COMBO_FAILURE: return { ...state, ...{ creatingCombo: false, error: action.error }};
		case actionTypes.CREATE_COMBO_SUCCESS: return { ...state, ...{ creatingCombo: false, createComboSuccessful:true,  combo: action.combo }};

        case actionTypes.UPDATE_COMBO_REQUEST: return { ...state, ...{ updatingCombo: true } };
		case actionTypes.UPDATE_COMBO_FAILURE: return { ...state, ...{ updatingCombo: false, error: action.error }};
        case actionTypes.UPDATE_COMBO_SUCCESS: return { ...state, ...{ updatingCombo: false, updateComboSuccessful:true,  combo: action.combo }};
        
        case actionTypes.DELETE_COMBO_REQUEST: return { ...state, ...{ deletingCombo: true } };
		case actionTypes.DELETE_COMBO_FAILURE: return { ...state, ...{ deletingCombo: false, error: action.error }};
        case actionTypes.DELETE_COMBO_SUCCESS: return { ...state, ...{ deletingCombo: false, deleteComboSuccessful:true,  combo: null }};
        
		default:return state;
	}
}

export default comboReducer;
