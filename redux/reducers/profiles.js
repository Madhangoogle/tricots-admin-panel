import { actionTypes } from "../actions/profiles";

export const initialState = {

    fetchingProfileDetails : false,
    fetchProfileDetailsSuccessful : false,
    profileDetails: null,

    updatingProfile : false,
    updateProfileSuccessful : false,
    
	error: null,
};

function profileReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

        case actionTypes.GET_PROFILE_DETAILS_REQUEST: return { ...state, ...{ fetchingProfileDetails: true } };
		case actionTypes.GET_PROFILE_DETAILS_FAILURE: return { ...state, ...{ fetchingProfileDetails: false, error: action.error }};
		case actionTypes.GET_PROFILE_DETAILS_SUCCESS: return { ...state, ...{ fetchingProfileDetails: false, fetchProfileDetailsSuccessful:true,  profileDetails: action.profileDetails }};

        case actionTypes.UPDATE_PROFILE_REQUEST: return { ...state, ...{ updatingProfile: true } };
		case actionTypes.UPDATE_PROFILE_FAILURE: return { ...state, ...{ updatingProfile: false, error: action.error }};
        case actionTypes.UPDATE_PROFILE_SUCCESS: return { ...state, ...{ updatingProfile: false, updateProfileSuccessful:true,  profile: action.profile }};
      
		default:return state;
	}
}

export default profileReducer;
