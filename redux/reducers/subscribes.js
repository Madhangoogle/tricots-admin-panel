import { actionTypes } from "../actions/subscribes";

export const initialState = {

    fetchingSubscribes : false,
    fetchSubscribeSuccessful : false,
    subscribes : [],

	error: null,
};

function subscribeReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_SUBSCRIBES_REQUEST: return { ...state, ...{ fetchingSubscribes: true } };
		case actionTypes.GET_SUBSCRIBES_FAILURE: return { ...state, ...{ fetchingSubscribes: false, error: action.error }};
		case actionTypes.GET_SUBSCRIBES_SUCCESS: return { ...state, ...{ fetchingSubscribes: false, fetchSubscribeSuccessful:true,  subscribes: action.subscribes }};

		default:return state;
	}
}

export default subscribeReducer;
