import { actionTypes } from "../actions/dashboard";

export const initialState = {

    fetchingDashboard : false,
    fetchDashboardSuccessful : false,
	dashboard : [],
	
	fetchingDashboardLocations : false,
	fetchDashboardLocationSuccessful : false,
	dashboardLocations: [],

	error: null,
};

function dashboardReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_DASHBOARD_REQUEST: return { ...state, ...{ fetchingDashboard: true } };
		case actionTypes.GET_DASHBOARD_FAILURE: return { ...state, ...{ fetchingDashboard: false, error: action.error }};
		case actionTypes.GET_DASHBOARD_SUCCESS: return { ...state, ...{ fetchingDashboard: false, fetchDashboardSuccessful:true,  dashboard: action.dashboard }};

		case actionTypes.GET_DASHBOARD_LOCATION_REQUEST: return { ...state, ...{ fetchingDashboardLocations: true } };
		case actionTypes.GET_DASHBOARD_LOCATION_FAILURE: return { ...state, ...{ fetchingDashboardLocations: false, error: action.error }};
		case actionTypes.GET_DASHBOARD_LOCATION_SUCCESS: return { ...state, ...{ fetchingDashboardLocations: false, fetchDashboardLocationSuccessful:true,  dashboardLocations: action.dashboardLocations }};

		default:return state;
	}
}

export default dashboardReducer;
