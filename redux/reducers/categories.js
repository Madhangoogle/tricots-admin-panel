import { actionTypes } from "../actions/categories";

export const initialState = {

    fetchingCategories : false,
    fetchCategorySuccessful : false,
    categories : [],

    fetchingCategoryDetails : false,
    fetchCategoryDetailsSuccessful : false,
    categoryDetails: null,

    creatingCategory : false,
    createCategorySuccessful : false,
    category : null,

    updatingCategory : false,
    updateCategorySuccessful : false,
    
    deletingCategory : false,
    deleteCategorySuccessful : false,

	error: null,
};

function categoryReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_CATEGORIES_REQUEST: return { ...state, ...{ fetchingCategories: true } };
		case actionTypes.GET_CATEGORIES_FAILURE: return { ...state, ...{ fetchingCategories: false, error: action.error }};
		case actionTypes.GET_CATEGORIES_SUCCESS: return { ...state, ...{ fetchingCategories: false, fetchCategorySuccessful:true,  categories: action.categories }};

        case actionTypes.GET_CATEGORY_DETAILS_REQUEST: return { ...state, ...{ fetchingCategoryDetails: true } };
		case actionTypes.GET_CATEGORY_DETAILS_FAILURE: return { ...state, ...{ fetchingCategoryDetails: false, error: action.error }};
		case actionTypes.GET_CATEGORY_DETAILS_SUCCESS: return { ...state, ...{ fetchingCategoryDetails: false, fetchCategoryDetailsSuccessful:true,  categoryDetails: action.categoryDetails }};

        case actionTypes.CREATE_CATEGORY_REQUEST: return { ...state, ...{ creatingCategory: true } };
		case actionTypes.CREATE_CATEGORY_FAILURE: return { ...state, ...{ creatingCategory: false, error: action.error }};
		case actionTypes.CREATE_CATEGORY_SUCCESS: return { ...state, ...{ creatingCategory: false, createCategorySuccessful:true,  category: action.category }};

        case actionTypes.UPDATE_CATEGORY_REQUEST: return { ...state, ...{ updatingCategory: true } };
		case actionTypes.UPDATE_CATEGORY_FAILURE: return { ...state, ...{ updatingCategory: false, error: action.error }};
        case actionTypes.UPDATE_CATEGORY_SUCCESS: return { ...state, ...{ updatingCategory: false, updateCategorySuccessful:true,  category: action.category }};
        
        case actionTypes.DELETE_CATEGORY_REQUEST: return { ...state, ...{ deletingCategory: true } };
		case actionTypes.DELETE_CATEGORY_FAILURE: return { ...state, ...{ deletingCategory: false, error: action.error }};
        case actionTypes.DELETE_CATEGORY_SUCCESS: return { ...state, ...{ deletingCategory: false, deleteCategorySuccessful:true,  category: null }};
        
		default:return state;
	}
}

export default categoryReducer;
