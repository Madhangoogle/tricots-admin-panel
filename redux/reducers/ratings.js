import { actionTypes } from "../actions/ratings";

export const initialState = {

    fetchingRatings : false,
    fetchRatingSuccessful : false,
    ratings : [],

    fetchingRatingDetails : false,
    fetchRatingDetailsSuccessful : false,
    ratingDetails: null,

    creatingRating : false,
    createRatingSuccessful : false,
    rating : null,

    updatingRating : false,
    updateRatingSuccessful : false,
    
    deletingRating : false,
    deleteRatingSuccessful : false,

	error: null,
};

function ratingReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_RATINGS_REQUEST: return { ...state, ...{ fetchingRatings: true } };
		case actionTypes.GET_RATINGS_FAILURE: return { ...state, ...{ fetchingRatings: false, error: action.error }};
		case actionTypes.GET_RATINGS_SUCCESS: return { ...state, ...{ fetchingRatings: false, fetchRatingSuccessful:true,  ratings: action.ratings }};

        case actionTypes.GET_RATING_DETAILS_REQUEST: return { ...state, ...{ fetchingRatingDetails: true } };
		case actionTypes.GET_RATING_DETAILS_FAILURE: return { ...state, ...{ fetchingRatingDetails: false, error: action.error }};
		case actionTypes.GET_RATING_DETAILS_SUCCESS: return { ...state, ...{ fetchingRatingDetails: false, fetchRatingDetailsSuccessful:true,  ratingDetails: action.ratingDetails }};

        case actionTypes.CREATE_RATING_REQUEST: return { ...state, ...{ creatingRating: true } };
		case actionTypes.CREATE_RATING_FAILURE: return { ...state, ...{ creatingRating: false, error: action.error }};
		case actionTypes.CREATE_RATING_SUCCESS: return { ...state, ...{ creatingRating: false, createRatingSuccessful:true,  rating: action.rating }};

        case actionTypes.UPDATE_RATING_REQUEST: return { ...state, ...{ updatingRating: true } };
		case actionTypes.UPDATE_RATING_FAILURE: return { ...state, ...{ updatingRating: false, error: action.error }};
        case actionTypes.UPDATE_RATING_SUCCESS: return { ...state, ...{ updatingRating: false, updateRatingSuccessful:true,  rating: action.rating }};
        
        case actionTypes.DELETE_RATING_REQUEST: return { ...state, ...{ deletingRating: true } };
		case actionTypes.DELETE_RATING_FAILURE: return { ...state, ...{ deletingRating: false, error: action.error }};
        case actionTypes.DELETE_RATING_SUCCESS: return { ...state, ...{ deletingRating: false, deleteRatingSuccessful:true,  rating: null }};
        
		default:return state;
	}
}

export default ratingReducer;
