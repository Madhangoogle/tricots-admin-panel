export const actionTypes = {
	CLEAR_ERROR: "PROMOCODES/CLEAR_ERROR",
	STATE_RESET: "PROMOCODES/STATE_RESET",

	GET_PROMOCODES_REQUEST: "PROMOCODES/GET_PROMOCODES_REQUEST",
	GET_PROMOCODES_SUCCESS: "PROMOCODES/GET_PROMOCODES_SUCCESS",
    GET_PROMOCODES_FAILURE: "PROMOCODES/GET_PROMOCODES_FAILURE",

    GET_PROMOCODE_DETAILS_REQUEST: "PROMOCODES/GET_PROMOCODE_DETAILS_REQUEST",
	GET_PROMOCODE_DETAILS_SUCCESS: "PROMOCODES/GET_PROMOCODE_DETAILS_SUCCESS",
    GET_PROMOCODE_DETAILS_FAILURE: "PROMOCODES/GET_PROMOCODE_DETAILS_FAILURE",
    
    CREATE_PROMOCODE_REQUEST: "PROMOCODES/CREATE_PROMOCODE_REQUEST",
	CREATE_PROMOCODE_SUCCESS: "PROMOCODES/CREATE_PROMOCODE_SUCCESS",
    CREATE_PROMOCODE_FAILURE: "PROMOCODES/CREATE_PROMOCODE_FAILURE",

    UPDATE_PROMOCODE_REQUEST: "PROMOCODES/UPDATE_PROMOCODE_REQUEST",
	UPDATE_PROMOCODE_SUCCESS: "PROMOCODES/UPDATE_PROMOCODE_SUCCESS",
    UPDATE_PROMOCODE_FAILURE: "PROMOCODES/UPDATE_PROMOCODE_FAILURE",
    
    DELETE_PROMOCODE_REQUEST: "PROMOCODES/DELETE_PROMOCODE_REQUEST",
	DELETE_PROMOCODE_SUCCESS: "PROMOCODES/DELETE_PROMOCODE_SUCCESS",
    DELETE_PROMOCODE_FAILURE: "PROMOCODES/DELETE_PROMOCODE_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getPromocodeRequest = (queryParams) => ({type: actionTypes.GET_PROMOCODES_REQUEST, queryParams});
export const getPromocodeFailure = (error) => ({type: actionTypes.GET_PROMOCODES_FAILURE, error});
export const getPromocodeSuccess = (promocodes) => ({type: actionTypes.GET_PROMOCODES_SUCCESS, promocodes});

export const getPromocodeDetailsRequest = (promocodeId) => ({type: actionTypes.GET_PROMOCODE_DETAILS_REQUEST, promocodeId});
export const getPromocodeDetailsFailure = (error) => ({type: actionTypes.GET_PROMOCODE_DETAILS_FAILURE, error});
export const getPromocodeDetailsSuccess = (promocodeDetails) => ({type: actionTypes.GET_PROMOCODE_DETAILS_SUCCESS, promocodeDetails});

export const createPromocodeRequest = (data) => ({type: actionTypes.CREATE_PROMOCODE_REQUEST, data});
export const createPromocodeFailure = (error) => ({type: actionTypes.CREATE_PROMOCODE_FAILURE, error});
export const createPromocodeSuccess = (promocode) => ({type: actionTypes.CREATE_PROMOCODE_SUCCESS, promocode});

export const updatePromocodeRequest = (promocodeId, data) => ({type: actionTypes.UPDATE_PROMOCODE_REQUEST, promocodeId, data});
export const updatePromocodeFailure = (error) => ({type: actionTypes.UPDATE_PROMOCODE_FAILURE, error});
export const updatePromocodeSuccess = (promocode) => ({type: actionTypes.UPDATE_PROMOCODE_SUCCESS, promocode});

export const deletePromocodeRequest = (promocodeId) => ({type: actionTypes.DELETE_PROMOCODE_REQUEST, promocodeId});
export const deletePromocodeFailure = (error) => ({type: actionTypes.DELETE_PROMOCODE_FAILURE, error});
export const deletePromocodeSuccess = () => ({type: actionTypes.DELETE_PROMOCODE_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getPromocodeRequest,
    getPromocodeFailure,
    getPromocodeSuccess,

    getPromocodeDetailsRequest,
    getPromocodeDetailsFailure,
    getPromocodeDetailsSuccess,

    createPromocodeRequest,
    createPromocodeFailure,
    createPromocodeSuccess,

    updatePromocodeRequest,
    updatePromocodeFailure,
    updatePromocodeSuccess,

    deletePromocodeRequest,
    deletePromocodeFailure,
    deletePromocodeSuccess,

};

export default AuthActions;
