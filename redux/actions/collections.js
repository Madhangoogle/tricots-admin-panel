export const actionTypes = {
	CLEAR_ERROR: "COLLECTIONS/CLEAR_ERROR",
	STATE_RESET: "COLLECTIONS/STATE_RESET",

	GET_COLLECTIONS_REQUEST: "COLLECTIONS/GET_COLLECTIONS_REQUEST",
	GET_COLLECTIONS_SUCCESS: "COLLECTIONS/GET_COLLECTIONS_SUCCESS",
    GET_COLLECTIONS_FAILURE: "COLLECTIONS/GET_COLLECTIONS_FAILURE",

    GET_COLLECTION_DETAILS_REQUEST: "COLLECTIONS/GET_COLLECTION_DETAILS_REQUEST",
	GET_COLLECTION_DETAILS_SUCCESS: "COLLECTIONS/GET_COLLECTION_DETAILS_SUCCESS",
    GET_COLLECTION_DETAILS_FAILURE: "COLLECTIONS/GET_COLLECTION_DETAILS_FAILURE",
    
    CREATE_COLLECTION_REQUEST: "COLLECTIONS/CREATE_COLLECTION_REQUEST",
	CREATE_COLLECTION_SUCCESS: "COLLECTIONS/CREATE_COLLECTION_SUCCESS",
    CREATE_COLLECTION_FAILURE: "COLLECTIONS/CREATE_COLLECTION_FAILURE",

    UPDATE_COLLECTION_REQUEST: "COLLECTIONS/UPDATE_COLLECTION_REQUEST",
	UPDATE_COLLECTION_SUCCESS: "COLLECTIONS/UPDATE_COLLECTION_SUCCESS",
    UPDATE_COLLECTION_FAILURE: "COLLECTIONS/UPDATE_COLLECTION_FAILURE",
    
    DELETE_COLLECTION_REQUEST: "COLLECTIONS/DELETE_COLLECTION_REQUEST",
	DELETE_COLLECTION_SUCCESS: "COLLECTIONS/DELETE_COLLECTION_SUCCESS",
    DELETE_COLLECTION_FAILURE: "COLLECTIONS/DELETE_COLLECTION_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getCollectionRequest = (queryParams) => ({type: actionTypes.GET_COLLECTIONS_REQUEST, queryParams});
export const getCollectionFailure = (error) => ({type: actionTypes.GET_COLLECTIONS_FAILURE, error});
export const getCollectionSuccess = (collections) => ({type: actionTypes.GET_COLLECTIONS_SUCCESS, collections});

export const getCollectionDetailsRequest = (collectionId) => ({type: actionTypes.GET_COLLECTION_DETAILS_REQUEST, collectionId});
export const getCollectionDetailsFailure = (error) => ({type: actionTypes.GET_COLLECTION_DETAILS_FAILURE, error});
export const getCollectionDetailsSuccess = (collectionDetails) => ({type: actionTypes.GET_COLLECTION_DETAILS_SUCCESS, collectionDetails});

export const createCollectionRequest = (data) => ({type: actionTypes.CREATE_COLLECTION_REQUEST, data});
export const createCollectionFailure = (error) => ({type: actionTypes.CREATE_COLLECTION_FAILURE, error});
export const createCollectionSuccess = (collection) => ({type: actionTypes.CREATE_COLLECTION_SUCCESS, collection});

export const updateCollectionRequest = (collectionId, data) => ({type: actionTypes.UPDATE_COLLECTION_REQUEST, collectionId, data});
export const updateCollectionFailure = (error) => ({type: actionTypes.UPDATE_COLLECTION_FAILURE, error});
export const updateCollectionSuccess = (collection) => ({type: actionTypes.UPDATE_COLLECTION_SUCCESS, collection});

export const deleteCollectionRequest = (collectionId) => ({type: actionTypes.DELETE_COLLECTION_REQUEST, collectionId});
export const deleteCollectionFailure = (error) => ({type: actionTypes.DELETE_COLLECTION_FAILURE, error});
export const deleteCollectionSuccess = () => ({type: actionTypes.DELETE_COLLECTION_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getCollectionRequest,
    getCollectionFailure,
    getCollectionSuccess,

    getCollectionDetailsRequest,
    getCollectionDetailsFailure,
    getCollectionDetailsSuccess,

    createCollectionRequest,
    createCollectionFailure,
    createCollectionSuccess,

    updateCollectionRequest,
    updateCollectionFailure,
    updateCollectionSuccess,

    deleteCollectionRequest,
    deleteCollectionFailure,
    deleteCollectionSuccess,

};

export default AuthActions;
