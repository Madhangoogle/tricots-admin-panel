export const actionTypes = {

    CLEAR_ERROR: "WASTEBIN/CLEAR_ERROR",
	STATE_RESET: "WASTEBIN/STATE_RESET",

	GET_WASTEBIN_REQUEST: "WASTEBINS/GET_WASTEBIN_REQUEST",
	GET_WASTEBIN_SUCCESS: "WASTEBINS/GET_WASTEBIN_SUCCESS",
    GET_WASTEBIN_FAILURE: "WASTEBINS/GET_WASTEBIN_FAILURE",

    GET_WASTEBIN_HISTORY_REQUEST:"WASTEBINS/GET_WASTEBIN_HISTORY_REQUEST",
    GET_WASTEBIN_HISTORY_SUCCESS:"WASTEBINS/GET_WASTEBIN_HISTORY_SUCCESS",
    GET_WASTEBIN_HISTORY_FAILURE:"WASTEBINS/GET_WASTEBIN_HISTORY_FAILURE",

    CREATE_WASTEBIN_REQUEST: "WASTEBIN/CREATE_WASTEBIN_REQUEST",
	CREATE_WASTEBIN_SUCCESS: "WASTEBIN/CREATE_WASTEBIN_SUCCESS",
    CREATE_WASTEBIN_FAILURE: "WASTEBIN/CREATE_WASTEBIN_FAILURE",

    GET_WASTEBIN_DETAILS_REQUEST:"WASTEBIN/GET_WASTEBIN_DETAILS_REQUEST",
    GET_WASTEBIN_DETAILS_SUCCESS:"WASTEBIN/GET_WASTEBIN_DETAILS_SUCCESS",
    GET_WASTEBIN_DETAILS_FAILURE:"WASTEBIN/GET_WASTEBIN_DETAILS_FAILURE",

    UPDATE_WASTEBIN_REQUEST: "WASTEBIN/UPDATE_WASTEBIN_REQUEST",
	UPDATE_WASTEBIN_SUCCESS: "WASTEBIN/UPDATE_WASTEBIN_SUCCESS",
    UPDATE_WASTEBIN_FAILURE: "WASTEBIN/UPDATE_WASTEBIN_FAILURE",
    
    DELETE_WASTEBIN_REQUEST: "WASTEBIN/DELETE_WASTEBIN_REQUEST",
	DELETE_WASTEBIN_SUCCESS: "WASTEBIN/DELETE_WASTEBIN_SUCCESS",
	DELETE_WASTEBIN_FAILURE: "WASTEBIN/DELETE_WASTEBIN_FAILURE",

};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getWasteBinRequest = (queryParams) => ({type: actionTypes.GET_WASTEBIN_REQUEST, queryParams});
export const getWasteBinFailure = (error) => ({type: actionTypes.GET_WASTEBIN_FAILURE, error});
export const getWasteBinSuccess = (wasteBins) => ({type: actionTypes.GET_WASTEBIN_SUCCESS, wasteBins});

export const getWasteBinHistoryRequest = (wasteBinId) => ({type:actionTypes.GET_WASTEBIN_HISTORY_REQUEST,wasteBinId});
export const getWasteBinHistoryFailure = () => ({type:actionTypes.GET_WASTEBIN_HISTORY_FAILURE,error});
export const getWasteBinHistorySuccess = (wasteBinHistory) => ({type:actionTypes.GET_WASTEBIN_HISTORY_SUCCESS,wasteBinHistory});

export const getWasteBinDetailsRequest = (wasteBinId) =>({type:actionTypes.GET_WASTEBIN_DETAILS_REQUEST,wasteBinId});
export const getWasteBinDetailsFailure = (error) =>({type:actionTypes.GET_WASTEBIN_DETAILS_FAILURE,error});
export const getWasteBinDetailsSuccess = (wasteBinDetail) => ({type:actionTypes.GET_WASTEBIN_DETAILS_SUCCESS,wasteBinDetail})

export const createWasteBinRequest = (data) => ({type: actionTypes.CREATE_WASTEBIN_REQUEST, data});
export const createWasteBinFailure = (error) => ({type: actionTypes.CREATE_WASTEBIN_FAILURE, error});
export const createWasteBinSuccess = (wasteBin) => ({type: actionTypes.CREATE_WASTEBIN_SUCCESS, wasteBin});

export const updateWasteBinRequest = (wasteBinId, data) => ({type: actionTypes.UPDATE_WASTEBIN_REQUEST, wasteBinId, data});
export const updateWasteBinFailure = (error) => ({type: actionTypes.UPDATE_WASTEBIN_FAILURE, error});
export const updateWasteBinSuccess = (wasteBin) => ({type: actionTypes.UPDATE_WASTEBIN_SUCCESS, wasteBin});

export const deleteWasteBinRequest = (wasteBinId) => ({type: actionTypes.DELETE_WASTEBIN_REQUEST, wasteBinId});
export const deleteWasteBinFailure = (error) => ({type: actionTypes.DELETE_WASTEBIN_FAILURE, error});
export const deleteWasteBinSuccess = () => ({type: actionTypes.DELETE_WASTEBIN_SUCCESS });

export const logoutRequest = () => ({ type: actionTypes.LOGOUT_REQUEST });
export const logoutSuccess = () => ({ type: actionTypes.LOGOUT_SUCCESS });
export const logoutFailure = (error) => ({type: actionTypes.LOGIN_FAILURE,error});


const WasteBinActions = {
	actionTypes,

	clearError,
	stateReset,

    getWasteBinRequest,
    getWasteBinFailure,
    getWasteBinSuccess,

    getWasteBinHistoryRequest,
    getWasteBinHistoryFailure,
    getWasteBinHistorySuccess,

    getWasteBinDetailsRequest,
    getWasteBinDetailsFailure,
    getWasteBinDetailsSuccess,

    createWasteBinRequest,
    createWasteBinFailure,
    createWasteBinSuccess,

    updateWasteBinRequest,
    updateWasteBinFailure,
    updateWasteBinSuccess,

    deleteWasteBinRequest,
    deleteWasteBinFailure,
    deleteWasteBinSuccess,

};

export default WasteBinActions;
