export const actionTypes = {
	CLEAR_ERROR: "RATINGS/CLEAR_ERROR",
	STATE_RESET: "RATINGS/STATE_RESET",

	GET_RATINGS_REQUEST: "RATINGS/GET_RATINGS_REQUEST",
	GET_RATINGS_SUCCESS: "RATINGS/GET_RATINGS_SUCCESS",
    GET_RATINGS_FAILURE: "RATINGS/GET_RATINGS_FAILURE",

    GET_RATING_DETAILS_REQUEST: "RATINGS/GET_RATING_DETAILS_REQUEST",
	GET_RATING_DETAILS_SUCCESS: "RATINGS/GET_RATING_DETAILS_SUCCESS",
    GET_RATING_DETAILS_FAILURE: "RATINGS/GET_RATING_DETAILS_FAILURE",
    
    CREATE_RATING_REQUEST: "RATINGS/CREATE_RATING_REQUEST",
	CREATE_RATING_SUCCESS: "RATINGS/CREATE_RATING_SUCCESS",
    CREATE_RATING_FAILURE: "RATINGS/CREATE_RATING_FAILURE",

    UPDATE_RATING_REQUEST: "RATINGS/UPDATE_RATING_REQUEST",
	UPDATE_RATING_SUCCESS: "RATINGS/UPDATE_RATING_SUCCESS",
    UPDATE_RATING_FAILURE: "RATINGS/UPDATE_RATING_FAILURE",
    
    DELETE_RATING_REQUEST: "RATINGS/DELETE_RATING_REQUEST",
	DELETE_RATING_SUCCESS: "RATINGS/DELETE_RATING_SUCCESS",
    DELETE_RATING_FAILURE: "RATINGS/DELETE_RATING_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getRatingRequest = (queryParams) => ({type: actionTypes.GET_RATINGS_REQUEST, queryParams});
export const getRatingFailure = (error) => ({type: actionTypes.GET_RATINGS_FAILURE, error});
export const getRatingSuccess = (ratings) => ({type: actionTypes.GET_RATINGS_SUCCESS, ratings});

export const getRatingDetailsRequest = (ratingId) => ({type: actionTypes.GET_RATING_DETAILS_REQUEST, ratingId});
export const getRatingDetailsFailure = (error) => ({type: actionTypes.GET_RATING_DETAILS_FAILURE, error});
export const getRatingDetailsSuccess = (ratingDetails) => ({type: actionTypes.GET_RATING_DETAILS_SUCCESS, ratingDetails});

export const createRatingRequest = (data) => ({type: actionTypes.CREATE_RATING_REQUEST, data});
export const createRatingFailure = (error) => ({type: actionTypes.CREATE_RATING_FAILURE, error});
export const createRatingSuccess = (rating) => ({type: actionTypes.CREATE_RATING_SUCCESS, rating});

export const updateRatingRequest = (ratingId, data) => ({type: actionTypes.UPDATE_RATING_REQUEST, ratingId, data});
export const updateRatingFailure = (error) => ({type: actionTypes.UPDATE_RATING_FAILURE, error});
export const updateRatingSuccess = (rating) => ({type: actionTypes.UPDATE_RATING_SUCCESS, rating});

export const deleteRatingRequest = (ratingId) => ({type: actionTypes.DELETE_RATING_REQUEST, ratingId});
export const deleteRatingFailure = (error) => ({type: actionTypes.DELETE_RATING_FAILURE, error});
export const deleteRatingSuccess = () => ({type: actionTypes.DELETE_RATING_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getRatingRequest,
    getRatingFailure,
    getRatingSuccess,

    getRatingDetailsRequest,
    getRatingDetailsFailure,
    getRatingDetailsSuccess,

    createRatingRequest,
    createRatingFailure,
    createRatingSuccess,

    updateRatingRequest,
    updateRatingFailure,
    updateRatingSuccess,

    deleteRatingRequest,
    deleteRatingFailure,
    deleteRatingSuccess,

};

export default AuthActions;
