export const actionTypes = {
    CLEAR_ERROR:"STAFFS/CLEAR_ERROR",
    STATE_RESET:"STAFFS/STATE_RESET",

    GET_STAFFS_REQUEST:"STAFFS/GET_STAFFS_REQUEST",
    GET_STAFFS_SUCCESS: "STAFFS/GET_STAFFS_SUCCESS",
    GET_STAFFS_FAILURE: "STAFFS/GET_STAFFS_FAILURE",

    GET_STAFF_HISTORY_REQUEST:"STAFFS/GET_STAFF_HISTORY_REQUEST",
    GET_STAFF_HISTORY_SUCCESS:"STAFFS/GET_STAFF_HISTORY_SUCCESS",
    GET_STAFF_HISTORY_FAILURE:"STAFFS/GET_STAFF_HISTORY_FAILURE",

    GET_STAFF_HISTORY_DETAIL_REQUEST:"STAFFS/GET_STAFF_HISTORY_DETAIL_REQUEST",
    GET_STAFF_HISTORY_DETAIL_SUCCESS:"STAFFS/GET_STAFF_HISTORY_DETAIL_SUCCESS",
    GET_STAFF_HISTORY_DETAIL_FAILURE:"STAFFS/GET_STAFF_HISTORY_DETAIL_FAILURE",
    
    CREATE_STAFF_REQUEST: "STAFF/CREATE_STAFF_REQUEST",
	CREATE_STAFF_SUCCESS: "STAFF/CREATE_STAFF_SUCCESS",
    CREATE_STAFF_FAILURE: "STAFF/CREATE_STAFF_FAILURE",

    GET_STAFF_DETAILS_REQUEST:"STAFF/GET_STAFF_DETAILS_REQUEST",
    GET_STAFF_DETAILS_SUCCESS:"STAFF/GET_STAFF_DETAILS_SUCCESS",
    GET_STAFF_DETAILS_FAILURE:"STAFF/GET_STAFF_DETAILS_FAILURE",

    UPDATE_STAFF_REQUEST: "STAFF/UPDATE_STAFF_REQUEST",
	UPDATE_STAFF_SUCCESS: "STAFF/UPDATE_STAFF_SUCCESS",
    UPDATE_STAFF_FAILURE: "STAFF/UPDATE_STAFF_FAILURE",
    
    DELETE_STAFF_REQUEST: "STAFF/DELETE_STAFF_REQUEST",
	DELETE_STAFF_SUCCESS: "STAFF/DELETE_STAFF_SUCCESS",
	DELETE_STAFF_FAILURE: "STAFF/DELETE_STAFF_FAILURE",
}

export const clearError = () =>({type:actionTypes.CLEAR_ERROR})
export const stateReset = (resetState) =>({type:actionTypes.STATE_RESET,resetState});

export const getStaffRequest = (queryParams) => ({type: actionTypes.GET_STAFFS_REQUEST, queryParams});
export const getStaffFailure = (error) => ({type: actionTypes.GET_STAFFS_FAILURE, error});
export const getStaffSuccess = (Staffs) => ({type: actionTypes.GET_STAFFS_SUCCESS, Staffs});

export const getStaffHistoryRequest = (StaffId, queryParams) => ({type:actionTypes.GET_STAFF_HISTORY_REQUEST,StaffId,queryParams});
export const getStaffHistoryFailure = () => ({type:actionTypes.GET_STAFF_HISTORY_FAILURE,error});
export const getStaffHistorySuccess = (StaffHistory) => ({type:actionTypes.GET_STAFF_HISTORY_SUCCESS,StaffHistory});

export const getStaffHistoryDetailRequest = (StaffId) => ({type:actionTypes.GET_STAFF_HISTORY_DETAIL_REQUEST,StaffId});
export const getStaffHistoryDetailFailure = () => ({type:actionTypes.GET_STAFF_HISTORY_DETAIL_FAILURE,error});
export const getStaffHistoryDetailSuccess = (historyDetail) => ({type:actionTypes.GET_STAFF_HISTORY_DETAIL_SUCCESS,historyDetail});


export const createStaffRequest = (data) => ({type: actionTypes.CREATE_STAFF_REQUEST, data});
export const createStaffFailure = (error) => ({type: actionTypes.CREATE_STAFF_FAILURE, error});
export const createStaffSuccess = (Staff) => ({type: actionTypes.CREATE_STAFF_SUCCESS, Staff});

export const getStaffDetailsRequest = (StaffId) =>({type:actionTypes.GET_STAFF_DETAILS_REQUEST,StaffId});
export const getStaffDetailsFailure = (error) =>({type:actionTypes.GET_STAFF_DETAILS_FAILURE,error});
export const getStaffDetailsSuccess = (staff) => ({type:actionTypes.GET_STAFF_DETAILS_SUCCESS,staff})

export const updateStaffRequest = (StaffId, data) => ({type: actionTypes.UPDATE_STAFF_REQUEST, StaffId, data});
export const updateStaffFailure = (error) => ({type: actionTypes.UPDATE_STAFF_FAILURE, error});
export const updateStaffSuccess = (Staff) => ({type: actionTypes.UPDATE_STAFF_SUCCESS, Staff});

export const deleteStaffRequest = (StaffId) => ({type: actionTypes.DELETE_STAFF_REQUEST, StaffId});
export const deleteStaffFailure = (error) => ({type: actionTypes.DELETE_STAFF_FAILURE, error});
export const deleteStaffSuccess = () => ({type: actionTypes.DELETE_STAFF_SUCCESS });

export const logoutRequest = () => ({ type: actionTypes.LOGOUT_REQUEST });
export const logoutSuccess = () => ({ type: actionTypes.LOGOUT_SUCCESS });
export const logoutFailure = (error) => ({type: actionTypes.LOGIN_FAILURE,error});

const StaffsActions = {
	actionTypes,

	clearError,
	stateReset,

    getStaffRequest,
    getStaffFailure,
    getStaffSuccess,

    getStaffHistoryRequest,
    getStaffHistoryFailure,
    getStaffHistorySuccess,

    getStaffHistoryDetailRequest,
    getStaffHistoryDetailFailure,
    getStaffHistoryDetailSuccess,
    
    createStaffRequest,
    createStaffFailure,
    createStaffSuccess,

    getStaffDetailsRequest,
    getStaffDetailsFailure,
    getStaffDetailsSuccess,

    updateStaffRequest,
    updateStaffFailure,
    updateStaffSuccess,

    deleteStaffRequest,
    deleteStaffFailure,
    deleteStaffSuccess,

};

export default StaffsActions;
