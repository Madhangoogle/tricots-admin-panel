export const actionTypes = {
	CLEAR_ERROR: "PRODUCTS/CLEAR_ERROR",
	STATE_RESET: "PRODUCTS/STATE_RESET",

	GET_PRODUCTS_REQUEST: "PRODUCTS/GET_PRODUCTS_REQUEST",
	GET_PRODUCTS_SUCCESS: "PRODUCTS/GET_PRODUCTS_SUCCESS",
    GET_PRODUCTS_FAILURE: "PRODUCTS/GET_PRODUCTS_FAILURE",

    GET_PRODUCT_DETAILS_REQUEST: "PRODUCTS/GET_PRODUCT_DETAILS_REQUEST",
	GET_PRODUCT_DETAILS_SUCCESS: "PRODUCTS/GET_PRODUCT_DETAILS_SUCCESS",
    GET_PRODUCT_DETAILS_FAILURE: "PRODUCTS/GET_PRODUCT_DETAILS_FAILURE",
    
    CREATE_PRODUCT_REQUEST: "PRODUCTS/CREATE_PRODUCT_REQUEST",
	CREATE_PRODUCT_SUCCESS: "PRODUCTS/CREATE_PRODUCT_SUCCESS",
    CREATE_PRODUCT_FAILURE: "PRODUCTS/CREATE_PRODUCT_FAILURE",

    UPDATE_PRODUCT_REQUEST: "PRODUCTS/UPDATE_PRODUCT_REQUEST",
	UPDATE_PRODUCT_SUCCESS: "PRODUCTS/UPDATE_PRODUCT_SUCCESS",
    UPDATE_PRODUCT_FAILURE: "PRODUCTS/UPDATE_PRODUCT_FAILURE",
    
    DELETE_PRODUCT_REQUEST: "PRODUCTS/DELETE_PRODUCT_REQUEST",
	DELETE_PRODUCT_SUCCESS: "PRODUCTS/DELETE_PRODUCT_SUCCESS",
    DELETE_PRODUCT_FAILURE: "PRODUCTS/DELETE_PRODUCT_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getProductRequest = (queryParams) => ({type: actionTypes.GET_PRODUCTS_REQUEST, queryParams});
export const getProductFailure = (error) => ({type: actionTypes.GET_PRODUCTS_FAILURE, error});
export const getProductSuccess = (products) => ({type: actionTypes.GET_PRODUCTS_SUCCESS, products});

export const getProductDetailsRequest = (productId) => ({type: actionTypes.GET_PRODUCT_DETAILS_REQUEST, productId});
export const getProductDetailsFailure = (error) => ({type: actionTypes.GET_PRODUCT_DETAILS_FAILURE, error});
export const getProductDetailsSuccess = (productDetails) => ({type: actionTypes.GET_PRODUCT_DETAILS_SUCCESS, productDetails});

export const createProductRequest = (data) => ({type: actionTypes.CREATE_PRODUCT_REQUEST, data});
export const createProductFailure = (error) => ({type: actionTypes.CREATE_PRODUCT_FAILURE, error});
export const createProductSuccess = (product) => ({type: actionTypes.CREATE_PRODUCT_SUCCESS, product});

export const updateProductRequest = (productId, data) => ({type: actionTypes.UPDATE_PRODUCT_REQUEST, productId, data});
export const updateProductFailure = (error) => ({type: actionTypes.UPDATE_PRODUCT_FAILURE, error});
export const updateProductSuccess = (product) => ({type: actionTypes.UPDATE_PRODUCT_SUCCESS, product});

export const deleteProductRequest = (productId) => ({type: actionTypes.DELETE_PRODUCT_REQUEST, productId});
export const deleteProductFailure = (error) => ({type: actionTypes.DELETE_PRODUCT_FAILURE, error});
export const deleteProductSuccess = () => ({type: actionTypes.DELETE_PRODUCT_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getProductRequest,
    getProductFailure,
    getProductSuccess,

    getProductDetailsRequest,
    getProductDetailsFailure,
    getProductDetailsSuccess,

    createProductRequest,
    createProductFailure,
    createProductSuccess,

    updateProductRequest,
    updateProductFailure,
    updateProductSuccess,

    deleteProductRequest,
    deleteProductFailure,
    deleteProductSuccess,

};

export default AuthActions;
