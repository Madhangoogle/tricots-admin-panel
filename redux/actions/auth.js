export const actionTypes = {
	CLEAR_ERROR: "AUTH/CLEAR_ERROR",
	STATE_RESET: "AUTH/STATE_RESET",

	LOGIN_REQUEST: "AUTH/LOGIN_REQUEST",
	LOGIN_SUCCESS: "AUTH/LOGIN_SUCCESS",
	LOGIN_FAILURE: "AUTH/LOGIN_FAILURE",

	LOGOUT_REQUEST: "AUTH/LOGOUT_REQUEST",
	LOGOUT_SUCCESS: "AUTH/LOGOUT_SUCCESS",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const loginRequest = (username, password) => ({type: actionTypes.LOGIN_REQUEST,username,password});
export const loginSuccess = (accessToken) => ({type: actionTypes.LOGIN_SUCCESS,accessToken});
export const loginFailure = (error) => ({type: actionTypes.LOGIN_FAILURE,error});

export const logoutRequest = () => ({ type: actionTypes.LOGOUT_REQUEST });
export const logoutSuccess = () => ({ type: actionTypes.LOGOUT_SUCCESS });
export const logoutFailure = (error) => ({type: actionTypes.LOGIN_FAILURE,error});


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

	loginRequest,
	loginSuccess,
	loginFailure,

	logoutRequest,
	logoutSuccess,
	logoutFailure
};

export default AuthActions;
