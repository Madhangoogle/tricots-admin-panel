export const actionTypes = {
	CLEAR_ERROR: "PROFILES/CLEAR_ERROR",
	STATE_RESET: "PROFILES/STATE_RESET",

    GET_PROFILE_DETAILS_REQUEST: "PROFILES/GET_PROFILE_DETAILS_REQUEST",
	GET_PROFILE_DETAILS_SUCCESS: "PROFILES/GET_PROFILE_DETAILS_SUCCESS",
    GET_PROFILE_DETAILS_FAILURE: "PROFILES/GET_PROFILE_DETAILS_FAILURE",
    
    UPDATE_PROFILE_REQUEST: "PROFILES/UPDATE_PROFILE_REQUEST",
	UPDATE_PROFILE_SUCCESS: "PROFILES/UPDATE_PROFILE_SUCCESS",
    UPDATE_PROFILE_FAILURE: "PROFILES/UPDATE_PROFILE_FAILURE",
    
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getProfileDetailsRequest = (profileId) => ({type: actionTypes.GET_PROFILE_DETAILS_REQUEST, profileId});
export const getProfileDetailsFailure = (error) => ({type: actionTypes.GET_PROFILE_DETAILS_FAILURE, error});
export const getProfileDetailsSuccess = (profileDetails) => ({type: actionTypes.GET_PROFILE_DETAILS_SUCCESS, profileDetails});

export const updateProfileRequest = (data) => ({type: actionTypes.UPDATE_PROFILE_REQUEST, data});
export const updateProfileFailure = (error) => ({type: actionTypes.UPDATE_PROFILE_FAILURE, error});
export const updateProfileSuccess = (profile) => ({type: actionTypes.UPDATE_PROFILE_SUCCESS, profile});



const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getProfileDetailsRequest,
    getProfileDetailsFailure,
    getProfileDetailsSuccess,

    updateProfileRequest,
    updateProfileFailure,
    updateProfileSuccess,

};

export default AuthActions;
