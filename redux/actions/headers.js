export const actionTypes = {
	CLEAR_ERROR: "HEADERS/CLEAR_ERROR",
	STATE_RESET: "HEADERS/STATE_RESET",

	GET_HEADERS_REQUEST: "HEADERS/GET_HEADERS_REQUEST",
	GET_HEADERS_SUCCESS: "HEADERS/GET_HEADERS_SUCCESS",
    GET_HEADERS_FAILURE: "HEADERS/GET_HEADERS_FAILURE",

    GET_HEADER_DETAILS_REQUEST: "HEADERS/GET_HEADER_DETAILS_REQUEST",
	GET_HEADER_DETAILS_SUCCESS: "HEADERS/GET_HEADER_DETAILS_SUCCESS",
    GET_HEADER_DETAILS_FAILURE: "HEADERS/GET_HEADER_DETAILS_FAILURE",
    
    CREATE_HEADER_REQUEST: "HEADERS/CREATE_HEADER_REQUEST",
	CREATE_HEADER_SUCCESS: "HEADERS/CREATE_HEADER_SUCCESS",
    CREATE_HEADER_FAILURE: "HEADERS/CREATE_HEADER_FAILURE",

    UPDATE_HEADER_REQUEST: "HEADERS/UPDATE_HEADER_REQUEST",
	UPDATE_HEADER_SUCCESS: "HEADERS/UPDATE_HEADER_SUCCESS",
    UPDATE_HEADER_FAILURE: "HEADERS/UPDATE_HEADER_FAILURE",
    
    DELETE_HEADER_REQUEST: "HEADERS/DELETE_HEADER_REQUEST",
	DELETE_HEADER_SUCCESS: "HEADERS/DELETE_HEADER_SUCCESS",
    DELETE_HEADER_FAILURE: "HEADERS/DELETE_HEADER_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getHeaderRequest = (queryParams) => ({type: actionTypes.GET_HEADERS_REQUEST, queryParams});
export const getHeaderFailure = (error) => ({type: actionTypes.GET_HEADERS_FAILURE, error});
export const getHeaderSuccess = (headers) => ({type: actionTypes.GET_HEADERS_SUCCESS, headers});

export const getHeaderDetailsRequest = (headerId) => ({type: actionTypes.GET_HEADER_DETAILS_REQUEST, headerId});
export const getHeaderDetailsFailure = (error) => ({type: actionTypes.GET_HEADER_DETAILS_FAILURE, error});
export const getHeaderDetailsSuccess = (headerDetails) => ({type: actionTypes.GET_HEADER_DETAILS_SUCCESS, headerDetails});

export const createHeaderRequest = (data) => ({type: actionTypes.CREATE_HEADER_REQUEST, data});
export const createHeaderFailure = (error) => ({type: actionTypes.CREATE_HEADER_FAILURE, error});
export const createHeaderSuccess = (header) => ({type: actionTypes.CREATE_HEADER_SUCCESS, header});

export const updateHeaderRequest = (headerId, data) => ({type: actionTypes.UPDATE_HEADER_REQUEST, headerId, data});
export const updateHeaderFailure = (error) => ({type: actionTypes.UPDATE_HEADER_FAILURE, error});
export const updateHeaderSuccess = (header) => ({type: actionTypes.UPDATE_HEADER_SUCCESS, header});

export const deleteHeaderRequest = (headerId) => ({type: actionTypes.DELETE_HEADER_REQUEST, headerId});
export const deleteHeaderFailure = (error) => ({type: actionTypes.DELETE_HEADER_FAILURE, error});
export const deleteHeaderSuccess = () => ({type: actionTypes.DELETE_HEADER_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getHeaderRequest,
    getHeaderFailure,
    getHeaderSuccess,

    getHeaderDetailsRequest,
    getHeaderDetailsFailure,
    getHeaderDetailsSuccess,

    createHeaderRequest,
    createHeaderFailure,
    createHeaderSuccess,

    updateHeaderRequest,
    updateHeaderFailure,
    updateHeaderSuccess,

    deleteHeaderRequest,
    deleteHeaderFailure,
    deleteHeaderSuccess,

};

export default AuthActions;
