export const actionTypes = {
	CLEAR_ERROR: "ORDERS/CLEAR_ERROR",
	STATE_RESET: "ORDERS/STATE_RESET",

	GET_ORDERS_REQUEST: "ORDERS/GET_ORDERS_REQUEST",
	GET_ORDERS_SUCCESS: "ORDERS/GET_ORDERS_SUCCESS",
    GET_ORDERS_FAILURE: "ORDERS/GET_ORDERS_FAILURE",

    GET_ORDER_DETAILS_REQUEST: "ORDERS/GET_ORDER_DETAILS_REQUEST",
	GET_ORDER_DETAILS_SUCCESS: "ORDERS/GET_ORDER_DETAILS_SUCCESS",
    GET_ORDER_DETAILS_FAILURE: "ORDERS/GET_ORDER_DETAILS_FAILURE",
    
    CREATE_ORDER_REQUEST: "ORDERS/CREATE_ORDER_REQUEST",
	CREATE_ORDER_SUCCESS: "ORDERS/CREATE_ORDER_SUCCESS",
    CREATE_ORDER_FAILURE: "ORDERS/CREATE_ORDER_FAILURE",

    UPDATE_ORDER_REQUEST: "ORDERS/UPDATE_ORDER_REQUEST",
	UPDATE_ORDER_SUCCESS: "ORDERS/UPDATE_ORDER_SUCCESS",
    UPDATE_ORDER_FAILURE: "ORDERS/UPDATE_ORDER_FAILURE",
    
    DELETE_ORDER_REQUEST: "ORDERS/DELETE_ORDER_REQUEST",
	DELETE_ORDER_SUCCESS: "ORDERS/DELETE_ORDER_SUCCESS",
    DELETE_ORDER_FAILURE: "ORDERS/DELETE_ORDER_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getOrderRequest = (queryParams) => ({type: actionTypes.GET_ORDERS_REQUEST, queryParams});
export const getOrderFailure = (error) => ({type: actionTypes.GET_ORDERS_FAILURE, error});
export const getOrderSuccess = (orders) => ({type: actionTypes.GET_ORDERS_SUCCESS, orders});

export const getOrderDetailsRequest = (orderId) => ({type: actionTypes.GET_ORDER_DETAILS_REQUEST, orderId});
export const getOrderDetailsFailure = (error) => ({type: actionTypes.GET_ORDER_DETAILS_FAILURE, error});
export const getOrderDetailsSuccess = (orderDetails) => ({type: actionTypes.GET_ORDER_DETAILS_SUCCESS, orderDetails});

export const createOrderRequest = (data) => ({type: actionTypes.CREATE_ORDER_REQUEST, data});
export const createOrderFailure = (error) => ({type: actionTypes.CREATE_ORDER_FAILURE, error});
export const createOrderSuccess = (order) => ({type: actionTypes.CREATE_ORDER_SUCCESS, order});

export const updateOrderRequest = (orderId, data) => ({type: actionTypes.UPDATE_ORDER_REQUEST, orderId, data});
export const updateOrderFailure = (error) => ({type: actionTypes.UPDATE_ORDER_FAILURE, error});
export const updateOrderSuccess = (order) => ({type: actionTypes.UPDATE_ORDER_SUCCESS, order});

export const deleteOrderRequest = (orderId) => ({type: actionTypes.DELETE_ORDER_REQUEST, orderId});
export const deleteOrderFailure = (error) => ({type: actionTypes.DELETE_ORDER_FAILURE, error});
export const deleteOrderSuccess = () => ({type: actionTypes.DELETE_ORDER_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getOrderRequest,
    getOrderFailure,
    getOrderSuccess,

    getOrderDetailsRequest,
    getOrderDetailsFailure,
    getOrderDetailsSuccess,

    createOrderRequest,
    createOrderFailure,
    createOrderSuccess,

    updateOrderRequest,
    updateOrderFailure,
    updateOrderSuccess,

    deleteOrderRequest,
    deleteOrderFailure,
    deleteOrderSuccess,

};

export default AuthActions;
