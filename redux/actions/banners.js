export const actionTypes = {
	CLEAR_ERROR: "BANNERS/CLEAR_ERROR",
	STATE_RESET: "BANNERS/STATE_RESET",

	GET_BANNERS_REQUEST: "BANNERS/GET_BANNERS_REQUEST",
	GET_BANNERS_SUCCESS: "BANNERS/GET_BANNERS_SUCCESS",
    GET_BANNERS_FAILURE: "BANNERS/GET_BANNERS_FAILURE",

    GET_BANNER_DETAILS_REQUEST: "BANNERS/GET_BANNER_DETAILS_REQUEST",
	GET_BANNER_DETAILS_SUCCESS: "BANNERS/GET_BANNER_DETAILS_SUCCESS",
    GET_BANNER_DETAILS_FAILURE: "BANNERS/GET_BANNER_DETAILS_FAILURE",
    
    CREATE_BANNER_REQUEST: "BANNERS/CREATE_BANNER_REQUEST",
	CREATE_BANNER_SUCCESS: "BANNERS/CREATE_BANNER_SUCCESS",
    CREATE_BANNER_FAILURE: "BANNERS/CREATE_BANNER_FAILURE",

    UPDATE_BANNER_REQUEST: "BANNERS/UPDATE_BANNER_REQUEST",
	UPDATE_BANNER_SUCCESS: "BANNERS/UPDATE_BANNER_SUCCESS",
    UPDATE_BANNER_FAILURE: "BANNERS/UPDATE_BANNER_FAILURE",
    
    DELETE_BANNER_REQUEST: "BANNERS/DELETE_BANNER_REQUEST",
	DELETE_BANNER_SUCCESS: "BANNERS/DELETE_BANNER_SUCCESS",
    DELETE_BANNER_FAILURE: "BANNERS/DELETE_BANNER_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getBannerRequest = (queryParams) => ({type: actionTypes.GET_BANNERS_REQUEST, queryParams});
export const getBannerFailure = (error) => ({type: actionTypes.GET_BANNERS_FAILURE, error});
export const getBannerSuccess = (banners) => ({type: actionTypes.GET_BANNERS_SUCCESS, banners});

export const getBannerDetailsRequest = (bannerId) => ({type: actionTypes.GET_BANNER_DETAILS_REQUEST, bannerId});
export const getBannerDetailsFailure = (error) => ({type: actionTypes.GET_BANNER_DETAILS_FAILURE, error});
export const getBannerDetailsSuccess = (bannerDetails) => ({type: actionTypes.GET_BANNER_DETAILS_SUCCESS, bannerDetails});

export const createBannerRequest = (data) => ({type: actionTypes.CREATE_BANNER_REQUEST, data});
export const createBannerFailure = (error) => ({type: actionTypes.CREATE_BANNER_FAILURE, error});
export const createBannerSuccess = (banner) => ({type: actionTypes.CREATE_BANNER_SUCCESS, banner});

export const updateBannerRequest = (bannerId, data) => ({type: actionTypes.UPDATE_BANNER_REQUEST, bannerId, data});
export const updateBannerFailure = (error) => ({type: actionTypes.UPDATE_BANNER_FAILURE, error});
export const updateBannerSuccess = (banner) => ({type: actionTypes.UPDATE_BANNER_SUCCESS, banner});

export const deleteBannerRequest = (bannerId) => ({type: actionTypes.DELETE_BANNER_REQUEST, bannerId});
export const deleteBannerFailure = (error) => ({type: actionTypes.DELETE_BANNER_FAILURE, error});
export const deleteBannerSuccess = () => ({type: actionTypes.DELETE_BANNER_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getBannerRequest,
    getBannerFailure,
    getBannerSuccess,

    getBannerDetailsRequest,
    getBannerDetailsFailure,
    getBannerDetailsSuccess,

    createBannerRequest,
    createBannerFailure,
    createBannerSuccess,

    updateBannerRequest,
    updateBannerFailure,
    updateBannerSuccess,

    deleteBannerRequest,
    deleteBannerFailure,
    deleteBannerSuccess,

};

export default AuthActions;
