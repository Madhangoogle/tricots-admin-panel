export const actionTypes = {
	CLEAR_ERROR: "CHARTS/CLEAR_ERROR",
	STATE_RESET: "CHARTS/STATE_RESET",

	GET_CHARTS_REQUEST: "CHARTS/GET_CHARTS_REQUEST",
	GET_CHARTS_SUCCESS: "CHARTS/GET_CHARTS_SUCCESS",
    GET_CHARTS_FAILURE: "CHARTS/GET_CHARTS_FAILURE",

    GET_CHART_DETAILS_REQUEST: "CHARTS/GET_CHART_DETAILS_REQUEST",
	GET_CHART_DETAILS_SUCCESS: "CHARTS/GET_CHART_DETAILS_SUCCESS",
    GET_CHART_DETAILS_FAILURE: "CHARTS/GET_CHART_DETAILS_FAILURE",
    
    CREATE_CHART_REQUEST: "CHARTS/CREATE_CHART_REQUEST",
	CREATE_CHART_SUCCESS: "CHARTS/CREATE_CHART_SUCCESS",
    CREATE_CHART_FAILURE: "CHARTS/CREATE_CHART_FAILURE",

    UPDATE_CHART_REQUEST: "CHARTS/UPDATE_CHART_REQUEST",
	UPDATE_CHART_SUCCESS: "CHARTS/UPDATE_CHART_SUCCESS",
    UPDATE_CHART_FAILURE: "CHARTS/UPDATE_CHART_FAILURE",
    
    DELETE_CHART_REQUEST: "CHARTS/DELETE_CHART_REQUEST",
	DELETE_CHART_SUCCESS: "CHARTS/DELETE_CHART_SUCCESS",
    DELETE_CHART_FAILURE: "CHARTS/DELETE_CHART_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getChartRequest = (queryParams) => ({type: actionTypes.GET_CHARTS_REQUEST, queryParams});
export const getChartFailure = (error) => ({type: actionTypes.GET_CHARTS_FAILURE, error});
export const getChartSuccess = (charts) => ({type: actionTypes.GET_CHARTS_SUCCESS, charts});

export const getChartDetailsRequest = (chartId) => ({type: actionTypes.GET_CHART_DETAILS_REQUEST, chartId});
export const getChartDetailsFailure = (error) => ({type: actionTypes.GET_CHART_DETAILS_FAILURE, error});
export const getChartDetailsSuccess = (chartDetails) => ({type: actionTypes.GET_CHART_DETAILS_SUCCESS, chartDetails});

export const createChartRequest = (data) => ({type: actionTypes.CREATE_CHART_REQUEST, data});
export const createChartFailure = (error) => ({type: actionTypes.CREATE_CHART_FAILURE, error});
export const createChartSuccess = (chart) => ({type: actionTypes.CREATE_CHART_SUCCESS, chart});

export const updateChartRequest = (chartId, data) => ({type: actionTypes.UPDATE_CHART_REQUEST, chartId, data});
export const updateChartFailure = (error) => ({type: actionTypes.UPDATE_CHART_FAILURE, error});
export const updateChartSuccess = (chart) => ({type: actionTypes.UPDATE_CHART_SUCCESS, chart});

export const deleteChartRequest = (chartId) => ({type: actionTypes.DELETE_CHART_REQUEST, chartId});
export const deleteChartFailure = (error) => ({type: actionTypes.DELETE_CHART_FAILURE, error});
export const deleteChartSuccess = () => ({type: actionTypes.DELETE_CHART_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getChartRequest,
    getChartFailure,
    getChartSuccess,

    getChartDetailsRequest,
    getChartDetailsFailure,
    getChartDetailsSuccess,

    createChartRequest,
    createChartFailure,
    createChartSuccess,

    updateChartRequest,
    updateChartFailure,
    updateChartSuccess,

    deleteChartRequest,
    deleteChartFailure,
    deleteChartSuccess,

};

export default AuthActions;
