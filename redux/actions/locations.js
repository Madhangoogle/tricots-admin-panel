export const actionTypes = {
	CLEAR_ERROR: "LOCATIONS/CLEAR_ERROR",
	STATE_RESET: "LOCATIONS/STATE_RESET",

	GET_LOCATIONS_REQUEST: "LOCATIONS/GET_LOCATIONS_REQUEST",
	GET_LOCATIONS_SUCCESS: "LOCATIONS/GET_LOCATIONS_SUCCESS",
    GET_LOCATIONS_FAILURE: "LOCATIONS/GET_LOCATIONS_FAILURE",

    GET_LOCATION_DETAILS_REQUEST: "LOCATIONS/GET_LOCATION_DETAILS_REQUEST",
	GET_LOCATION_DETAILS_SUCCESS: "LOCATIONS/GET_LOCATION_DETAILS_SUCCESS",
    GET_LOCATION_DETAILS_FAILURE: "LOCATIONS/GET_LOCATION_DETAILS_FAILURE",
    
    CREATE_LOCATION_REQUEST: "LOCATIONS/CREATE_LOCATION_REQUEST",
	CREATE_LOCATION_SUCCESS: "LOCATIONS/CREATE_LOCATION_SUCCESS",
    CREATE_LOCATION_FAILURE: "LOCATIONS/CREATE_LOCATION_FAILURE",

    UPDATE_LOCATION_REQUEST: "LOCATIONS/UPDATE_LOCATION_REQUEST",
	UPDATE_LOCATION_SUCCESS: "LOCATIONS/UPDATE_LOCATION_SUCCESS",
    UPDATE_LOCATION_FAILURE: "LOCATIONS/UPDATE_LOCATION_FAILURE",
    
    DELETE_LOCATION_REQUEST: "LOCATIONS/DELETE_LOCATION_REQUEST",
	DELETE_LOCATION_SUCCESS: "LOCATIONS/DELETE_LOCATION_SUCCESS",
    DELETE_LOCATION_FAILURE: "LOCATIONS/DELETE_LOCATION_FAILURE",
    
    GET_LOCATION_HISTORY_REQUEST: "LOCATIONS/GET_LOCATION_HISTORY_REQUEST",
	GET_LOCATION_HISTORY_SUCCESS: "LOCATIONS/GET_LOCATION_HISTORY_SUCCESS",
    GET_LOCATION_HISTORY_FAILURE: "LOCATIONS/GET_LOCATION_HISTORY_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getLocationsRequest = (queryParams) => ({type: actionTypes.GET_LOCATIONS_REQUEST, queryParams});
export const getLocationsFailure = (error) => ({type: actionTypes.GET_LOCATIONS_FAILURE, error});
export const getLocationsSuccess = (locations) => ({type: actionTypes.GET_LOCATIONS_SUCCESS, locations});

export const getLocationDetailsRequest = (locationId) => ({type: actionTypes.GET_LOCATION_DETAILS_REQUEST, locationId});
export const getLocationDetailsFailure = (error) => ({type: actionTypes.GET_LOCATION_DETAILS_FAILURE, error});
export const getLocationDetailsSuccess = (locationDetails) => ({type: actionTypes.GET_LOCATION_DETAILS_SUCCESS, locationDetails});

export const createLocationRequest = (data) => ({type: actionTypes.CREATE_LOCATION_REQUEST, data});
export const createLocationFailure = (error) => ({type: actionTypes.CREATE_LOCATION_FAILURE, error});
export const createLocationSuccess = (location) => ({type: actionTypes.CREATE_LOCATION_SUCCESS, location});

export const updateLocationRequest = (locationId, data) => ({type: actionTypes.UPDATE_LOCATION_REQUEST, locationId, data});
export const updateLocationFailure = (error) => ({type: actionTypes.UPDATE_LOCATION_FAILURE, error});
export const updateLocationSuccess = (location) => ({type: actionTypes.UPDATE_LOCATION_SUCCESS, location});

export const deleteLocationRequest = (locationId) => ({type: actionTypes.DELETE_LOCATION_REQUEST, locationId});
export const deleteLocationFailure = (error) => ({type: actionTypes.DELETE_LOCATION_FAILURE, error});
export const deleteLocationSuccess = () => ({type: actionTypes.DELETE_LOCATION_SUCCESS });

export const getLocationHistoryRequest = (locationId, queryParams) => ({type: actionTypes.GET_LOCATION_HISTORY_REQUEST, locationId, queryParams});
export const getLocationHistoryFailure = (error) => ({type: actionTypes.GET_LOCATION_HISTORY_FAILURE, error});
export const getLocationHistorySuccess = (locationHistory) => ({type: actionTypes.GET_LOCATION_HISTORY_SUCCESS, locationHistory});

const LocationActions = {
	actionTypes,

	clearError,
	stateReset,

    getLocationsRequest,
    getLocationsFailure,
    getLocationsSuccess,

    getLocationDetailsRequest,
    getLocationDetailsFailure,
    getLocationDetailsSuccess,

    createLocationRequest,
    createLocationFailure,
    createLocationSuccess,

    updateLocationRequest,
    updateLocationFailure,
    updateLocationSuccess,

    deleteLocationRequest,
    deleteLocationFailure,
    deleteLocationSuccess,

    getLocationHistoryRequest,
    getLocationHistoryFailure,
    getLocationHistorySuccess,


};

export default LocationActions;
