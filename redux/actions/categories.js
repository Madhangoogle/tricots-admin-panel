export const actionTypes = {
	CLEAR_ERROR: "CATEGORIES/CLEAR_ERROR",
	STATE_RESET: "CATEGORIES/STATE_RESET",

	GET_CATEGORIES_REQUEST: "CATEGORIES/GET_CATEGORIES_REQUEST",
	GET_CATEGORIES_SUCCESS: "CATEGORIES/GET_CATEGORIES_SUCCESS",
    GET_CATEGORIES_FAILURE: "CATEGORIES/GET_CATEGORIES_FAILURE",

    GET_CATEGORY_DETAILS_REQUEST: "CATEGORIES/GET_CATEGORY_DETAILS_REQUEST",
	GET_CATEGORY_DETAILS_SUCCESS: "CATEGORIES/GET_CATEGORY_DETAILS_SUCCESS",
    GET_CATEGORY_DETAILS_FAILURE: "CATEGORIES/GET_CATEGORY_DETAILS_FAILURE",
    
    CREATE_CATEGORY_REQUEST: "CATEGORIES/CREATE_CATEGORY_REQUEST",
	CREATE_CATEGORY_SUCCESS: "CATEGORIES/CREATE_CATEGORY_SUCCESS",
    CREATE_CATEGORY_FAILURE: "CATEGORIES/CREATE_CATEGORY_FAILURE",

    UPDATE_CATEGORY_REQUEST: "CATEGORIES/UPDATE_CATEGORY_REQUEST",
	UPDATE_CATEGORY_SUCCESS: "CATEGORIES/UPDATE_CATEGORY_SUCCESS",
    UPDATE_CATEGORY_FAILURE: "CATEGORIES/UPDATE_CATEGORY_FAILURE",
    
    DELETE_CATEGORY_REQUEST: "CATEGORIES/DELETE_CATEGORY_REQUEST",
	DELETE_CATEGORY_SUCCESS: "CATEGORIES/DELETE_CATEGORY_SUCCESS",
    DELETE_CATEGORY_FAILURE: "CATEGORIES/DELETE_CATEGORY_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getCategoryRequest = (queryParams) => ({type: actionTypes.GET_CATEGORIES_REQUEST, queryParams});
export const getCategoryFailure = (error) => ({type: actionTypes.GET_CATEGORIES_FAILURE, error});
export const getCategorySuccess = (categories) => ({type: actionTypes.GET_CATEGORIES_SUCCESS, categories});

export const getCategoryDetailsRequest = (categoryId) => ({type: actionTypes.GET_CATEGORY_DETAILS_REQUEST, categoryId});
export const getCategoryDetailsFailure = (error) => ({type: actionTypes.GET_CATEGORY_DETAILS_FAILURE, error});
export const getCategoryDetailsSuccess = (categoryDetails) => ({type: actionTypes.GET_CATEGORY_DETAILS_SUCCESS, categoryDetails});

export const createCategoryRequest = (data) => ({type: actionTypes.CREATE_CATEGORY_REQUEST, data});
export const createCategoryFailure = (error) => ({type: actionTypes.CREATE_CATEGORY_FAILURE, error});
export const createCategorySuccess = (category) => ({type: actionTypes.CREATE_CATEGORY_SUCCESS, category});

export const updateCategoryRequest = (categoryId, data) => ({type: actionTypes.UPDATE_CATEGORY_REQUEST, categoryId, data});
export const updateCategoryFailure = (error) => ({type: actionTypes.UPDATE_CATEGORY_FAILURE, error});
export const updateCategorySuccess = (category) => ({type: actionTypes.UPDATE_CATEGORY_SUCCESS, category});

export const deleteCategoryRequest = (categoryId) => ({type: actionTypes.DELETE_CATEGORY_REQUEST, categoryId});
export const deleteCategoryFailure = (error) => ({type: actionTypes.DELETE_CATEGORY_FAILURE, error});
export const deleteCategorySuccess = () => ({type: actionTypes.DELETE_CATEGORY_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getCategoryRequest,
    getCategoryFailure,
    getCategorySuccess,

    getCategoryDetailsRequest,
    getCategoryDetailsFailure,
    getCategoryDetailsSuccess,

    createCategoryRequest,
    createCategoryFailure,
    createCategorySuccess,

    updateCategoryRequest,
    updateCategoryFailure,
    updateCategorySuccess,

    deleteCategoryRequest,
    deleteCategoryFailure,
    deleteCategorySuccess,

};

export default AuthActions;
