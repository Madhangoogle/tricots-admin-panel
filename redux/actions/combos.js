export const actionTypes = {
	CLEAR_ERROR: "COMBOS/CLEAR_ERROR",
	STATE_RESET: "COMBOS/STATE_RESET",

	GET_COMBOS_REQUEST: "COMBOS/GET_COMBOS_REQUEST",
	GET_COMBOS_SUCCESS: "COMBOS/GET_COMBOS_SUCCESS",
    GET_COMBOS_FAILURE: "COMBOS/GET_COMBOS_FAILURE",

    GET_COMBO_DETAILS_REQUEST: "COMBOS/GET_COMBO_DETAILS_REQUEST",
	GET_COMBO_DETAILS_SUCCESS: "COMBOS/GET_COMBO_DETAILS_SUCCESS",
    GET_COMBO_DETAILS_FAILURE: "COMBOS/GET_COMBO_DETAILS_FAILURE",
    
    CREATE_COMBO_REQUEST: "COMBOS/CREATE_COMBO_REQUEST",
	CREATE_COMBO_SUCCESS: "COMBOS/CREATE_COMBO_SUCCESS",
    CREATE_COMBO_FAILURE: "COMBOS/CREATE_COMBO_FAILURE",

    UPDATE_COMBO_REQUEST: "COMBOS/UPDATE_COMBO_REQUEST",
	UPDATE_COMBO_SUCCESS: "COMBOS/UPDATE_COMBO_SUCCESS",
    UPDATE_COMBO_FAILURE: "COMBOS/UPDATE_COMBO_FAILURE",
    
    DELETE_COMBO_REQUEST: "COMBOS/DELETE_COMBO_REQUEST",
	DELETE_COMBO_SUCCESS: "COMBOS/DELETE_COMBO_SUCCESS",
    DELETE_COMBO_FAILURE: "COMBOS/DELETE_COMBO_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getComboRequest = (queryParams) => ({type: actionTypes.GET_COMBOS_REQUEST, queryParams});
export const getComboFailure = (error) => ({type: actionTypes.GET_COMBOS_FAILURE, error});
export const getComboSuccess = (combos) => ({type: actionTypes.GET_COMBOS_SUCCESS, combos});

export const getComboDetailsRequest = (comboId) => ({type: actionTypes.GET_COMBO_DETAILS_REQUEST, comboId});
export const getComboDetailsFailure = (error) => ({type: actionTypes.GET_COMBO_DETAILS_FAILURE, error});
export const getComboDetailsSuccess = (comboDetails) => ({type: actionTypes.GET_COMBO_DETAILS_SUCCESS, comboDetails});

export const createComboRequest = (data) => ({type: actionTypes.CREATE_COMBO_REQUEST, data});
export const createComboFailure = (error) => ({type: actionTypes.CREATE_COMBO_FAILURE, error});
export const createComboSuccess = (combo) => ({type: actionTypes.CREATE_COMBO_SUCCESS, combo});

export const updateComboRequest = (comboId, data) => ({type: actionTypes.UPDATE_COMBO_REQUEST, comboId, data});
export const updateComboFailure = (error) => ({type: actionTypes.UPDATE_COMBO_FAILURE, error});
export const updateComboSuccess = (combo) => ({type: actionTypes.UPDATE_COMBO_SUCCESS, combo});

export const deleteComboRequest = (comboId) => ({type: actionTypes.DELETE_COMBO_REQUEST, comboId});
export const deleteComboFailure = (error) => ({type: actionTypes.DELETE_COMBO_FAILURE, error});
export const deleteComboSuccess = () => ({type: actionTypes.DELETE_COMBO_SUCCESS });


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getComboRequest,
    getComboFailure,
    getComboSuccess,

    getComboDetailsRequest,
    getComboDetailsFailure,
    getComboDetailsSuccess,

    createComboRequest,
    createComboFailure,
    createComboSuccess,

    updateComboRequest,
    updateComboFailure,
    updateComboSuccess,

    deleteComboRequest,
    deleteComboFailure,
    deleteComboSuccess,

};

export default AuthActions;
