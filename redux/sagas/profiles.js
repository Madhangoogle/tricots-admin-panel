import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {

	getProfileDetailsSuccess, 
	getProfileDetailsFailure,

	updateProfileSuccess,
	updateProfileFailure,
	
	
} from "../actions/profiles";


export function * profileDetailsGet({  }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/profiles/" , {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getProfileDetailsSuccess(response.data));
		} else {
			yield put(getProfileDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getProfileDetailsFailure(error.toString()));
	}
}


export function * profileUpdate({ data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/profiles/" ,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateProfileSuccess(response.data));
		} else {
			yield put(updateProfileFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateProfileFailure(error.toString()));
	}
}

const ProfileSaga = {
	profileDetailsGet,
	profileUpdate,
};

export default ProfileSaga;
