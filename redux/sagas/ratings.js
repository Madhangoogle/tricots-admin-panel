import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getRatingSuccess,
	getRatingFailure,

	getRatingDetailsSuccess, 
	getRatingDetailsFailure,

	createRatingSuccess,
	createRatingFailure,

	updateRatingSuccess,
	updateRatingFailure,
	
	deleteRatingSuccess, 
	deleteRatingFailure, 
	
} from "../actions/ratings";

export function * ratingsGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/ratings${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getRatingSuccess(response.data));
		} else {
			yield put(getRatingFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getRatingFailure(error.toString()));
	}
}

export function * ratingDetailsGet({ ratingId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/ratings/" + ratingId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getRatingDetailsSuccess(response.data));
		} else {
			yield put(getRatingDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getRatingDetailsFailure(error.toString()));
	}
}

export function * ratingCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/ratings", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createRatingSuccess(response.data));
		} else {
			yield put(createRatingFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getRatingFailure(error.toString()));
	}
}

export function * ratingUpdate({ ratingId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/ratings/" + ratingId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateRatingSuccess(response.data));
		} else {
			yield put(updateRatingFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateRatingFailure(error.toString()));
	}
}

export function * ratingDelete({ ratingId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/ratings/" + ratingId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteRatingSuccess(response.data));
		} else {
			yield put(deleteRatingFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteRatingFailure(error.toString()));
	}
}

const RatingSaga = {
	ratingsGet,
	ratingDetailsGet,
	ratingCreate,
	ratingUpdate,
	ratingDelete,
};

export default RatingSaga;
