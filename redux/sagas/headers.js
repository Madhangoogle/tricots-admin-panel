import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getHeaderSuccess,
	getHeaderFailure,

	getHeaderDetailsSuccess, 
	getHeaderDetailsFailure,

	createHeaderSuccess,
	createHeaderFailure,

	updateHeaderSuccess,
	updateHeaderFailure,
	
	deleteHeaderSuccess, 
	deleteHeaderFailure, 
	
} from "../actions/headers";

export function * headersGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/headers${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getHeaderSuccess(response.data));
		} else {
			yield put(getHeaderFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getHeaderFailure(error.toString()));
	}
}

export function * headerDetailsGet({ headerId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/headers/" + headerId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getHeaderDetailsSuccess(response.data));
		} else {
			yield put(getHeaderDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getHeaderDetailsFailure(error.toString()));
	}
}

export function * headerCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/headers", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createHeaderSuccess(response.data));
		} else {
			yield put(createHeaderFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getHeaderFailure(error.toString()));
	}
}

export function * headerUpdate({ headerId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/headers/" + headerId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateHeaderSuccess(response.data));
		} else {
			yield put(updateHeaderFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateHeaderFailure(error.toString()));
	}
}

export function * headerDelete({ headerId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/headers/" + headerId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteHeaderSuccess(response.data));
		} else {
			yield put(deleteHeaderFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteHeaderFailure(error.toString()));
	}
}

const HeaderSaga = {
	headersGet,
	headerDetailsGet,
	headerCreate,
	headerUpdate,
	headerDelete,
};

export default HeaderSaga;
