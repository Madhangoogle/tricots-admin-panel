import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getProductSuccess,
	getProductFailure,

	getProductDetailsSuccess, 
	getProductDetailsFailure,

	createProductSuccess,
	createProductFailure,

	updateProductSuccess,
	updateProductFailure,
	
	deleteProductSuccess, 
	deleteProductFailure, 
	
} from "../actions/products";

export function * productsGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/products${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getProductSuccess(response.data));
		} else {
			yield put(getProductFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getProductFailure(error.toString()));
	}
}

export function * productDetailsGet({ productId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/products/" + productId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getProductDetailsSuccess(response.data));
		} else {
			yield put(getProductDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getProductDetailsFailure(error.toString()));
	}
}

export function * productCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/products", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createProductSuccess(response.data));
		} else {
			yield put(createProductFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getProductFailure(error.toString()));
	}
}

export function * productUpdate({ productId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/products/" + productId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateProductSuccess(response.data));
		} else {
			yield put(updateProductFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateProductFailure(error.toString()));
	}
}

export function * productDelete({ productId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/products/" + productId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteProductSuccess(response.data));
		} else {
			yield put(deleteProductFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteProductFailure(error.toString()));
	}
}

const ProductSaga = {
	productsGet,
	productDetailsGet,
	productCreate,
	productUpdate,
	productDelete,
};

export default ProductSaga;
