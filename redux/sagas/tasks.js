import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getTasksSuccess,
	getTasksFailure,

	createTaskSuccess,
	createTaskFailure,

	updateTaskSuccess,
	updateTaskFailure,
	
	deleteTaskSuccess, 
    deleteTaskFailure, 
    
    getTaskDetailsSuccess, 
    getTaskDetailsFailure
} from "../actions/tasks";

export function * tasksGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/tasks?${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getTasksSuccess(response.data.data));
		} else {
			yield put(getTasksFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getTasksFailure(error.toString()));
	}
}

export function * taskDetailsGet({ taskId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/tasks/" + taskId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getTaskDetailsSuccess(response.data));
		} else {
			yield put(getTaskDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getTaskDetailsFailure(error.toString()));
	}
}

export function * taskCreate({ data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/tasks", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createTaskSuccess(response.data));
		} else {
			yield put(createTaskFailure(response.meta.message));
		}
	} catch (error) {
		yield put(createTaskFailure(error.toString()));
	}
}

export function * taskUpdate({ taskId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/tasks/" + taskId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateTaskSuccess(response.data));
		} else {
			yield put(updateTaskFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateTaskFailure(error.toString()));
	}
}

export function * taskDelete({ taskId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/tasks/" + taskId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteTaskSuccess(response.data));
		} else {
			yield put(deleteTaskFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteTaskFailure(error.toString()));
	}
}

const TaskSaga = {
	tasksGet,
	taskDetailsGet,
	taskCreate,
	taskUpdate,
	taskDelete,
};

export default TaskSaga;
