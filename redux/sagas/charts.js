import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getChartSuccess,
	getChartFailure,

	getChartDetailsSuccess, 
	getChartDetailsFailure,

	createChartSuccess,
	createChartFailure,

	updateChartSuccess,
	updateChartFailure,
	
	deleteChartSuccess, 
	deleteChartFailure, 
	
} from "../actions/charts";

export function * chartsGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/charts${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getChartSuccess(response.data));
		} else {
			yield put(getChartFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getChartFailure(error.toString()));
	}
}

export function * chartDetailsGet({ chartId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/charts/" + chartId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getChartDetailsSuccess(response.data));
		} else {
			yield put(getChartDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getChartDetailsFailure(error.toString()));
	}
}

export function * chartCreate({ data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/charts", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createChartSuccess(response.data));
		} else {
			yield put(createChartFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getChartFailure(error.toString()));
	}
}

export function * chartUpdate({ chartId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/charts/" + chartId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateChartSuccess(response.data));
		} else {
			yield put(updateChartFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateChartFailure(error.toString()));
	}
}

export function * chartDelete({ chartId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/charts/" + chartId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteChartSuccess(response.data));
		} else {
			yield put(deleteChartFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteChartFailure(error.toString()));
	}
}

const ChartSaga = {
	chartsGet,
	chartDetailsGet,
	chartCreate,
	chartUpdate,
	chartDelete,
};

export default ChartSaga;
