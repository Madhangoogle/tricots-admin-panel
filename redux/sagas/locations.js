import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getLocationsSuccess,
	getLocationsFailure,

	createLocationSuccess,
	createLocationFailure,

	updateLocationSuccess,
	updateLocationFailure,
	
	deleteLocationSuccess, 
    deleteLocationFailure, 
    
    getLocationDetailsSuccess, 
	getLocationDetailsFailure,
	
	getLocationHistorySuccess,
	getLocationHistoryFailure
} from "../actions/locations";

export function * locationsGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/locations${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getLocationsSuccess(response.data.data));
		} else {
			yield put(getLocationsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getLocationsFailure(error.toString()));
	}
}

export function * locationDetailsGet({ locationId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/locations/" + locationId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getLocationDetailsSuccess(response.data));
		} else {
			yield put(getLocationDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getLocationDetailsFailure(error.toString()));
	}
}

export function * locationCreate({ data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/locations", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createLocationSuccess(response.data));
		} else {
			yield put(createLocationFailure(response.meta.message));
		}
	} catch (error) {
		yield put(createLocationFailure(error.toString()));
	}
}

export function * locationUpdate({ locationId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/locations/" + locationId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateLocationSuccess(response.data));
		} else {
			yield put(updateLocationFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateLocationFailure(error.toString()));
	}
}

export function * locationDelete({ locationId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/locations/" + locationId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteLocationSuccess(response.data));
		} else {
			yield put(deleteLocationFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteLocationFailure(error.toString()));
	}
}

export function * locationHistoryGet({ locationId, queryParams }){
	
	const state = yield select();
	const accessToken = state.auth.accessToken;

	if (queryParams) {
		queryParams += `&locationId=${locationId}`;
	} else {
		queryParams = `locationId=${locationId}`;
	}

	try{
		const response = yield call(
			request,
			API_URL + `/admin/activities/locations?${queryParams}` ,
			{
				method:"GET",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
			);
			if (response && response.apiStatus === "SUCCESS") {
				yield put(getLocationHistorySuccess(response.data.data));
			} else {
				yield put(getLocationHistoryFailure(response.meta.message));
			}
	}
	catch{
		yield put(getLocationHistoryFailure(error.toString()));
	}

}

const LocationSaga = {
	locationsGet,
	locationDetailsGet,
	locationCreate,
	locationUpdate,
	locationDelete,
	locationHistoryGet,
};

export default LocationSaga;
