import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getCollectionSuccess,
	getCollectionFailure,

	getCollectionDetailsSuccess, 
	getCollectionDetailsFailure,

	createCollectionSuccess,
	createCollectionFailure,

	updateCollectionSuccess,
	updateCollectionFailure,
	
	deleteCollectionSuccess, 
	deleteCollectionFailure, 
	
} from "../actions/collections";

export function * collectionsGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/collections${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getCollectionSuccess(response.data));
		} else {
			yield put(getCollectionFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCollectionFailure(error.toString()));
	}
}

export function * collectionDetailsGet({ collectionId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/collections/" + collectionId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getCollectionDetailsSuccess(response.data));
		} else {
			yield put(getCollectionDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCollectionDetailsFailure(error.toString()));
	}
}

export function * collectionCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/collections", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createCollectionSuccess(response.data));
		} else {
			yield put(createCollectionFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCollectionFailure(error.toString()));
	}
}

export function * collectionUpdate({ collectionId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/collections/" + collectionId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateCollectionSuccess(response.data));
		} else {
			yield put(updateCollectionFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateCollectionFailure(error.toString()));
	}
}

export function * collectionDelete({ collectionId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/collections/" + collectionId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteCollectionSuccess(response.data));
		} else {
			yield put(deleteCollectionFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteCollectionFailure(error.toString()));
	}
}

const CollectionSaga = {
	collectionsGet,
	collectionDetailsGet,
	collectionCreate,
	collectionUpdate,
	collectionDelete,
};

export default CollectionSaga;
