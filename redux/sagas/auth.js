import { call, select, put } from 'redux-saga/effects'
import request from '../../util/request'

import { API_URL } from '../../constants'
import {
	loginSuccess,
	loginFailure,
	logoutSuccess,
} from '../actions/auth'

export function * loginPost ({ username, password }) {
	try {
		const response = yield call(request, API_URL + '/admin/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json;charset=utf-8'
			},
			body: JSON.stringify({
				username,
				password
			})
		})
		if (response && response.token) {
			const accessToken = response.token
			yield put(loginSuccess(accessToken))
		} else {
			yield put(loginFailure(response?.message ? response?.message : 'Unauthorized'))
		}
	} catch (error) {
		yield put(loginFailure('Please check your username and/or password.')) // do this only for login response
		// yield put(loginFailure(error.toString())); // in all other cases use this
	}
}

export function * logoutPost () {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		yield call(request, API_URL + '/Users/logout', {
			method: 'POST',
			headers: {
				Authorization: accessToken
			}
		})
		yield put(logoutSuccess())
	} catch (error) {
		yield put(logoutSuccess())
	}
}



const AuthSaga =  {
	loginPost,
	logoutPost,
}

export default AuthSaga
