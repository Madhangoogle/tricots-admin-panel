import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getFeedbackSuccess,
	getFeedbackFailure,

	
} from "../actions/feedbacks";

export function * feedbacksGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/feedbacks${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getFeedbackSuccess(response.data));
		} else {
			yield put(getFeedbackFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getFeedbackFailure(error.toString()));
	}
}


const FeedbackSaga = {
	feedbacksGet,
};

export default FeedbackSaga;
