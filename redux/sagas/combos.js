import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getComboSuccess,
	getComboFailure,

	getComboDetailsSuccess, 
	getComboDetailsFailure,

	createComboSuccess,
	createComboFailure,

	updateComboSuccess,
	updateComboFailure,
	
	deleteComboSuccess, 
	deleteComboFailure, 
	
} from "../actions/combos";

export function * combosGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/combos${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getComboSuccess(response.data));
		} else {
			yield put(getComboFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getComboFailure(error.toString()));
	}
}

export function * comboDetailsGet({ comboId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/combos/" + comboId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getComboDetailsSuccess(response.data));
		} else {
			yield put(getComboDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getComboDetailsFailure(error.toString()));
	}
}

export function * comboCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/combos", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createComboSuccess(response.data));
		} else {
			yield put(createComboFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getComboFailure(error.toString()));
	}
}

export function * comboUpdate({ comboId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/combos/" + comboId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateComboSuccess(response.data));
		} else {
			yield put(updateComboFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateComboFailure(error.toString()));
	}
}

export function * comboDelete({ comboId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/combos/" + comboId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteComboSuccess(response.data));
		} else {
			yield put(deleteComboFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteComboFailure(error.toString()));
	}
}

const ComboSaga = {
	combosGet,
	comboDetailsGet,
	comboCreate,
	comboUpdate,
	comboDelete,
};

export default ComboSaga;
