import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getWasteBinSuccess,
	getWasteBinFailure,

	getWasteBinHistorySuccess,
	getWasteBinHistoryFailure,

	createWasteBinSuccess,
	createWasteBinFailure,

	getWasteBinDetailsSuccess,
	getWasteBinDetailsFailure,

	updateWasteBinSuccess,
	updateWasteBinFailure,
	
	deleteWasteBinSuccess, 
	deleteWasteBinFailure
} from "../actions/wastebins";

export function * wasteBinGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	console.log(accessToken);
	try {
		const response = yield call(request, API_URL + `/admin/bins${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			console.log("success")
			yield put(getWasteBinSuccess(response.data.data));
		} else {
			yield put(getWasteBinFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getWasteBinFailure(error.toString()));
	}
}

export function * wasteBinCreate({ data }) {
	
	console.log(data);
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/bins", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createWasteBinSuccess(response.data));
		} else {
			yield put(createWasteBinFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getWasteBinFailure(error.toString()));
	}
}

export function * wasteBinGetDetail({wasteBinId}){

	console.log(wasteBinId)
	const state = yield select();
	const accessToken = state.auth.accessToken;;
	try{


		const response = yield call(request, API_URL + "/admin/bins/"+wasteBinId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});

		if (response && response.apiStatus === "SUCCESS") {
			console.log("success")
			yield put(getWasteBinDetailsSuccess(response.data));
		} else {
			yield put(getWasteBinDetailsFailure(response.meta.message));
		}
	} catch(error){
		yield put(getWasteBinDetailsFailure(error.toString()))																																																		
	}
}


export function * wasteBinUpdate({ wasteBinId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/bins/" + wasteBinId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateWasteBinSuccess(response.data));
		} else {
			yield put(updateWasteBinFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateWasteBinFailure(error.toString()));
	}
}

export function * wasteBinDelete({ wasteBinId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/bins/" + wasteBinId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteWasteBinSuccess(response.data));
		} else {
			yield put(deleteWasteBinFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteWasteBinFailure(error.toString()));
	}
}

export function * wasteBinHistoryGet({wasteBinId}) {

	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/activities/bins?binId="+wasteBinId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			console.log("success")
			yield put(getWasteBinHistorySuccess(response.data.data));
		} else {
			yield put(getWasteBinHistoryFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getWasteBinHistoryFailure(error.toString()));
	}
}
const WasteBinSaga = {
	wasteBinGet,
	wasteBinCreate,
	wasteBinUpdate,
	wasteBinDelete,
	wasteBinGetDetail,
	wasteBinHistoryGet
};

export default WasteBinSaga;
