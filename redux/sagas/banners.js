import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getBannerSuccess,
	getBannerFailure,

	getBannerDetailsSuccess, 
	getBannerDetailsFailure,

	createBannerSuccess,
	createBannerFailure,

	updateBannerSuccess,
	updateBannerFailure,
	
	deleteBannerSuccess, 
	deleteBannerFailure, 
	
} from "../actions/banners";

export function * bannersGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/banners${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getBannerSuccess(response.data));
		} else {
			yield put(getBannerFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getBannerFailure(error.toString()));
	}
}

export function * bannerDetailsGet({ bannerId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/banners/" + bannerId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getBannerDetailsSuccess(response.data));
		} else {
			yield put(getBannerDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getBannerDetailsFailure(error.toString()));
	}
}

export function * bannerCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/banners", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createBannerSuccess(response.data));
		} else {
			yield put(createBannerFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getBannerFailure(error.toString()));
	}
}

export function * bannerUpdate({ bannerId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/banners/" + bannerId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateBannerSuccess(response.data));
		} else {
			yield put(updateBannerFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateBannerFailure(error.toString()));
	}
}

export function * bannerDelete({ bannerId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/banners/" + bannerId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteBannerSuccess(response.data));
		} else {
			yield put(deleteBannerFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteBannerFailure(error.toString()));
	}
}

const BannerSaga = {
	bannersGet,
	bannerDetailsGet,
	bannerCreate,
	bannerUpdate,
	bannerDelete,
};

export default BannerSaga;
