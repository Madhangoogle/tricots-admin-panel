import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getStaffSuccess,
	getStaffFailure,

	getStaffHistorySuccess,
	getStaffHistoryFailure,

	getStaffHistoryDetailSuccess,
	getStaffHistoryDetailFailure,

	createStaffSuccess,
	createStaffFailure,

	getStaffDetailsSuccess,
	getStaffDetailsFailure,

	updateStaffSuccess,
	updateStaffFailure,
	
	deleteStaffSuccess, 
	deleteStaffFailure
} from "../actions/staffs";

export function * staffsGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/staffs${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			console.log("success")
			yield put(getStaffSuccess(response.data.data));
		} else {
			yield put(getStaffFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getStaffFailure(error.toString()));
	}
}

export function * staffCreate({ data }) {
	
	console.log(data);
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/staffs", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createStaffSuccess(response.data));
		} else {
			yield put(createStaffFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getStaffFailure(error.toString()));
	}
}

export function * staffGetDetail({StaffId}){

	console.log(StaffId)
	const state = yield select();
	const accessToken = state.auth.accessToken;;
	try{


		const response = yield call(request, API_URL + "/admin/staffs/"+StaffId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});

		if (response && response.apiStatus === "SUCCESS") {
			yield put(getStaffDetailsSuccess(response.data));
		} else {
			yield put(getStaffDetailsFailure(response.meta.message));
		}
	} catch(error){
		yield put(getStaffDetailsFailure(error.toString()))																																																		
	}
}


export function * staffUpdate({ StaffId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/staffs/" + StaffId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateStaffSuccess(response.data));
		} else {
			yield put(updateStaffFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateStaffFailure(error.toString()));
	}
}

export function * staffDelete({ StaffId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/staffs/" + StaffId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteStaffSuccess(response.data));
		} else {
			yield put(deleteStaffFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteStaffFailure(error.toString()));
	}
}

export function * staffHistoryGet({StaffId, queryParams}) {

	if (queryParams) {
		queryParams += `&staffId=${StaffId}`;
	} else {
		queryParams = `staffId=${StaffId}`;
	}

	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/staffHistories?${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			console.log("success")
			yield put(getStaffHistorySuccess(response.data.data));
		} else {
			yield put(getStaffHistoryFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getStaffHistoryFailure(error.toString()));
	}


}

export function * staffHistoryDetailGet({StaffId}) {

	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/staffHistories/"+StaffId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			console.log("success")
			yield put(getStaffHistoryDetailSuccess(response.data));
		} else {
			yield put(getStaffHistoryDetailFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getStaffHistoryDetailFailure(error.toString()));
	}


}

const StaffSaga = {
	staffsGet,
	staffCreate,
	staffUpdate,
	staffDelete,
	staffGetDetail,
	staffHistoryGet,
	staffHistoryDetailGet
};

export default StaffSaga;
