const onNavigate = (router, link = null, asUrl = null, options = null) => {
	if (!router) return false

	if (!link) {
		router.back()
		return true
	}

	if (asUrl) {
		router.push(link, asUrl)
		return true
	}

	router.push(link)
}

export { onNavigate }
