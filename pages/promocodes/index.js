import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import PromocodeActions from "../../redux/actions/promocodes"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Promocodes = () => {
	const dispatch = useDispatch()
	const {error, promocode, promocodes, fetchingPromocodes, fetchPromocodeSuccessful,deletingPromocode,deletePromocodeSuccessful,  updatingPromocode, updatePromocodeSuccessful,} = useSelector(state => state.promocodes)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(PromocodeActions.stateReset({
			fetchingPromocodes : false,
			fetchPromocodeSuccessful : false,

			deletingPromocode:false,
			deletePromocodeSuccessful : false,

			updatingPromocode: false, 
			updatePromocodeSuccessful: false,
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(PromocodeActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(PromocodeActions.getPromocodeRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingPromocodes && fetchPromocodeSuccessful){
			dispatch(PromocodeActions.stateReset({
				fetchPromocodeSuccessful : false
			}))
		}
	},[fetchPromocodeSuccessful])

	useEffect(()=> {
		if(!deletingPromocode && deletePromocodeSuccessful){
			dispatch(PromocodeActions.stateReset({
				deletePromocodeSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Promocode Deleted successfully");
			dispatch(PromocodeActions.getPromocodeRequest())
		}
	},[deletePromocodeSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(PromocodeActions.getPromocodeRequest(queryParams))
	},[searchText])

	useEffect(()=>{
		if(!updatingPromocode && updatePromocodeSuccessful){
			dispatch(PromocodeActions.stateReset({
				updatePromocodeSuccessful : false
			}))
			message.success("Promocode updated successfully");
			dispatch(PromocodeActions.getPromocodeRequest(""))
		}
	},[updatePromocodeSuccessful])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeletePromocode = () => {
		dispatch(PromocodeActions.deletePromocodeRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const handleStatus = (promocodeId, status) => {
		dispatch(PromocodeActions.updatePromocodeRequest(promocodeId, {isEnabled: status}))	
	};


	const columns = [
		{title: "Code",key: "code",dataIndex: "code"},
		{title: "Discount Type",key: "discountType",dataIndex: "discountType", 
			render: discountType => {
				return (
					<div>
						{discountType === "PERCENTAGE" ? 'Percentage' : "Amount"}
					</div>
				)
			}
		},
		{title: "Offer",key: "offer",dataIndex: "offer"},
		{title: "Minimum",key: "min",dataIndex: "min"},
		{title: "Maximum",key: "max",dataIndex: "max"},
		{title: "Customer Type",key: "customerType",dataIndex: "customerType", 
			render: customerType => {
				return (
					<div>
						{customerType === "COD" ? 'COD' : customerType === "ONLINE" ? "Pre-paid" : "Both"}
					</div>
				)
			}
		},
        {title: "Enable (Public View)",
            render: text => {
                if (text.isEnabled) {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-success-button.png")} onClick={() => handleStatus(text._id, false)}/>)
                } else {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer"}} src={require("../../Images/toggle-failure-button.png")} onClick={() => handleStatus(text._id, true)}/>)
                }
            }
        },
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/promocodes/edit/" + text._id)}
					// onView={() => handleNavigation("/promocodes/view/" + text._id)}
					onView={false}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"11"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Promocodes"
						button="Create"
						onClickButton={() => handleNavigation("/promocodes/create")}
					/>
					{/* <FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/> */}
					<TableComponent loading={fetchingPromocodes} data={promocodes} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingPromocode}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeletePromocode}
						handleCancel={handleCancel}
						title="Delete Promocode"
					>
						<p>Are you sure you want to delete promocode ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Promocodes;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
