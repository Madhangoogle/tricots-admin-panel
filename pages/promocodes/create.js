import { useState, useEffect } from "react";
import styled from "styled-components";
import { message, Row, Col, Button ,Select ,Typography ,Input, Upload} from "antd";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader from "../../components/ContainerHeader";
import InputContainer from "../../components/InputContainer";

import PromocodeActions from "../../redux/actions/promocodes"
import { useDispatch, useSelector } from "react-redux";
import Themes from "../../themes/themes";
import { onNavigate } from "../../util/navigation";
import Router from "next/router";
import axios from 'axios';
import {API_URL} from '../../constants';

const { Title } = Typography;

const CreatePromocode = () => {
	const dispatch = useDispatch()
	const {error, creatingPromocode, createPromocodeSuccessful} = useSelector(state => state.promocodes)

    const [accessToken, setAccessToken] = useState("");
	const [name, setName] = useState("");
	const [code, setCode] = useState("");
	const [discountType, setDiscountType] = useState("");
	const [isEnabled, setIsEnabled] = useState(false);
	const [min, setMin] = useState("");
	const [max, setMax] = useState("");
	const [customerType, setCustomerType] = useState("");
	const [offer, setOffer] = useState("");

    useEffect(() => {
        let token = localStorage.getItem('accessToken');
        setAccessToken(token);
    }, [])

	useEffect(()=>{
		dispatch(PromocodeActions.stateReset({
			creatingPromocode : false,
			createPromocodeSuccessful : false,
		}))
	},[])

	useEffect(()=>{
		dispatch(PromocodeActions.clearError())
	},[])

	useEffect(()=>{
		if(error)
			message.error(error)
	},[error])

	useEffect(()=>{
		if(!creatingPromocode && createPromocodeSuccessful){
			dispatch(PromocodeActions.stateReset({
				createPromocodeSuccessful : false
			}))
			message.success("Promocode created successfully");
			onNavigate(Router, "/promocodes");
		}
	},[createPromocodeSuccessful])

	const handleClickCancel = () => {
		onNavigate(Router, "/promocodes");
	};
	const handleClickCreate = () => {
		if(name === "" || min === "" || max === "" || code === ""){
			return message.warn("Please fill all the fields")
		}
        dispatch(PromocodeActions.createPromocodeRequest({ name, code, discountType, customerType, min, max, offer, isEnabled }))
		}
    
	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"11"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title="Create Promocode"
						onClickBackButton={() => Router.back()}
					/>
					<div>
						<Row gutter={24}>
							<Col span={12}>
								<InputContainer
									title="Name"
									value={name}
									onChange={(e) => setName(e.target.value)}
								/>
                                <InputContainer
									title="Minimum Credit"
									value={min}
									onChange={(e) => setMin(e.target.value)}
								/>
							</Col>
							<Col span={12}>
                            <InputContainer
                                        title="Code"
                                        value={code}
                                        onChange={(e) => setCode(e.target.value)}
                                    />
                           

                                <InputContainer
                                        title="Maximum Credit"
                                        value={max}
                                        onChange={(e) => setMax(e.target.value)}
                                    />
								
								
							</Col>
                            <Col span={12}>
                            <Title level={5} className="title">Discount Type</Title>
                            <Select
                                      placeholder=""
                                      className="select"
                                      value={discountType}
                                      onChange={(value) => setDiscountType(value)}
                                      style={{ width: '100%',backgroundColor:"#f0f0f0"}}
                                >
                                        <Option value="PERCENTAGE">Percentage</Option>
                                        <Option value="AMOUNT">Amount</Option>
                                </Select>
                                <InputContainer
                                        title="Offer"
                                        value={offer}
                                        onChange={(e) => setOffer(e.target.value)}
                                    />
                                   
                                    </Col>
                            <Col span={12}>
								<Title level={5} className="title">Customer Type</Title>
								<Select
                                      placeholder=""
                                      className="select"
                                      value={customerType}
                                      onChange={(value) => setCustomerType(value)}
                                      style={{ width: '100%',backgroundColor:"#f0f0f0"}}
                                >
                                        <Option value="COD">COD</Option>
                                        <Option value="ONLINE">Pre-paid</Option>
                                        <Option value="BOTH">Both</Option>
                                </Select>

							</Col>
                            <Col span={24}>
                            <Title level={5} className="title" style={{display: "inline-block"}}>Enable</Title>
                                {isEnabled 
                                    ? <img 
                                        style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} 
                                        onClick={() => {setIsEnabled(false)}} 
                                        src={require("../../Images/toggle-success-button.png")}/>
                                    : <img 
                                        style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
                                        onClick={() => {setIsEnabled(true)}} 
                                        src={require("../../Images/toggle-failure-button.png")}/>
                                }
                            </Col>
						</Row>
						<div className="row">
							<Button className="cancel-btn" onClick={handleClickCancel}>
								Cancel
							</Button>
							<Button loading={creatingPromocode} className="create-btn" onClick={handleClickCreate}>
								Create
							</Button>
						</div>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default CreatePromocode;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		.card-root {
			width: 80%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;

	}
	.title1{
		margin-top:20px;
		margin-left:30px;
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.title{
        margin-top:20px
    }
	.hide{
		display:none;
	}
`;
