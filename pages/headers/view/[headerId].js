import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Input, Row, Col, Typography, Button, Select, Upload } from "antd";
import {MinusCircleOutlined, PlusOutlined  } from '@ant-design/icons';

import TopHeader from "../../../components/TopHeader/TopHeader";
import SideMenu from "../../../components/SideMenu/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader/ContainerHeader";
import InputContainer from "../../../components/InputContainer/InputContainer";

import HeaderActions from "../../../redux/actions/headers"
import CollectionActions from "../../../redux/actions/collections"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router, { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import axios from 'axios';
import { API_URL } from '../../../constants';

const { Title } = Typography;

const EditHeader = () => {
	const router = useRouter()
	const dispatch = useDispatch()

	const { error, headerDetails, updatingHeader, updateHeaderSuccessful, fetchingHeaderDetails, fetchHeaderDetailsSuccessful } = useSelector(state => state.headers)
	const { error: collectionError, collections, fetchingCollections, fetchCollectionSuccessful } = useSelector(state => state.collections)

	const [headerId, setHeaderId] = useState("")
	const [accessToken, setAccessToken] = useState("");
	const [title, setTitle] = useState("");
    const [priority, setPriority] = useState(0);
    const [isEnabled, setIsEnabled] = useState(false);
    const [subMenu, setSubMenu] = useState([]);

	useEffect(() => {
		var queryParams = ""
		dispatch(CollectionActions.getCollectionRequest(queryParams))
	}, [])

	useEffect(() => {
		if (!fetchingCollections && fetchCollectionSuccessful) {
			dispatch(CollectionActions.stateReset({
				fetchCollectionSuccessful: false
			}))
		}
	}, [fetchCollectionSuccessful])


	useEffect(() => {
		let token = localStorage.getItem('accessToken');
		setAccessToken(token);
	}, [])

	useEffect(() => {
		dispatch(HeaderActions.stateReset({
			fetchingHeaderDetails: false,
			fetchHeaderDetailsSuccessful: false,

			updatingHeader: false,
			updateHeaderSuccessful: false,
		}))
	}, [])

	useEffect(() => {
		if (error) {
			message.error(error)
			dispatch(HeaderActions.clearError())
		}
	}, [error])

	useEffect(() => {
		let { headerId } = router.query
		if (headerId) {
			setHeaderId(headerId)
		} else {
			onNavigate(Router, "headers")
		}
	}, [])

	useEffect(() => {
		if (headerId)
			dispatch(HeaderActions.getHeaderDetailsRequest(headerId))
	}, [headerId])

	useEffect(() => {
		if (!fetchingHeaderDetails && fetchHeaderDetailsSuccessful) {
			setTitle(headerDetails.title);
			setPriority(headerDetails.priority);
			setIsEnabled(headerDetails.isEnabled);
			setSubMenu(headerDetails.subMenu);

			dispatch(HeaderActions.stateReset({
				fetchHeaderDetailsSuccessful: false
			}))
		}
	}, [fetchHeaderDetailsSuccessful]);

	useEffect(() => {
		if (!updatingHeader && updateHeaderSuccessful) {
			dispatch(HeaderActions.stateReset({
				updateHeaderSuccessful: false
			}))
			message.success("Header updated successfully");
			onNavigate(Router, "/headers");
		}
	}, [updateHeaderSuccessful])


	const handleClickCancel = () => {
		onNavigate(Router, "/headers");
	};

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"8"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title="View Header"
						onClickBackButton={() => Router.back()}
					/>
					<div>
						<Row gutter={24}>
							<Col span={12}>
								<InputContainer
									title="Title"
									value={title}
									disabled
								/>
							</Col>
							<Col span={12}>
								<InputContainer
									title="Priority"
									value={priority}
									disabled
								/>
							</Col>
							<Col span={24}>
								<Title level={5} className="title" style={{ display: "inline-block" }}>Enable</Title>
								{isEnabled
									? <img
										style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
										src={require("../../../Images/toggle-success-button.png")} />
									: <img
										style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
										src={require("../../../Images/toggle-failure-button.png")} />
								}
							</Col>
							<Col span={24} style={{ marginTop: 30, width: "100%" }}>
								{subMenu.length ? subMenu.map((menu, index) => {
									return (
										<Row gutter={24}>
											<Col span={6}>
												<InputContainer
													title="Name"
													value={menu.name}
													disabled
												/>
											</Col>

											<Col span={6}>
												<Title level={5} className="title">Type</Title>
												<Select
													placeholder=""
													disabled
													className="select"
													value={menu.type}
													style={{ width: '100%', backgroundColor: "#f0f0f0" }}
												>
													 <Option value="bestSale">Best Sale</Option>
													<Option value="collection">Collection</Option>
													<Option value="combo">Combo</Option>
													<Option value="featured">Featured</Option>
													<Option value="newArrival">New Arrival</Option>
													<Option value="wholeSale">Whole Sale</Option>
												</Select>
											</Col>

											{menu.type === "collection" ? (<Col span={6}>
												<Title level={5} className="title">Collection</Title>
												<Select disabled style={{ width: '100%' }} placeholder="Select the collection" value={menu?.collectionId?.name} >
												</Select>
											</Col>) : <Col span={6}></Col>}

											<Col span={4} >
												<Title level={5}  className="title" style={{}}>Enabled</Title>
												{menu.isEnabled
													? <img
														style={{ width: 35, height: 20, cursor: "pointer" }}
														src={require("../../../Images/toggle-success-button.png")} />
													: <img
														style={{ width: 35, height: 20, cursor: "pointer" }}
														src={require("../../../Images/toggle-failure-button.png")} />
												}
											</Col>
											<Col span={2} style={{ marginTop: 50 }}>
												<MinusCircleOutlined  />
											</Col>
										</Row>
									)
								}) : null}
							</Col>

						</Row>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default EditHeader;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		.card-root {
			width: 80%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;
	}	
	.title1{
		margin-top:20px;
		margin-left:30px
	}
	.hide{
		display:none;
	}
	.title{
        margin-top:20px
    }
`;
