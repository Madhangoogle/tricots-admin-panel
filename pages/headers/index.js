import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import HeaderActions from "../../redux/actions/headers"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Headers = () => {
	const dispatch = useDispatch()
	const {error, header, headers, fetchingHeaders, fetchHeaderSuccessful,deletingHeader,deleteHeaderSuccessful, updatingHeader, updateHeaderSuccessful,} = useSelector(state => state.headers)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(HeaderActions.stateReset({
			fetchingHeaders : false,
			fetchHeaderSuccessful : false,

			deletingHeader:false,
			deleteHeaderSuccessful : false,

			updatingHeader: false, 
			updateHeaderSuccessful: false,
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(HeaderActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(HeaderActions.getHeaderRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingHeaders && fetchHeaderSuccessful){
			dispatch(HeaderActions.stateReset({
				fetchHeaderSuccessful : false
			}))
		}
	},[fetchHeaderSuccessful])

	useEffect(()=> {
		if(!deletingHeader && deleteHeaderSuccessful){
			dispatch(HeaderActions.stateReset({
				deleteHeaderSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Header Deleted successfully");
			dispatch(HeaderActions.getHeaderRequest())
		}
	},[deleteHeaderSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(HeaderActions.getHeaderRequest(queryParams))
	},[searchText])

	useEffect(()=>{
		if(!updatingHeader && updateHeaderSuccessful){
			dispatch(HeaderActions.stateReset({
				updateHeaderSuccessful : false
			}))
			message.success("Header updated successfully");
			dispatch(HeaderActions.getHeaderRequest(""))
		}
	},[updateHeaderSuccessful])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteHeader = () => {
		dispatch(HeaderActions.deleteHeaderRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const handleStatus = (headerId, status) => {
		dispatch(HeaderActions.updateHeaderRequest(headerId, {isEnabled: status}))	
	};

	const columns = [
		{title: "Title", dataIndex: "title", key: "title"},
		{title: "Priority",key: "priority",dataIndex: "priority"},
        {title: "Enable (Public View)",
            render: text => {
                if (text.isEnabled) {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-success-button.png")} onClick={() => handleStatus(text._id, false)}/>)
                } else {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer"}} src={require("../../Images/toggle-failure-button.png")} onClick={() => handleStatus(text._id, true)}/>)
                }
            }
        },
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/headers/edit/" + text._id)}
					onView={() => handleNavigation("/headers/view/" + text._id)}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"8"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Headers"
						button="Create"
						onClickButton={() => handleNavigation("/headers/create")}
					/>
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingHeaders} data={headers} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingHeader}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteHeader}
						handleCancel={handleCancel}
						title="Delete Header"
					>
						<p>Are you sure you want to delete header ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Headers;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
