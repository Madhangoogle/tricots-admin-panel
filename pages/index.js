import styled from "styled-components";
import Themes from "../themes/themes";
import React from "react";
import { connect } from "react-redux";
import Router from "next/router";

import { onNavigate } from "../util/navigation";

class Home extends React.Component {
	static async getInitialProps() {
		return {};
	}

	constructor(props) {
		super(props);
		onNavigate(Router, "/dashboard");
	}

	render() {
		return (
			<Wrapper>
				<div>Loading...</div>
			</Wrapper>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		auth: store.auth,
	};
};
const actions = {};

export default connect(mapStateToProps, actions)(Home);

const Wrapper = styled.div``;
