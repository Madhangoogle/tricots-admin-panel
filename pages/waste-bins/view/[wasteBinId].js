import { useEffect, useState } from "react";
import styled from "styled-components"
import {message,Row,Col,Button} from "antd"

import TopHeader from "../../../components/TopHeader/TopHeader"
import SideMenu from "../../../components/SideMenu/SideMenu"
import ContainerHeader from "../../../components/ContainerHeader/ContainerHeader"
import MapComponent from "../../../components/MapComponent/MapComponent"
import InputContainer from "../../../components/InputContainer/InputContainer"

import WasteBinActions from "../../../redux/actions/wastebins"
import Themes from "../../../themes/themes"
import {onNavigate} from "../../../util/navigation"
import Router,{useRouter}from "next/router"
import { useDispatch, useSelector } from "react-redux"

import { Images } from '../../../components/TopHeader/assets/index'


const ViewStaff = () =>{



    const router = useRouter()
    const dispatch = useDispatch()

    var objectURL

    const {error,wasteBinDetails,fetchingWasteBinDetails,fetchWasteBinDetailsSuccessfull} = useSelector(state =>state.wastebins)

    const [wasteBinId,setWasteBinId] = useState("")
    const [wasteBinName,setWasteBinName] = useState("")
    const [wasteBinDescription,setWasteBinDescription] = useState("")
    const [wasteBinQRCode,setWasteBinQRCode] = useState("")

    useEffect(()=>{
		dispatch(WasteBinActions.stateReset({
            
            fetchingWasteBinDetails : false,
			fetchWasteBinDetailsSuccessfull : false,

		}))
	},[])

    useEffect(()=>{
		dispatch(WasteBinActions.clearError())
	},[])

    useEffect(()=>{
		if(error){
            message.error(error)
			dispatch(WasteBinActions.clearError())
        }    
	},[error])

    useEffect(()=>{
        let {wasteBinId} = router.query
        
        if(wasteBinId){
            setWasteBinId(wasteBinId)
        }
        else{
            onNavigate(Router,"waste-bins")
        }
		
    },[])

    useEffect(()=>{
        if(wasteBinId)		
            dispatch(WasteBinActions.getWasteBinDetailsRequest(wasteBinId))
	},[wasteBinId])
    
    useEffect(()=> {
		if(!fetchingWasteBinDetails && fetchWasteBinDetailsSuccessfull){

            setWasteBinName(wasteBinDetails.name);
            setWasteBinDescription(wasteBinDetails.description)
            setWasteBinQRCode(wasteBinDetails.qrCodeUrl)

			dispatch(WasteBinActions.stateReset({
				fetchWasteBinDetailsSuccessfull : false
			}))
		}
	},[fetchWasteBinDetailsSuccessfull])



    const handleClickCancel = () => {
		onNavigate(Router, "/waste-bins");
	};
    
    const downloadFile = async (e) => {

        const linkn = e.target.href
        console.log(linkn);
        const blob = new Blob(
          [ linkn ],
          { type: 'image/jpeg' }
        );
        const href = await URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = href;
        link.download = 'your file name' + '.png';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    
    
    return(
        <Wrapper>
            <TopHeader/>
            <SideMenu selectedKey={"6"}/>
                <div className="container">
                    <div className="card-root">
                    <ContainerHeader
						onClickBackButton={() => Router.back()}
					/>
                        <h1>{wasteBinName}</h1>
                        <h2>Bin Type</h2>
                        <h4>{wasteBinDescription}</h4>
                        <h2>Scan code</h2>
                        <div><img src={wasteBinQRCode} className="qrcode" /></div>
                    <Button className="create-btn" onClick={handleClickCancel}>
                        Go Back
                    </Button>
                    <Button className="create-btn">
                        <a href={wasteBinQRCode} download target="_blank" onClick={(e) => downloadFile(e)} >Download</a>
                    </Button>
                    </div>

                </div>    
        </Wrapper>    
    );
};

export default ViewStaff

const Wrapper = styled.div`

background-color:${Themes.backgroundMain};
height:100vh;
width:100vw;
overflow:scroll;
.container{
    margin-left:256px;
    padding: 20px;
    margin-top: 10vh;
    display:flex;
    justify-content:center;
    align-items:center;
    .card-root{
        width:80%;
        background-color: ${Themes.white};
        padding:40px;
    }
}
.create-btn{
    background-color:${Themes.primary};
    border:none;
    margin:20px;
    width:200px;
    height:40px;
    font-weight:bold;
    font-size:16px;
    border-radius:5px;
    color:${Themes.white}
}
.cancel-btn{
    background-color:${Themes.cancelButton};
    border:none;
    margin:20px;
    width:200px;
    height:40px;
    font-weight:bold;
    font-size:16px;
    border-radius:5px;
    color:${Themes.white};
}
.row{
    display:flex;
    flex-direction:row;
    justify-content:center;
    align-items:center;
}
.qrcode{
    width:150px;
    height:150px;
}

`;