import { useEffect, useState } from "react";
import styled from "styled-components";
import { message,Button} from "antd";

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import WasteBinsAction from "../../redux/actions/wastebins"
import Themes from "../../themes/themes";
import Router, { useRouter } from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";
import WasteBinActions from "../../redux/actions/wastebins";

const WasteBins = () => {

	const dispatch = useDispatch();
	const {error, wasteBins ,fetchingWasteBins ,fetchWasteBinsSuccessfull, deletingWasteBins , deleteWasteBinSuccessfull} = useSelector(state=> state.wastebins)

	const [wasteBinList,setWasteBins] = useState([]);
	const [deleteConfirmationModal,setDeleteModalOpen] = useState(false);
	const [deleteId,setDeleteId] = useState("")
	const [searchText, setSearchText] = useState("")

	useEffect(()=>{
		dispatch (WasteBinsAction.stateReset({
			fetchingWasteBins:false,
			fetchWasteBinsSuccessfull:false,

			deletingWasteBins:false,
			deleteWasteBinSuccessfull:false
		}))
	},[])

	useEffect(()=>{
		dispatch(WasteBinsAction.clearError())
	},[])

	useEffect(()=>{
		if(error)
			message.error(error);		
	},[error])

	useEffect(()=>{
		var queryParams = "";
		dispatch(WasteBinActions.getWasteBinRequest(queryParams))
	},[])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(WasteBinActions.getWasteBinRequest(queryParams))
	},[searchText])

	useEffect(()=>{
		if(!fetchingWasteBins && fetchWasteBinsSuccessfull){

			dispatch(WasteBinsAction.stateReset({
				fetchWasteBinsSuccessfull:false
			}))
		}
	},[fetchWasteBinsSuccessfull])

	useEffect(()=>{
		if(!deletingWasteBins && deleteWasteBinSuccessfull){
			
			dispatch(WasteBinsAction.stateReset({
				deleteWasteBinSuccessfull:false
			}))
			setDeleteModalOpen(false);
			message.success("Waste Bin Deleted Succesfully")
			dispatch(WasteBinActions.getWasteBinRequest())
		}
	},[deleteWasteBinSuccessfull])

	const handleOpenDeleteModal=(_id) =>{
		setDeleteId(_id);
		setDeleteModalOpen(true);
	}

	const handleDeleteWasteBin = ()=>{
		dispatch(WasteBinsAction.deleteWasteBinRequest(deleteId))
	}

	const handleCancel = ()=>{
		setDeleteModalOpen(false);	
	}

	const columns = [
		{title: "Bin name",dataIndex:"name",key:"name"},
		{title: "Bin type",dataIndex:"description",key:"description"},
		{
			title: "History",
			key: "_id",
			render: (text, record) => (
				<Button className="history-btn" type="primary" onClick={()=>handleNavigation("/waste-bins/history/"+text._id)}>
					History
				</Button>
			),
		},
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/waste-bins/edit/"+text._id)}
					onView={() => handleNavigation("/waste-bins/view/"+text._id)}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];


	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"6"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title="Waste Bins" 
						button="Create" 
						onClickButton={()=>handleNavigation("/waste-bins/create")} 
					/>
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingWasteBins} data={wasteBins} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingWasteBins}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteWasteBin}
						handleCancel={handleCancel}
						title="Delete Customer"
					>
						<p>Are you sure you want to delete wastebin ?</p>
					</CenteredModal>
				
				</div>
			</div>
		</Wrapper>
	);
};

export default WasteBins;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

// const columns = [
// 	{
// 		title: "Name",
// 		dataIndex: "name",
// 		key: "name",
// 	},
// 	{
// 		title: "Description",
// 		dataIndex: "description",
// 		key: "description",
// 	},
// 	{
// 		title: "Action",
// 		key: "action",
// 		render: (text, record) => (
// 			<UsualActions
// 				onEdit={() => message.success("Edit is clicked")}
// 				onDelete={() => message.success("Delete is clicked")}
// 				onView={() => message.success("View is clicked")}
// 			/>
// 		),
// 	},
// ];

// const data = [
// 	{
// 		key: "1",
// 		name: "B12",
// 		description: "Bio degradable",
// 	},
// 	{
// 		key: "2",
// 		name: "B12",
// 		description: "Bio degradable",
// 	},
// 	{
// 		key: "3",
// 		name: "B2232",
// 		description: "Bio degradable",
// 	},
// 	{
// 		key: "4",
// 		name: "B122",
// 		description: "Bio degradable",
// 	},
// ];
