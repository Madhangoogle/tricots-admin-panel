import styled from "styled-components";
const { RangePicker } = DatePicker;
import { Button, message, DatePicker, Space , Select, Row } from "antd";




import TopHeader from "../../../components/TopHeader/TopHeader";
import SideMenu from "../../../components/SideMenu/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader/ContainerHeader";
import TableComponent, { ViewAction } from "../../../components/Table/Table";
import Themes from "../../../themes/themes"

import WasteBinActions from "../../../redux/actions/wastebins"
import { useDispatch,useSelector } from "react-redux";
import Router,{ useRouter } from "next/router";
import {onNavigate} from "../../../util/navigation"
import { useEffect, useState } from "react";
import summaryGenerator from "../../../services/summaryGenerator";

const WasteBinHistory = () => {

	const dispatch = useDispatch();
	const router = useRouter();

	const {error,wasteBinHistory,fetchingWasteBinHistory,fetchWasteBinHistorySuccessfulll} = useSelector(state =>state.wastebins)

	const [wasteBinId,setWasteBinId] = useState("");
	// const [historyDate,setHistoryDate] = useState("");
	// const [historyStartTime,setHistoryStartTime] = useState("");
	// const [historyEndTime,setHistoryEndTime] = useState("");
	// const [status,setStatus] = useState("");

	useEffect(()=>{

		dispatch(WasteBinActions.stateReset({

			fetchingWasteBinHistory:false,
			fetchWasteBinHistorySuccessfulll:false

		}))
	},[])

	useEffect(()=>{
		dispatch(WasteBinActions.clearError())
	},[])

	useEffect(()=>{
		if(error){
            message.error(error)
            dispatch(WasteBinActions.clearError())
        }    
	},[error])

	useEffect(()=>{
        let {wasteBinId} = router.query
        
        if(wasteBinId){
            setWasteBinId(wasteBinId)
        }
        else{
            onNavigate(Router,"waste-bins")
        }
		
    },[])


	useEffect(()=>{
        if(wasteBinId)		
            dispatch(WasteBinActions.getWasteBinHistoryRequest(wasteBinId))
	},[wasteBinId])

	useEffect(()=> {
        
		if(!fetchingWasteBinHistory && fetchWasteBinHistorySuccessfulll){
            
            // setHistoryDate(staffHistory.date);
            // setHistoryStartTime(staffHistory.startTime);
            // setHistoryEndTime(staffHistory.endTime);
            // setStatus(staffHistory.status);

            dispatch(WasteBinActions.stateReset({
				fetchWasteBinHistorySuccessfulll : false
			}))
		}
	},[fetchWasteBinHistorySuccessfulll])
	
	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};	

	const columns = [
		{
			title: "Date",
			dataIndex: "scheduledAt",
			key: "scheduledAt",
			render: (text, record) => {
				var res = text.slice(0,10)
				return(res)
			}
		},
		{
			title: "Start Time",
			dataIndex: "time",
			key: "time",
		},
		{
			title: "Staff Name",
			dataIndex: "staffId",
            key: "staffId",
            render: (text, record) => {
				var res = text.firstName+" "+text.lastName
				return(res)
			}
		},
		{
			title: "Status",
			dataIndex: "status",
			key: "status",
			render: (text, record) => {
				if (text == "completed") {
					return ("completed")
				} 
				else if(text=="half_completed") {
					return ("half completed")
				}
				else if(text=="cancelled"){
					return ("cancelled")     
				}
				else{
					return ("replaced")
				}
			},
		},
		
	];


	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"4"} />
			<div className="container">
				<div className="card-root">
				<ContainerHeader
						title="Waste Bin History"
						onClickBackButton={() => Router.back()}
						// button="Create"
						// onClickButton={() => handleNavigation("/locations/create")}
					/>

					<Row justify="space-between" className="filter-row" >
						<RangePicker />
						<Select  placeholder="Status" style={{ width: 180 }} onChange={() =>{}}>
							<Option value="jack">In Progress</Option>
							<Option value="lucy">Completed</Option>
							<Option value="Yiminghe">Cancelled</Option>
							<Option value="Yiminghe">Scheduled</Option>
						</Select>
						<Button type="primary" >Download</Button>					
					</Row>	
					<TableComponent loading={fetchingWasteBinHistory} data={wasteBinHistory} columns={columns} />
				</div>
			</div>
		</Wrapper>
	);
};

export default WasteBinHistory;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
	.filter-row{
		margin-bottom : 30px;
	}
`;


// const data = [
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "completed",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "inprogress",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "completed",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "inprogress",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "cancelled",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "complsvdseted",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "cancelled",
// 	},
// ];