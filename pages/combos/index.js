import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import ComboActions from "../../redux/actions/combos"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Combos = () => {
	const dispatch = useDispatch()
	const {error, combo, combos, fetchingCombos, fetchComboSuccessful,deletingCombo,deleteComboSuccessful, updatingCombo, updateComboSuccessful,} = useSelector(state => state.combos)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(ComboActions.stateReset({
			fetchingCombos : false,
			fetchComboSuccessful : false,

			deletingCombo:false,
			deleteComboSuccessful : false,

			updatingCombo: false, 
			updateComboSuccessful: false,
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(ComboActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(ComboActions.getComboRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingCombos && fetchComboSuccessful){
			dispatch(ComboActions.stateReset({
				fetchComboSuccessful : false
			}))
		}
	},[fetchComboSuccessful])

	useEffect(()=> {
		if(!deletingCombo && deleteComboSuccessful){
			dispatch(ComboActions.stateReset({
				deleteComboSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Combo Deleted successfully");
			dispatch(ComboActions.getComboRequest())
		}
	},[deleteComboSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(ComboActions.getComboRequest(queryParams))
	},[searchText])

	useEffect(()=>{
		if(!updatingCombo && updateComboSuccessful){
			dispatch(ComboActions.stateReset({
				updateComboSuccessful : false
			}))
			message.success("Combo updated successfully");
			dispatch(ComboActions.getComboRequest(""))
		}
	},[updateComboSuccessful])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteCombo = () => {
		dispatch(ComboActions.deleteComboRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const handleStatus = (comboId, name, status) => {
		dispatch(ComboActions.updateComboRequest(comboId, {name, isEnabled: status}))	
	};

	const columns = [
		{title: "Name", dataIndex: "name", key: "name"},
		{title: "Image", dataIndex: "imageId",key: "imageId", 
            render: imageId => (
                <img style={{width: 70, height: 70, borderRadius: 10}} src={imageId.imageUrl}/>
            ) 
        },
		{title: "Items",key: "numberOfItems",dataIndex: "numberOfItems"},
		{title: "Price",key: "defaultPrice",dataIndex: "defaultPrice"},
		{title: "Special Price",key: "specialPrice",dataIndex: "specialPrice"},
        {title: "Enable (Public View)",
            render: text => {
                if (text.isEnabled) {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-success-button.png")} onClick={() => handleStatus(text._id, text.name, false)}/>)
                } else {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer"}} src={require("../../Images/toggle-failure-button.png")} onClick={() => handleStatus(text._id, text.name, true)}/>)
                }
            }
        },
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/combos/edit/" + text._id)}
					onView={() => handleNavigation("/combos/view/" + text._id)}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"5"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Combos"
						button="Create"
						onClickButton={() => handleNavigation("/combos/create")}
					/>
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingCombos} data={combos} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingCombo}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteCombo}
						handleCancel={handleCancel}
						title="Delete Combo"
					>
						<p>Are you sure you want to delete combo ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Combos;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
