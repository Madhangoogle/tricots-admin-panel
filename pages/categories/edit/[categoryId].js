import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Input, Row, Col, Typography, Button , Select, Upload} from "antd";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import TopHeader from "../../../components/TopHeader/TopHeader";
import SideMenu from "../../../components/SideMenu/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader/ContainerHeader";
import InputContainer from "../../../components/InputContainer/InputContainer";

import CategoryActions from "../../../redux/actions/categories"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router,{ useRouter } from "next/router";
import { useDispatch , useSelector} from "react-redux";

import axios from 'axios';
import {API_URL} from '../../../constants';

const { Title } = Typography;

const EditCategory = () => {
	const router = useRouter()	
	const dispatch = useDispatch()

	const {error, categoryDetails, updatingCategory, updateCategorySuccessful, fetchingCategoryDetails, fetchCategoryDetailsSuccessful} = useSelector(state => state.categories)

    const [categoryId, setCategoryId] = useState("")
	const [accessToken, setAccessToken] = useState("");
	const [name, setName] = useState("");
	const [priority, setPriority] = useState(0);
	const [collectionId, setCollectionId] = useState("");
	const [isEnabled, setIsEnabled] = useState(false);
	const [imageUrl, setImageUrl] = useState("");
	const [imageId, setImageId] = useState("");
    const [webLoading, setWebLoading] = useState(false);

    const customRequest = ({
        action,
        data,
        file,
        filename,
        headers,
        onError,
        onProgress,
        onSuccess,
        withCredentials,
    }) => {
        const formData = new FormData();
        if (data) {
            Object.keys(data).forEach((key) => {
                formData.append(key, data[key]);
            });
        }
        formData.append("file", file);
        formData.append("type", "category");
    
        onSuccess(formData);
    
        return {
            abort() {
                // message.error('File Upload Aborted')
            },
        };
    };

    const __func__uploadImage = (data, imageType) => {
        if (imageType == "web") {
			setImageUrl("")
            setWebLoading(true);
        } else if (imageType == "mobile") {
			setMobileImageUrl("")
            setMobileLoading(true);
        }
        axios
            .post(`${API_URL}/admin/thumbnail/upload`, data, {
                "content-type": "multipart/form-data",
                headers: { Authorization: `JWT ${accessToken}`},
            })
            .then((res) => {
                setWebLoading(false);
                setMobileLoading(false);
                if (res.data && res.data.data && res.data.data.imageUrl) {
                    if (imageType === "web") {
                        setImageUrl(res.data.data.imageUrl);
                        setImageId(res.data.data._id);
                    } else if (imageType === "mobile") {
                        setMobileImageUrl(res.data.data.imageUrl);
                        setMobileImageId(res.data.data._id);
                    }
                } else {
                    message.error("Image Upload Failed. Try Again.")
                }
            }).catch((error) => {
                if (imageType == "web") {
                    setWebLoading(false);
                } else if (imageType == "mobile") {
                    setMobileLoading(false);
                }
                message.error("Image Upload Failed. Try Again.")
            });
    };

	useEffect(() => {
        let token = localStorage.getItem('accessToken');
        setAccessToken(token);
    }, [])

	useEffect(()=>{
		dispatch(CategoryActions.stateReset({
			fetchingCategoryDetails : false,
			fetchCategoryDetailsSuccessful : false,

			updatingCategory : false,
			updateCategorySuccessful : false,
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(CategoryActions.clearError())
		}
	},[error])

	useEffect(()=>{
		let {categoryId} = router.query
		if(categoryId){
			setCategoryId(categoryId)
		}else {
			onNavigate(Router, "categories")
		}
	},[])

	useEffect(()=>{
		if(categoryId)
			dispatch(CategoryActions.getCategoryDetailsRequest(categoryId))
	},[categoryId])

	useEffect(() => {
		if(!fetchingCategoryDetails && fetchCategoryDetailsSuccessful) {
            setName(categoryDetails.name);
            setImageUrl(categoryDetails.imageId.imageUrl);
			setImageId(categoryDetails.imageId._id);
            setPriority(categoryDetails.priority);
            setCollectionId(categoryDetails.collectionId);
            setIsEnabled(categoryDetails.isEnabled);

            dispatch(CategoryActions.stateReset({
                fetchCategoryDetailsSuccessful : false
            }))
		}			
	}, [fetchCategoryDetailsSuccessful]);

	useEffect(()=>{
		if(!updatingCategory && updateCategorySuccessful){
			dispatch(CategoryActions.stateReset({
				updateCategorySuccessful : false
			}))
			message.success("Category updated successfully");
			onNavigate(Router, "/categories");
		}
	},[updateCategorySuccessful])


	const handleClickCancel = () => {
		onNavigate(Router, "/categories");
	};
	const handleClickUpdate = () => {
		if (name === "" || priority === "" || type === "" || link === "") {
			return message.warn("Please fill all the fields")	
		}
        dispatch(CategoryActions.updateCategoryRequest(categoryId, {name, imageId, collectionId, priority, isEnabled }))			
	};


	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"3"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title="Edit Category" 
						onClickBackButton={() => Router.back()}
					/>
					<div>
                    <Row gutter={24}>
							<Col span={12}>
								<InputContainer
									title="Name"
									value={name}
									onChange={(e) => setName(e.target.value)}
								/>
                                <InputContainer
									title="Priority"
									value={priority}
									onChange={(e) => setPriority(e.target.value)}
								/>
							</Col>
							<Col span={12}>
                                <InputContainer
                                        title="Link"
                                        value={link}
                                        onChange={(e) => setLink(e.target.value)}
                                    />
								<Title level={5} className="title">Type</Title>
								<Select
                                      placeholder=""
                                      className="select"
                                      value={type}
                                      onChange={(value) => setType(value)}
                                      style={{ width: '100%',backgroundColor:"#f0f0f0"}}
                                >
                                        <Option value="small">Small</Option>
                                        <Option value="large">Large</Option>
                                </Select>

							</Col>
                            <Col span={12}>
                            <Title level={5} className="title">Web Image</Title>
                            <Upload
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                onStart={(file) => {
                                    console.log("onStart", file, file.name);
                                }}
                                onSuccess={(data) => {
                                    __func__uploadImage(data, "web");
                                }}
                                onError={(err) => {
                                    console.log(err);
                                    message.error(`${err} File upload failed.`);
                                }}
                                customRequest={customRequest}
                            >
                                {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%', height: "100%" }} /> : (
                                    <div>
                                        {webLoading ? <LoadingOutlined /> : <PlusOutlined />}
                                        <div style={{ marginTop: 8 }}>Upload</div>
                                  </div>
                                )}
                            </Upload>
                            <Title level={5} className="title">Mobile Image</Title>
                            <Upload
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                onStart={(file) => {
                                    console.log("onStart", file, file.name);
                                }}
                                onSuccess={(data) => {
                                    __func__uploadImage(data, "mobile");
                                }}
                                onError={(err) => {
                                    console.log(err);
                                    message.error(`${err} File upload failed.`);
                                }}
                                customRequest={customRequest}
                            >
                                {mobileImageUrl ? <img src={mobileImageUrl} alt="avatar" style={{ width: '100%', height: "100%"  }} /> : (
                                    <div>
                                        {mobileLoading ? <LoadingOutlined /> : <PlusOutlined />}
                                        <div style={{ marginTop: 8 }}>Upload</div>
                                  </div>
                                )}
                            </Upload>
                            </Col>
                            <Col span={24}>
                            <Title level={5} className="title" style={{display: "inline-block"}}>Enable</Title>
                                {isEnabled 
                                    ? <img 
                                        style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} 
                                        onClick={() => {setIsEnabled(false)}} 
                                        src={require("../../../Images/toggle-success-button.png")}/>
                                    : <img 
                                        style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
                                        onClick={() => {setIsEnabled(true)}} 
                                        src={require("../../../Images/toggle-failure-button.png")}/>
                                }
                            </Col>
						</Row>
						<div className="row">
							<Button className="cancel-btn" onClick={handleClickCancel}>
								Cancel
							</Button>
							<Button loading={updatingCategory} className="create-btn" onClick={handleClickUpdate}>
								Save
							</Button>
						</div>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default EditCategory;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		.card-root {
			width: 80%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;
	}	
	.title1{
		margin-top:20px;
		margin-left:30px
	}
	.hide{
		display:none;
	}
	.title{
        margin-top:20px
    }
`;
