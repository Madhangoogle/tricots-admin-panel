import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import CategoryActions from "../../redux/actions/categories"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Categories = () => {
	const dispatch = useDispatch()
	const {error, categories, fetchingCategories, fetchCategorySuccessful, deletingCategory, deleteCategorySuccessful} = useSelector(state => state.categories)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(CategoryActions.stateReset({
			fetchingCategories : false,
			fetchCategorySuccessful : false,

			deletingCategory:false,
			deleteCategorySuccessful : false
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(CategoryActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(CategoryActions.getCategoryRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingCategories && fetchCategorySuccessful){
			dispatch(CategoryActions.stateReset({
				fetchCategorySuccessful : false
			}))
		}
	},[fetchCategorySuccessful])

	useEffect(()=> {
		if(!deletingCategory && deleteCategorySuccessful){
			dispatch(CategoryActions.stateReset({
				deleteCategorySuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Category Deleted successfully");
			dispatch(CategoryActions.getCategoryRequest())
		}
	},[deleteCategorySuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(CategoryActions.getCategoryRequest(queryParams))
	},[searchText])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteCategory = () => {
		dispatch(CategoryActions.deleteCategoryRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const columns = [
		{title: "Web Image", dataIndex: "imageId",key: "imageUrl", 
            render: imageId => (
                <img style={{width: 70, height: 70, borderRadius: 10}} src={imageId.imageUrl}/>
            ) 
        },
		{title: "Name", dataIndex: "name", key: "name"},
		{title: "Priority",key: "priority",dataIndex: "priority"},
		{title: "Collection",key: "collectionName",dataIndex: "collectionId", render: collectionId => <p>{collectionId.name}</p>},
        {title: "Enable (Public View)",key: "isEnabled",dataIndex: "isEnabled",
            render: isEnabled => {
                if (isEnabled) {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, }} src={require("../../Images/toggle-success-button.png")}/>)
                } else {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, }} src={require("../../Images/toggle-failure-button.png")}/>)
                }
            }
        },
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/categories/edit/" + text._id)}
					// onView={() => handleNavigation("/categories/view/" + text._id)}
					onView={false}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"3"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Categories"
						button="Create"
						onClickButton={() => handleNavigation("/categories/create")}
					/>
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingCategories} data={categories} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingCategory}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteCategory}
						handleCancel={handleCancel}
						title="Delete Category"
					>
						<p>Are you sure you want to delete category ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Categories;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
