import styled from "styled-components";
import { Card, Row, Col, message, Spin, DatePicker, Typography, Progress } from 'antd';
import { MenuFoldOutlined, UserOutlined, FallOutlined, DeleteOutlined, RiseOutlined, LineChartOutlined, DollarCircleOutlined, CheckCircleOutlined, InfoCircleOutlined } from "@ant-design/icons";
const { Text } = Typography;
import TopHeader from "../components/TopHeader";
import SideMenu from "../components/SideMenu";

import MapComponent from "../components/MapComponent";

import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import DashboardActions from "../redux/actions/dashboard"
import { useRouter } from "next/router";

import moment from 'moment';

const Dashboard = () => {
	const dispatch = useDispatch()
	const router = useRouter()
	const { error, dashboard, fetchingDashboard, fetchDashboardSuccessful, } = useSelector(state => state.dashboard)
	const [selectedLocation, setSelectedLocation] = useState()
	const [selectedDate, setSelectedDate] = useState("")
	const [loading, setLoading] = useState(false)
	useEffect(() => {
		dispatch(DashboardActions.stateReset({
			fetchingDashboard: false,
			fetchDashboardSuccessful: false,
		}))
	}, [])

	useEffect(() => {
		dispatch(DashboardActions.clearError())
	}, [])

	useEffect(() => {
		if (error) {
			message.error(error)
			dispatch(DashboardActions.clearError())
		}
	}, [error])

	useEffect(() => {
		dispatch(DashboardActions.getDashboardRequest(""))
	}, [])

	useEffect(() => {
		if (!fetchingDashboard && fetchDashboardSuccessful) {
			dispatch(DashboardActions.stateReset({
				fetchDashboardSuccessful: false
			}))
			setLoading(false)
		}
	}, [fetchDashboardSuccessful])

	const onChangeDate = (date) => {
		setSelectedDate(date)
		var queryParams = "?date=" + moment(date).format('YYYY-MM-DD').toString()
		dispatch(DashboardActions.getDashboardRequest(queryParams))
		setLoading(true)
	}


	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"1"} />
			<div className="container">
				<div className="card-root">
					<Text style={{ marginBottom: "3%", display: 'block', fontSize: 20 }} strong>Overview </Text>
					<div style={{ display: "flex", justifyContent: "space-between" }} >

						<Card.Grid style={{
							width: '30%',
							// height: '15%',
							textAlign: 'center',
							// marginLeft: 300,
							// marginTop: 100,
							background: "#1b2ba1",
							borderRadius: "10px"
						}}>
							<Row>
								<Col span={10}>
									<UserOutlined style={{ color: "white", fontSize: "40px" }} />
								</Col>
								<Col span={14}>
									<Row>
										<Col span={24} style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>Total Customers</Col>
									</Row>
									<Row style={{ marginTop: 7 }}>
										{<Col span={24} style={{ color: "white", fontSize: 20, fontWeight: "bold" }}>
											{dashboard?.customerCount}
										</Col>}
									</Row>
								</Col>
							</Row>
						</Card.Grid>

						<Card.Grid style={{
							width: '30%',
							// height: '15%',
							textAlign: 'center',
							// marginLeft: 100,
							// marginTop: 100,
							background: "#058046",
							borderRadius: "10px"
						}}

						>
							<Row>
								<Col span={10}>
									<MenuFoldOutlined style={{ color: "white", fontSize: "40px" }} />
								</Col>
								<Col span={14}>
									<Row>
										<Col span={24} style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>Delivered Orders</Col>
									</Row>
									<Row style={{ marginTop: 7 }}>
										{<Col span={24} style={{ color: "white", fontSize: 20, fontWeight: "bold" }}>
											{dashboard?.totalOrderCount}
										</Col>}
									</Row>
								</Col>
							</Row>
						</Card.Grid>

						<Card.Grid style={{
							width: '30%',
							// height: '15%',
							textAlign: 'center',
							// marginLeft: 100,
							// marginTop: 100,
							background: "#db1d1d",
							borderRadius: "10px"
						}}>
							<Row>
								<Col span={10}>
									<FallOutlined style={{ color: "white", fontSize: "40px" }} />
								</Col>
								<Col span={14}>
									<Row>
										<Col span={24} style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>RTO</Col>
									</Row>
									<Row style={{ marginTop: 7 }}>
										{<Col span={24} style={{ color: "white", fontSize: 20, fontWeight: "bold" }}>
											{dashboard?.totalRTOCount}
										</Col>}
									</Row>
								</Col>
							</Row>
						</Card.Grid>
					</div>

					<div style={{ marginTop: "3%", }}>
						<Text style={{ fontSize: 20, display: 'inline' }} strong>Payment </Text>
					</div>

					<div style={{ display: "flex", justifyContent: "space-between", marginTop: "3%" }} >

						<Card.Grid style={{
							width: '30%',
							// height: '15%',
							textAlign: 'center',
							// marginLeft: 300,
							// marginTop: 100,
							// background: "#1b2ba1",
							borderRadius: "10px"
						}}>
							<Row>
								<Col span={10}>
									<DollarCircleOutlined style={{ color: "#1b2ba1", fontSize: "40px" }} />
								</Col>
								<Col span={14}>
									<Row>
										<Col span={24} style={{ color: "black", fontSize: 16, fontWeight: "bold" }}>Revenue</Col>
									</Row>
									<Row style={{ marginTop: 7 }}>
										{<Col span={24} style={{ color: "black", fontSize: 20, fontWeight: "bold" }}>
											{dashboard?.totalPrice}
										</Col>}
									</Row>
								</Col>
							</Row>
						</Card.Grid>
						<Card.Grid style={{
							width: '30%',
							// height: '15%',
							textAlign: 'center',
							// marginLeft: 300,
							// marginTop: 100,
							// background: "#1b2ba1",
							borderRadius: "10px"
						}}>
							<Row>
								<Col span={10}>
									<CheckCircleOutlined style={{ color: "#058046", fontSize: "40px" }} />
								</Col>
								<Col span={14}>
									<Row>
										<Col span={24} style={{ color: "black", fontSize: 16, fontWeight: "bold" }}>Completed Payment</Col>
									</Row>
									<Row style={{ marginTop: 7 }}>
										{<Col span={24} style={{ color: "black", fontSize: 20, fontWeight: "bold" }}>
											{dashboard?.completedPaymentCount}
										</Col>}
									</Row>
								</Col>
							</Row>
						</Card.Grid>
						<Card.Grid style={{
							width: '30%',
							// height: '15%',
							textAlign: 'center',
							// marginLeft: 300,
							// marginTop: 100,
							// background: "#1b2ba1",
							borderRadius: "10px"
						}}>
							<Row>
								<Col span={10}>
									<InfoCircleOutlined style={{ color: "#deda09", fontSize: "40px" }} />
								</Col>
								<Col span={14}>
									<Row>
										<Col span={24} style={{ color: "black", fontSize: 16, fontWeight: "bold" }}>Pending Payment</Col>
									</Row>
									<Row style={{ marginTop: 7 }}>
										{<Col span={24} style={{ color: "black", fontSize: 20, fontWeight: "bold" }}>
											{dashboard?.pendingPaymentCount}
										</Col>}
									</Row>
								</Col>
							</Row>
						</Card.Grid>
					</div>

					{!loading ?
						<div>
							<div style={{ marginTop: "3%", }}>
								<Text style={{ fontSize: 20, display: 'inline' }} strong> Report </Text>
								<DatePicker value={selectedDate} style={{ float: 'right', display: 'inline' }} onChange={onChangeDate} />
							</div>

							<div style={{ display: "flex", justifyContent: "space-between", marginTop: "3%" }} >

								<Card.Grid style={{
									width: '30%',
									// height: '15%',
									textAlign: 'center',
									// marginLeft: 300,
									// marginTop: 100,
									// background: "#1b2ba1",
									borderRadius: "10px"
								}}>
									<Row>
										<Col span={10}>
											<Progress type="circle" width={70} strokeColor={"#d5db2a"} percent={((dashboard?.todayOrderedCount / dashboard?.todayOrderCount) * 100)} format={percent => `${isNaN(parseFloat(percent)) ? 0 : percent.toFixed(2)} `} />
										</Col>
										<Col span={14}>
											<Row>
												<Col span={24} style={{ color: "#d5db2a", fontSize: 16, fontWeight: "bold" }}>Pending Orders</Col>
											</Row>
											<Row style={{ marginTop: 7 }}>
												{<Col span={24} style={{ color: "#d5db2a", fontSize: 20, fontWeight: "bold" }}>
													{dashboard?.todayOrderedCount}
												</Col>}
											</Row>
										</Col>
									</Row>
								</Card.Grid>

								<Card.Grid style={{
									width: '30%',
									// height: '15%',
									textAlign: 'center',
									// marginLeft: 100,
									// marginTop: 100,
									// background: "#db1d1d",
									borderRadius: "10px"
								}}

								>
									<Row>
										<Col span={10}>
											<Progress type="circle" width={70} strokeColor={"#3921d9"} percent={((dashboard?.todayPackedCount / dashboard?.todayOrderCount) * 100)} format={percent => `${isNaN(parseFloat(percent)) ? 0 : percent.toFixed(2)} `} />
										</Col>
										<Col span={14}>
											<Row>
												<Col span={24} style={{ color: "#3921d9", fontSize: 16, fontWeight: "bold" }}>Packed</Col>
											</Row>
											<Row style={{ marginTop: 7 }}>
												{<Col span={24} style={{ color: "#3921d9", fontSize: 20, fontWeight: "bold" }}>
													{dashboard?.todayPackedCount}
												</Col>}
											</Row>
										</Col>
									</Row>
								</Card.Grid>

								<Card.Grid style={{
									width: '30%',
									// height: '15%',
									textAlign: 'center',
									// marginLeft: 100,
									// marginTop: 100,
									// background: "#058046",
									borderRadius: "10px"
								}}>
									<Row>
										<Col span={10}>
											<Progress type="circle" width={70} strokeColor={"#b620c7"} percent={((dashboard?.todayShippedCount / dashboard?.todayOrderCount) * 100)} format={percent => `${isNaN(parseFloat(percent)) ? 0 : percent.toFixed(2)} `} />
										</Col>
										<Col span={14}>
											<Row>
												<Col span={24} style={{ color: "#b620c7", fontSize: 16, fontWeight: "bold" }}>Shipment</Col>
											</Row>
											<Row style={{ marginTop: 7 }}>
												{<Col span={24} style={{ color: "#b620c7", fontSize: 20, fontWeight: "bold" }}>
													{dashboard?.todayShippedCount}
												</Col>}
											</Row>
										</Col>
									</Row>
								</Card.Grid>
							</div>
							<div style={{ display: "flex", justifyContent: "space-between", marginTop: "1%" }} >
								<Card.Grid style={{
									width: '30%',
									// height: '15%',
									textAlign: 'center',
									// marginLeft: 100,
									// marginTop: 100,
									// background: "#058046",
									borderRadius: "10px"
								}}>
									<Row>
										<Col span={10}>
											<Progress type="circle" width={70} strokeColor={"#1dbf2a"} percent={((dashboard?.todayDeliveredCount / dashboard?.todayOrderCount) * 100)} format={percent => `${isNaN(parseFloat(percent)) ? 0 : percent.toFixed(2)} `} />
										</Col>
										<Col span={14}>
											<Row>
												<Col span={24} style={{ color: "#1dbf2a", fontSize: 16, fontWeight: "bold" }}>Delivered</Col>
											</Row>
											<Row style={{ marginTop: 7 }}>
												{<Col span={24} style={{ color: "#1dbf2a", fontSize: 20, fontWeight: "bold" }}>
													{dashboard?.todayDeliveredCount}
												</Col>}
											</Row>
										</Col>
									</Row>
								</Card.Grid>
								<Card.Grid style={{
									width: '30%',
									// height: '15%',
									textAlign: 'center',
									// marginLeft: 100,
									// marginTop: 100,
									// background: "#058046",
									borderRadius: "10px"
								}}>
									<Row>
										<Col span={10}>
											<Progress type="circle" width={70} strokeColor={"#de0b2b"} percent={((dashboard?.todayCancelledCount / dashboard?.todayOrderCount) * 100)} format={percent => `${isNaN(parseFloat(percent)) ? 0 : percent.toFixed(2)} `} />
										</Col>
										<Col span={14}>
											<Row>
												<Col span={24} style={{ color: "#de0b2b", fontSize: 16, fontWeight: "bold" }}>Cancelled</Col>
											</Row>
											<Row style={{ marginTop: 7 }}>
												{<Col span={24} style={{ color: "#de0b2b", fontSize: 20, fontWeight: "bold" }}>
													{dashboard?.todayCancelledCount}
												</Col>}
											</Row>
										</Col>
									</Row>
								</Card.Grid>

								<Card.Grid style={{
									width: '30%',
									// height: '15%',
									textAlign: 'center',
									// marginLeft: 100,
									// marginTop: 100,
									// background: "#058046",
									borderRadius: "10px"
								}}>
									<Row>
										<Col span={10}>
											<Progress type="circle" width={70} strokeColor={"#de5b09"} percent={((dashboard?.todayRTOCount / dashboard?.todayOrderCount) * 100)} format={percent => `${isNaN(parseFloat(percent)) ? 0 : percent.toFixed(2)} `} />
										</Col>
										<Col span={14}>
											<Row>
												<Col span={24} style={{ color: "#de5b09", fontSize: 16, fontWeight: "bold" }}>RTO</Col>
											</Row>
											<Row style={{ marginTop: 7 }}>
												{<Col span={24} style={{ color: "#de5b09", fontSize: 20, fontWeight: "bold" }}>
													{dashboard?.todayRTOCount}
												</Col>}
											</Row>
										</Col>
									</Row>
								</Card.Grid>
							</div>
						</div>
						: <Spin style={{marginLeft: "50%", marginTop: "10%"}} />}
				</div>
			</div>




		</Wrapper>
	);
};

export default Dashboard;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100%;
	width: 100%;
	/* overflow: scroll; */
	.container {
		margin-left: 256px;
		padding: 20px;
		height :100%;
		margin-top: 10vh;
		.card-root {
			background-color: #fff;
			padding: 20px;
			height :100%
			
			/* justify-content : space-between; */
			/* display:flex; */
		}
	}
	.map-overlay{
		position : absolute;
		/* height : 200px; */
		width : 200px;
		margin-left : 10px;
		margin-top : -60px;
		border : 1px solid #000;
		background-color : #FFF;
		z-index : 1000;
		/* justify-content : space-between;
		align-items :center;
		display : flex;
		flex-direction : column; */
		padding : 20px;
	}
`;
