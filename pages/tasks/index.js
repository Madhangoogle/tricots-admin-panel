import styled from "styled-components";
import { DatePicker, Button, Modal, message } from "antd";

import TasksActions from "../../redux/actions/tasks"
import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { TaskActions } from "../../components/Table";
import Themes from "../../themes/themes";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector} from "react-redux"
import moment from "moment"

const Tasks = () => {
	const {error, tasks, fetchingTasks, fetchTaskSuccessful,creatingTask,createTaskSuccessful,} = useSelector(state => state.tasks)
	const [openCopy, setOpenCopy] = useState(false)
	const [date, setDate] = useState()
	const router = useRouter()
	const dispatch = useDispatch()
	const [Tasks, setTasks] = useState([]);

	const [searchText, setSearchText] = useState("")
	const [dateFilter, setDateFilter] = useState()
	const [selectedValue, setSelectedValue] = useState()
	const options = [
		{name : "All", value: ""}, 
		{name : "Scheduled", value: "scheduled"}, 
		{name : "In Progress", value: "in_progress"},
		{name : "Completed", value: "completed"},
		{name : "Cancelled", value: "cancelled"},
	]

	useEffect(() => {
		dispatch(TasksActions.stateReset({
			fetchingTasks:false,
			fetchTaskSuccessful:false,
			creatingTask: false,
			createTaskSuccessful: false,
		}))
	}, []);

	useEffect(() => {
		if(error){
			message.error(error)
			dispatch(TasksActions.clearError())
		}
	}, [error]);
	
	useEffect(()=>{
		var queryParams = "";
		dispatch(TasksActions.getTasksRequest(queryParams))
	},[])

	useEffect(() => {
		if(!fetchingTasks && fetchTaskSuccessful){
			dispatch(TasksActions.stateReset({
				fetchTaskSuccessful: false
			}))
			setTasks(tasks)
		}
	}, [fetchTaskSuccessful]);

	useEffect(()=>{
		var queryParams = "";
		if (searchText) {
			queryParams += `&search=${searchText}`;
		}

		if (dateFilter) {
			var startDate = moment(dateFilter[0]).format("YYYY-MM-DD");
			var endDate = moment(dateFilter[1]).format("YYYY-MM-DD");
			queryParams += `&startDate=${startDate}&endDate=${endDate}`;
		}

		if (selectedValue) {
			queryParams += `&status=${selectedValue}`;
		}

		if (queryParams) {
			queryParams = queryParams.substring(1);	
		}

		dispatch(TasksActions.getTasksRequest(queryParams))
	},[searchText, dateFilter, selectedValue])


	const columns = [
		{
			title: "Date",
			render: (text, record) => {
				return <div>{moment(text.scheduledAt).format("DD-MM-YYYY")}</div>
			},
			key: "_id",
		},
		{
			title: "Staff Name",
			render: (text, record) => {
				return <div>{text?.staffId?.firstName + " " + text?.staffId?.lastName }</div>
			},			
			key: "_id",
		},
		{
			title: "Status",
			key: "_id",
			render: (text, record) => {
				console.log(text)
				if (text == "completed") {
					return text;
				} else {
					return (
						<Button type="primary" className="create-btn" onClick={()=> handleNavigation("/tasks/track/"+ text._id)}>
							Track
						</Button>
					);
				}
			},
		},
		{
			title: "Action",
			key: "action",
			render: (text, record) => (
				<TaskActions
					// onEdit={() => message.success("Edit is clicked")}
					// onDelete={() => message.success("Delete is clicked")}
					onCopy={() => handleOpenCopy(text)}
				/>
			),
		},
	];
	const handleOpenCopy = (text)=>{
		setOpenCopy(text)
	}
	const handleCloseModal = ()=>{
		setOpenCopy(false)
	}

	const handleNavigation = (route) =>{
		router.push(route)
	}

	const handleSubmit = () => {
		console.log(openCopy)
		let taskItems = openCopy.taskItems.map(taskItem => {
			return {
				priority: taskItem.priority,
				locationId: taskItem.locationId?._id,
				time:moment(taskItem.time).format("HH:mm") ,
			}
		})
		dispatch(
			TasksActions.createTaskRequest({
				staffId: openCopy?.staffId?._id,
				scheduledAt: moment(date).format("YYYY-MM-DD"),
				taskItems: taskItems,
			})
		);
	};
	useEffect(() => {
		if (!creatingTask && createTaskSuccessful) {
			dispatch(
				TasksActions.stateReset({
					createTaskSuccessful: false,
				})
			);
			var queryParams = "";
			dispatch(TasksActions.getTasksRequest(queryParams))
			message.success("Task assigned successfully");
			handleCloseModal()
		}
	}, [createTaskSuccessful]);

	return (
		<Wrapper>
			<TopHeader />
			<Modal title="Select Date" visible={openCopy} onOk={handleSubmit} onCancel={handleCloseModal}>
				<DatePicker onChange={(date)=>setDate(date)} />
			</Modal>
			<SideMenu selectedKey={"5"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader
						button="Assign"
						title="Tasks"
						onClickButton={()=> handleNavigation("/tasks/assign")}
					/>
					<FilterOptions 
						datePicker
						select
						search	
						options={options}
						onChangeDate={(value) => setDateFilter(value)}
						date={dateFilter}
						selectedValue={selectedValue}
						inputValue={searchText}
						selectkey={"name"}
						selectvalue={"value"}
						placeholder={"Search"}
						onChangeSelect={(value) => setSelectedValue(value)}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={false} data={Tasks} columns={columns} />
				</div>
			</div>
		</Wrapper>
	);
};

export default Tasks;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;



const data = [
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "completed",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "sdc",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "completed",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "complsdveted",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "comdfpleted",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "complsvdseted",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "compfdleted",
	},
];
