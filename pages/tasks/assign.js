import React, { useEffect } from "react";
import styled from "styled-components";
import {
	DatePicker,
	message,
	Button,
	Modal,
	Col,
	Row,
	Select,
	Input,
	TimePicker,
} from "antd";
import { CloseCircleOutlined } from "@ant-design/icons";

import TasksActions from "../../redux/actions/tasks";
import StaffsActions from "../../redux/actions/staffs";
import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader from "../../components/ContainerHeader";
import Themes from "../../themes/themes";
import MapComponent from "../../components/MapComponent";

import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useState } from "react";
import { useRouter } from "next/router";
import TaskActions from "../../redux/actions/tasks";
import LocationActions from "../../redux/actions/locations";

const { Option } = Select;

const Tasks = () => {
	const {
		error,
		taskDetails,
		creatingTask,
		createTaskSuccessful,
	} = useSelector((state) => state.tasks);
	const {
		error: staffError,
		staffs,
		fetchingStaffs,
		fetchStaffSuccessful,
	} = useSelector((state) => state.staffs);
	const {
		error: locationError,
		locations,
		fetchingLocations,
		fetchLocationSuccessful,
	} = useSelector((state) => state.locations);
	const router = useRouter();
	const dispatch = useDispatch();
	const [markers, setMarkers] = useState([]);
	const [PolyLines, setPolyLines] = useState([]);
	const [InitialCenter, setInitialCenter] = useState();
	const [StaffId, setStaffId] = useState();
	const [TaskItems, setTaskItems] = useState([]);
	const [StaffList, setStaffList] = useState([]);
	const [Date, setDate] = useState();
	const [selectedLocation, setSelectedLocation] = useState();
	const taskId = router.query.taskId;

	const handleNavigation = (route) => {
		router.push(route);
	};

	useEffect(() => {
		dispatch(
			TasksActions.stateReset({
				creatingTask: false,
				createTaskSuccessful: false,
			})
		);
		dispatch(
			StaffsActions.stateReset({
				fetchingStaffs: false,
				fetchStaffSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error || staffError) {
			message.error(error);
			dispatch(TasksActions.clearError());
		}
	}, [error, staffError]);

	useEffect(() => {
		dispatch(StaffsActions.getStaffRequest(""));
		dispatch(LocationActions.getLocationsRequest(""));
	}, []);

	useEffect(() => {
		if (!fetchingStaffs && fetchStaffSuccessful) {
			dispatch(
				StaffsActions.stateReset({
					fetchStaffSuccessful: false,
				})
			);
			setStaffList(staffs);
		}
	}, [fetchStaffSuccessful]);

	useEffect(() => {
		if (!fetchingLocations && fetchLocationSuccessful) {
			dispatch(
				StaffsActions.stateReset({
					fetchLocationSuccessful: false,
				})
			);
			setMarkers(
				locations.map((location) => {
					return {
						position: {
							lat: location.latitude,
							lng: location.longitude,
							name: location.name,
							locationStatus: location.status,
							customerName : location.customerId?.firstName +" " + location.customerId?.lastName,
							_id: location._id,
						},
					};
				})
			);

		}
	}, [fetchLocationSuccessful]);

	useEffect(() => {
		if (!creatingTask && createTaskSuccessful) {
			dispatch(
				StaffsActions.stateReset({
					createTaskSuccessful: false,
				})
			);
			message.success("Task assigned successfully");
			router.push("/tasks");
		}
	}, [createTaskSuccessful]);

	useEffect(() => {
		if (markers[0]) {
			console.log(markers[0].position);
			// setInitialCenter({lat:markers[0]?.position.lat,
			// lng:markers[0]?.position.lng});
		}
	}, [markers]);

	const loadData = async( ) =>{
		let data = await Promise.all(TaskItems.map(ti => {
			return { lat: ti.lat, lng: ti.lng }
		}))
		setPolyLines(data)
	}
	useEffect(()=>{
		if(TaskItems){
			loadData()
		}
	},[TaskItems])

	const handleSubmit = () => {
		let taskItems = TaskItems.map(taskItem => {
			return {
				priority: taskItem.priority,
				locationId: taskItem._id,
				time:moment(taskItem.time).format("HH:mm") 
			}
		})

		console.log(taskItems)
		dispatch(
			TasksActions.createTaskRequest({
				staffId: StaffId,
				scheduledAt: moment(Date).format("YYYY-MM-DD"),
				taskItems: taskItems,
			})
		);
	};

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"5"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title="Tracking Task"
						onClickBackButton={() => Router.back()}
					/>
					<Row gutter={12}>
						<Col span={24} style={{ padding: 25 }}>
							<Row gutter={24}>
								<Col>
									<Select
										placeholder="Select Staff"
										style={{ width: 120 }}
										onChange={(value) => setStaffId(value)}
										value={StaffId}
									>
										{StaffList.map((staff) => {
											return (
												<Option value={staff._id}>
													{staff.firstName + " " + staff.lastName}
												</Option>
											);
										})}
									</Select>
								</Col>
								<Col>
									<DatePicker value={Date} onChange={(date) => setDate(date)} />
								</Col>
							</Row>
						</Col>
						<Col md={12}>
							{TaskItems.map((taskItem) => {
								return (
									<Row
										justify="space-between"
										align="middle"
										style={{ padding: 20 }}
									>
										<TimePicker
											format={"HH:mm"}
											onChange={(time) => {
												let newList = TaskItems.map(ti => {
													if(ti._id === taskItem._id) return {...ti, time :time }
													else return { ...ti  }
												})
												setTaskItems(newList)
											}}
											value={taskItem.time}
										/>
										<Input
											style={{ width: 120 }}
											placeholder="location"
											value={taskItem.name}
											disabled
										/>
										<Input
											style={{ width: 120 }}
											placeholder="priority"
											value={taskItem.priority}
											disabled
										/>
										<CloseCircleOutlined
											onClick={() => {
												setTaskItems(
													TaskItems.filter((tI) => {
														if (
															tI.lat !== taskItem.lat&&
															tI.lng !== taskItem.lng
														)
															return tI;
													})
												);
											}}
										/>
									</Row>
								);
							})}
						</Col>
						<Col md={12}>
							<MapComponent
								initialCenter={{lat : 25.2854, lng :51.5310}}
								markers={markers}
								style={{ minHeight: 450 }}
								onClickMap={(data) => {}}
								polylineData={PolyLines}
								onClickMarker={(data) =>{
									// setTaskItems([
									// 	...TaskItems,
									// 	{ ...data, priority: TaskItems.length + 1 },
									// ])
									setSelectedLocation(data)
								}}
							/>
						{selectedLocation &&
						<div className="map-overlay" >
							<Row justify="center">
								<Col span={24} style={{ marginTop: 5 }} >Name : {selectedLocation.name}</Col>
								<Col span={24} style={{ marginTop: 5 }}>Customer : {selectedLocation.customerName}</Col>
								{/* <Col span={24} style={{ marginTop: 5 }}>Bin : Bin 1</Col> */}
								<Col span={24} style={{ marginTop: 10 }}><Button type="primary" onClick={()=> {
									let already = TaskItems.filter(ti => {
										if(ti._id === selectedLocation._id)
											return ti
									})
									if(already.length ===0) {
										setTaskItems([
											...TaskItems,
											{ ...selectedLocation, priority: TaskItems.length + 1 },
										])
									}
									
								}} >
									Insert
									</Button></Col>
							</Row>
						</div>
						}
						</Col>
					</Row>
					{TaskItems.length !== 0 && (
						<Button className="create-btn" onClick={handleSubmit}>
							Submit
						</Button>
					)}
				</div>
			</div>
		</Wrapper>
	);
};

export default Tasks;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			height: 80vh;
			overflow-y: scroll;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
		color: ${Themes.white};
		margin: 20px;
	}
	.map-overlay{
		position : absolute;
		/* height : 200px; */
		width : 200px;
		margin-left : 10px;
		margin-top : -60px;
		border : 1px solid #000;
		background-color : #FFF;
		z-index : 1000;
		/* justify-content : space-between;
		align-items :center;
		display : flex;
		flex-direction : column; */
		padding : 10px;
	}
`;
