import React, { useEffect } from "react";
import styled from "styled-components";
import {
	DatePicker,
	message,
	Button,
	Modal,
	Col,
	Row,
	Select,
	Input,
	TimePicker,
} from "antd";
import { CloseCircleOutlined } from "@ant-design/icons";

import TasksActions from "../../../redux/actions/tasks";
import StaffsActions from "../../../redux/actions/staffs";
import TopHeader from "../../../components/TopHeader";
import SideMenu from "../../../components/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader";
import Themes from "../../../themes/themes";
import MapComponent from "../../../components/MapComponent";

import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useState } from "react";
import Router,{ useRouter } from "next/router";
import TaskActions from "../../../redux/actions/tasks";

const { Option } = Select;

const Tasks = () => {
	const {
		error,
		taskDetails,
		fetchingTaskDetails,
		fetchTaskDetailsSuccessful,
	} = useSelector((state) => state.tasks);
	const {
		error: staffError,
		staffs,
		fetchingStaffs,
		fetchStaffSuccessful,
	} = useSelector((state) => state.staffs);
	const router = useRouter();
	const dispatch = useDispatch();
	const [markers, setMarkers] = useState([]);
	const [PolyLines, setPolyLines] = useState([]);
	const [InitialCenter, setInitialCenter] = useState();
	const [StaffId, setStaffId] = useState();
	const [TaskItems, setTaskItems] = useState([]);
	const [StaffList, setStaffList] = useState([]);
	const [Date, setDate] = useState();
	const taskId = router.query.taskId;

	const handleNavigation = (route) => {
		router.push(route);
	};

	useEffect(() => {
		dispatch(
			TasksActions.stateReset({
				fetchingTaskDetails: false,
				fetchTaskDetailsSuccessful: false,
			})
		);
		dispatch(
			StaffsActions.stateReset({
				fetchingStaffs: false,
				fetchStaffSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error || staffError) {
			message.error(error);
			dispatch(TasksActions.clearError());
		}
	}, [error, staffError]);

	useEffect(() => {
		dispatch(TasksActions.getTaskDetailsRequest(taskId));
		dispatch(StaffsActions.getStaffRequest());
	}, []);

	useEffect(() => {
		if (!fetchingTaskDetails && fetchTaskDetailsSuccessful) {
			dispatch(
				TasksActions.stateReset({
					fetchTaskDetailsSuccessful: false,
				})
			);
			setStaffId(taskDetails.staffId);
			setDate(moment(taskDetails.scheduledAt));
			setTaskItems(taskDetails.taskItems);
			setMarkers(taskDetails.taskItems.map(ti => {
				return {position: { lat: ti.locationId.latitude, lng: ti.locationId.longitude }}
			}))
			setPolyLines(taskDetails.taskItems.map(ti => {
				return { lat: ti.locationId.latitude, lng: ti.locationId.longitude }
			}))
		}
	}, [fetchTaskDetailsSuccessful]);

	useEffect(() => {
		if (!fetchingStaffs && fetchStaffSuccessful) {
			dispatch(
				StaffsActions.stateReset({
					fetchStaffSuccessful: false,
				})
			);
			setStaffList(staffs);
		}
	}, [fetchStaffSuccessful]);

	useEffect(()=>{
		if(markers[0]){
			console.log(markers[0].position)
			setInitialCenter(markers[0]?.position)
		}
	},[markers])

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"5"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title="Tracking Task"
						onClickBackButton={() => Router.back()}
					/>
					<Row gutter={12}>
						<Col span={24} style={{ padding: 25 }}>
							<Row gutter={24}>
								<Col>
									<Select
										placeholder="Select Staff"
										style={{ width: 120 }}
										onChange={() => {}}
										value={StaffId}
									>
										{StaffList.map((staff) => {
											return (
												<Option value={staff._id}>
													{staff.firstName + " " + staff.lastName}
												</Option>
											);
										})}
									</Select>
								</Col>
								<Col>
									<DatePicker value={Date} onChange={(date) => setDate(date)} />
								</Col>
							</Row>
						</Col>
						<Col md={12}>
							{TaskItems.map((taskItem) => {
								return (
									<Row
										justify="space-between"
										align="middle"
										style={{ padding: 20 }}
									>
										<TimePicker onChange={() => {}} value={moment(taskItem.time, "HH:mm")} />
										<Input
											style={{ width: 120 }}
											placeholder="location"
											value={taskItem.locationId.name}
											disabled
										/>
										<Input
											style={{ width: 120 }}
											placeholder="priority"
											value={taskItem.priority}
											disabled
										/>
										{/* <CloseCircleOutlined
											onClick={() => {
												setTaskItems(
													TaskItems.filter((tI) => {
														if (
															tI.locationId.latitude !==
																taskItem.locationId.latitude &&
															tI.locationId.longitude !==
																taskItem.locationId.longitude
														)
															return tI;
													})
												);
											}}
										/> */}
									</Row>
								);
							})}
						</Col>
						<Col md={12}>
							{InitialCenter && 
								 <MapComponent
								initialCenter={InitialCenter}
								markers={markers}
								style={{ minHeight: 450 }}
								onClickMap={(data) => {}}
								polylineData={PolyLines}
								onClickMarker={(data) =>
									// setSelectedLocations([
										// 	...selectedLocations,
										// 	{ ...data, priority: selectedLocations.length + 1 },
										// ])
										{}
									}
									/>
								}
						</Col>
					</Row>
					{/* {TaskItems.length !== 0 && (
						<Button className="create-btn">Submit</Button>
					)} */}
				</div>
			</div>
		</Wrapper>
	);
};

export default Tasks;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			height: 80vh;
			overflow-y: scroll;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
		color: ${Themes.white};
		margin: 20px;
	}
`;
