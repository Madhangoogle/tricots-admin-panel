import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Input, Row, Col, Typography, Button } from "antd";

import TopHeader from "../../../components/TopHeader";
import SideMenu from "../../../components/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader";
import InputContainer from "../../../components/InputContainer";
import ViewLocationMap from "../../../components/MapComponent";

import CustomerActions from "../../../redux/actions/customers"
import LocationActions from "../../../redux/actions/locations"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router,{ useRouter } from "next/router";
import { useDispatch , useSelector} from "react-redux";

const ViewCustomer = () => {
	const router = useRouter()
	const dispatch = useDispatch()

	const {error, locationDetails, updatingLocation, updateLocationSuccessful, fetchLocationDetailsSuccessful, fetchingLocationDetails} = useSelector(state => state.locations)


	const [locationName, setLocationName] = useState("")
	const [locationId, setLocationId] = useState("")
	const [customerName, setCustomerName] = useState("")
	const [binName, setBinName] = useState("")
	const [location, setLocation] = useState({latitude: 0, longitude: 0})
	const [mapView, setMapLoaded] = useState(false)

	useEffect(()=>{
		dispatch(LocationActions.stateReset({
			fetchLocationDetailsSuccessful: false,
			fetchingLocationDetails: false
		}))
		let {locationId} = router.query
		if(locationId){
			setLocationId(locationId)
		}else {
			onNavigate(Router, "locations")
		}
	},[])


	useEffect(()=>{
		if(locationId){
			dispatch(CustomerActions.getCustomerRequest())
			console.log(locationId)
			dispatch(LocationActions.getLocationDetailsRequest(locationId))
		}
	},[locationId])

	useEffect(()=> {
		if(!fetchingLocationDetails && fetchLocationDetailsSuccessful){
			dispatch(LocationActions.stateReset({
				fetchLocationDetailsSuccessful : false
			}))
			setLocationName(locationDetails.name)
			setLocation({latitude: locationDetails.latitude, longitude: locationDetails.longitude})
			setCustomerName(locationDetails.customerId.firstName + " " + locationDetails.customerId.lastName)
			setBinName(locationDetails.binName)
			setMapLoaded(true)
		}
	},[fetchLocationDetailsSuccessful])

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"3"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title={locationName} 
						onClickBackButton={() => Router.back()}
					/>
					<div>
						<Row gutter={24}>
							<Col span={10}>
								<InputContainer
									disabled
									title="Customer Name"
									value={customerName}
								/>
									<InputContainer
									disabled
									title="Bin Name"
									value={binName}
								/>
							</Col>
							<Col span={14}>
								{mapView &&
									<ViewLocationMap  
									onClickMap={async () =>{}}
									initialCenter={{lat: location.latitude, lng: location.longitude}} 
									markers={[{name : 1, position : {lat: location.latitude, lng: location.longitude}}]}
									style={{height : 450, marginTop:30}}
									/>
								}
							</Col>
						</Row>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default ViewCustomer;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		height: 90%;
		.card-root {
			width: 80%;
			height: 100%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
`;
