import { useState, useEffect } from "react";
import styled from "styled-components";
import { Button, message } from "antd";

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import LocationActions from "../../redux/actions/locations"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";


const Locations = () => {
	const dispatch = useDispatch()
	const {error, locations, fetchingLocations, fetchLocationSuccessful,deletingLocation,deleteLocationSuccessful} = useSelector(state => state.locations)

	const [searchText, setSearchText] = useState("")
	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");

	useEffect(()=>{
		dispatch(LocationActions.stateReset({
			fetchingLocations : false,
			fetchLocationSuccessful : false,

			deletingLocation:false,
			deleteLocationSuccessful : false
		}))
	},[])

	useEffect(()=>{
		dispatch(LocationActions.clearError())
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(LocationActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(LocationActions.getLocationsRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingLocations && fetchLocationSuccessful){
			dispatch(LocationActions.stateReset({
				fetchLocationSuccessful : false
			}))
		}
	},[fetchLocationSuccessful])

	useEffect(()=> {
		if(!deletingLocation && deleteLocationSuccessful){
			dispatch(LocationActions.stateReset({
				deleteLocationSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Location Deleted successfully");
			dispatch(LocationActions.getLocationsRequest())
		}
	},[deleteLocationSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(LocationActions.getLocationsRequest(queryParams))
	},[searchText])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteLocation = () => {
		dispatch(LocationActions.deleteLocationRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};
	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};
	const columns = [
		{
			title: "Customer Name",
			key: "customerName",
			render: (text, record) => (
				<div>{text?.customerId?.firstName + " " +text?.customerId?.lastName }</div>
			),
		},
		{
			title: "Locatoin Name",
			dataIndex: "name",
			key: "name",
		},
		{
			title: "Building No.",
			dataIndex: "buildingNumber",
			key: "buildingNumber",
		},
		{
			title: "Zone No.",
			dataIndex: "zoneNumber",
			key: "zoneNumber",
		},
		{
			title: "Street No.",
			dataIndex: "streetNumber",
			key: "streetNumber",
		},
		{
			title: "History",
			key: "_id",
			render: (text, record) => (
				<Button className="history-btn" type="primary" onClick={()=>handleNavigation("/locations/history/"+text._id) }>
					History
				</Button>
			),
		},
		{
			title: "Action",
			key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/locations/edit/" + text._id)}
					onView={() => handleNavigation("/locations/view/" + text._id)}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];
	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"3"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Locations"
						button="Create"
						onClickButton={() => handleNavigation("/locations/create")}
					/>
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingLocations} data={locations} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingLocation}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteLocation}
						handleCancel={handleCancel}
						title="Delete Customer"
					>
						<p>Are you sure you want to delete customer ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Locations;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			background-color: #fff;
			padding: 20px;
		}
	}
	.history-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		key: "1",
		name: "John Brown",
		customerName: "Jim Green",
		history: "+91 987654321",
		locations: 23,
	},
	{
		key: "2",
		name: "Jim Green",
		customerName: "Joe Black",
		history: "+91 987654321",
		locations: 22,
	},
	{
		key: "3",
		name: "Joe Black",
		customerName: "Jim Green",
		history: "+91 987654321",
		locations: 24,
	},
	{
		key: "2",
		name: "Jim Green",
		customerName: "Joe Black",
		history: "+91 987654321",
		locations: 22,
	},
	{
		key: "3",
		name: "Joe Black",
		customerName: "Joe Black",
		history: "+91 987654321",
		locations: 24,
	},
];
