import {useEffect, useState} from "react"
import styled from "styled-components"
import {message,Row,Col,Button,Input, Typography} from "antd"

// import Wrapper from "./InputContainer.style";

const { Title } = Typography;

import TopHeader from "../../components/TopHeader"
import SideMenu from "../../components/SideMenu"
import ContainerHeader from "../../components/ContainerHeader"
import InputContainer from "../../components/InputContainer";
import MapComponent from "../../components/MapComponent"

import StaffActions from "../../redux/actions/staffs"
import { useDispatch, useSelector } from "react-redux";
import Themes from "../../themes/themes";
import { onNavigate } from "../../util/navigation";
import Router from "next/router";
// import Password from "antd/lib/input/Password"

const CreateWasteBin = () =>{
    
    const dispatch = useDispatch()
	const {error, creatingStaff, createStaffSuccessful} = useSelector(state => state.staffs)

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailId, setEmailId] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [password,setPassword] = useState("");
    const [buildingNumber, setBuildingNumber] = useState("");
	const [zoneNumber, setZoneNumber] = useState("");
	const [streetNumber, setStreetNumber] = useState("");

    useEffect(()=>{
        dispatch(StaffActions.stateReset({
			creatingStaff : false,
			createStaffSuccessful : false,
		}))
    },[])

    useEffect(()=>{
		dispatch(StaffActions.clearError())
    },[])

    useEffect(()=>{
		if(error)
			message.error(error)
    },[error])
    
    useEffect(()=>{
		if(!creatingStaff && createStaffSuccessful){
			dispatch(StaffActions.stateReset({
				createStaffSuccessful : false
			}))
			message.success("Staff created successfully");
			onNavigate(Router, "/staffs");
		}
	},[createStaffSuccessful])

    const handleClickCancel = () => {
		onNavigate(Router, "/staffs");
	};
	const handleClickCreate = () => {
		if(firstName === "" || lastName === "" ){
			return message.warn("Please fill all the fields")
		}
		dispatch(StaffActions.createStaffRequest({
			firstName,
            lastName,
            buildingNumber,
            zoneNumber,
            streetNumber,
			email : emailId,
            mobile : phoneNumber,
            password
		}))
	};

    return(
        <Wrapper>
            <TopHeader/>
            <SideMenu selectedKey={"4"}/>
                <div className="container">
                    <div className="card-root">
                        <ContainerHeader title="Create Staff"
                            onClickBackButton={() => Router.back()}
                        />
                        <div>
                            <Row gutter={24}>
                                <Col span={12}>
                                    <InputContainer
                                        title="First Name"
                                        value={firstName}
                                        onChange={(e) => setFirstName(e.target.value)}
                                    />
                                </Col>  
                                <Col span={12}>
                                    <InputContainer
                                        title="Last Name"
                                        value={lastName}
                                        onChange={(e) => setLastName(e.target.value)}
                                    />
                                </Col>    
                            </Row>    
                            <Row gutter={24}>
                                <Col span={12}>
                                    <InputContainer
                                        title="E mail"
                                        value={emailId}
                                        onChange={(e) => setEmailId(e.target.value)}
                                    />
                                </Col>  
                                <Col span={12}>
                                    <InputContainer
                                        title="Mobile Number"
                                        value={phoneNumber}
                                        onChange={(e) => setPhoneNumber(e.target.value)}
                                    />
                                </Col>    
                            </Row>   
                            <Col span={12}>
                                
			                        <Title level={5} className="title">
				                        Password
			                        </Title>
			                        <Input.Password
                                        className="inputBox1"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                            </Col>    
                            <Col span={24}>
							<Row>
								<InputContainer
									title="Bulding No."
									placeholder="Bulding No."
									value={buildingNumber}
									onChange={(e) => setBuildingNumber(e.target.value)}
								/>
								<Col>
								<Title level={5} className="title1">Zone Number</Title>
								<Input
									placeholder="Zone No"
									value={zoneNumber}
									onChange={(e) => setZoneNumber(e.target.value)}
									className="customInputBox"
								/>
								</Col>
								<Col>
								<Title level={5} className="title1">Street Number</Title>
								<Input
									placeholder="Street No"
									value={streetNumber}
									onChange={(e) => setStreetNumber(e.target.value)}
									className="customInputBox"
								/>
								</Col>
								</Row>
							
							</Col>
                            <div className="row">
                                <Button className="cancel-btn" onClick={handleClickCancel} >
                                    Cancel
                                </Button>
                                <Button loading={creatingStaff}  className="create-btn" onClick={handleClickCreate} >
                                    Create
                                </Button>
						    </div>
                        </div>
                    </div>
                </div>    
        </Wrapper>    
    );
};

export default CreateWasteBin

const Wrapper = styled.div`

    background-color:${Themes.backgroundMain};
    height:100vh;
    width:100vw;
    overflow:scroll;
    .container{
        margin-left:256px;
        padding: 20px;
        margin-top: 10vh;
        display:flex;
        justify-content:center;
        align-items:center;
        .card-root{
            width:80%;
            background-color: ${Themes.white};
            padding:40px;
        }
    }
    .create-btn{
        background-color:${Themes.primary};
        border:none;
        margin:20px;
        width:200px;
        height:40px;
        font-weight:bold;
        font-size:16px;
        border-radius:5px;
        color:${Themes.white}
    }
    .cancel-btn{
        background-color:${Themes.cancelButton};
        border:none;
        margin:20px;
        width:200px;
        height:40px;
        font-weight:bold;
        font-size:16px;
        border-radius:5px;
        color:${Themes.white};
    }
    .row{
        display:flex;
        flex-direction:row;
        justify-content:center;
        align-items:center;
    }
    .title {
		margin-top: 20px;
    }
    .customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;
    }
    .title1{
        margin-top:20px;
        margin-left:30px;
    }
	.inputBox1 {
        border-radius: 5px;
		background-color: ${Themes.inputBackground};
		border: none;
        height: 40px;
        width : 98%;
		input{
        
		background-color: ${Themes.inputBackground};
		

        }
	}

`;
