import { useEffect, useState } from "react";
import styled from "styled-components";
import { Button, message } from "antd";

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import StaffActions from "../../redux/actions/staffs"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Staffs = () => {

	const dispatch = useDispatch()
	const {error, staffs, fetchingStaffs, fetchStaffSuccessful,deletingStaff,deleteStaffSuccessful} = useSelector(state => state.staffs)

	const [staffList, setStaffs] = useState([])
	const [searchText, setSearchText] = useState("")
	const [date, setDate] = useState()
	const [selectedValue, setSelectedValue] = useState()
	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	
	const options = [{name : "name 1", value: "value 1"}, {name : "name 1", value: "value 1"}]
	useEffect(()=>{
		dispatch(StaffActions.stateReset({
			fetchingStaffs : false,
			fetchStaffSuccessful : false,

			deletingStaff:false,
			deleteStaffSuccessful : false
		}))
	},[])

	useEffect(()=>{
		dispatch(StaffActions.clearError())
	},[])

	useEffect(()=>{
		if(error)
			message.error(error)
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(StaffActions.getStaffRequest(queryParams))
	},[])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(StaffActions.getStaffRequest(queryParams))
	},[searchText])

	useEffect(()=> {
		if(!fetchingStaffs && fetchStaffSuccessful){
	
			dispatch(StaffActions.stateReset({
				fetchStaffSuccessful : false
			}))
		}
	},[fetchStaffSuccessful])

	useEffect(()=> {
		if(!deletingStaff && deleteStaffSuccessful){
			dispatch(StaffActions.stateReset({
				deleteStaffSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Staff Deleted successfully");
			dispatch(StaffActions.getStaffRequest())
		}
	},[deleteStaffSuccessful])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteStaff = () => {
		dispatch(StaffActions.deleteStaffRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const columns = [
		{title: "Staffs Name",key: "firstName", 
			render: (text, record) => <div>{text.firstName + " " + text.lastName}</div>},
		{title: "Email",dataIndex: "email",key: "email"},
		{title: "Mobile",dataIndex: "mobile",key: "mobile"},
		{
			title: "History",
			key: "_id",
			render: (text, record) => (
				<Button className="history-btn" type="primary" onClick={()=>handleNavigation("/staffs/history/"+text._id)}>
					History
				</Button>
			),
		},
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/staffs/edit/"+text._id)}
					onView={() => handleNavigation("/staffs/view/"+text._id)}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];



	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"4"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title="Staffs" button="Create" onClickButton={()=>handleNavigation("/staffs/create")} />
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent  loading={fetchingStaffs} data={staffs} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingStaff}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteStaff}
						handleCancel={handleCancel}
						title="Delete Customer"
					>
						<p>Are you sure you want to delete staff ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Staffs;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			background-color: #fff;
			padding: 20px;
		}
	}
	.history-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

// const columns = [
// 	{
// 		title: "Name",
// 		dataIndex: "name",
// 		key: "name",
// 	},
// 	{
// 		title: "Email",
// 		dataIndex: "email",
// 		key: "email",
// 	},
// 	{
// 		title: "Mobile",
// 		dataIndex: "mobile",
// 		key: "email",
// 	},
// 	{
// 		title: "History",
// 		key: "history",
// 		dataIndex: "history",
// 		render: (text, record) => (
// 			<Button className="history-btn" type="primary">
// 				History
// 			</Button>
// 		),
// 	},
// 	{
// 		title: "Action",
// 		key: "action",
// 		render: (text, record) => (
// 			<UsualActions
// 				onEdit={() => message.success("Edit is clicked")}
// 				onDelete={() => message.success("Delete is clicked")}
// 				onView={() => message.success("View is clicked")}
// 			/>
// 		),
// 	},
// ];

// const data = [
// 	{
// 		key: "1",
// 		name: "John Brown",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "2",
// 		name: "Jim Green",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "3",
// 		name: "Joe Black",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "2",
// 		name: "Jim Green",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "3",
// 		name: "Joe Black",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "1",
// 		name: "John Brown",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "2",
// 		name: "Jim Green",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "3",
// 		name: "Joe Black",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "2",
// 		name: "Jim Green",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// 	{
// 		key: "3",
// 		name: "Joe Black",
// 		email: "johnbrown@gmail.com",
// 		mobile: "+91 987654321",
// 	},
// ];
