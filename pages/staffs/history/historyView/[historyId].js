import { useEffect, useState } from "react";
import styled from "styled-components";
import { message,Table,Space,Button,Row,Col} from "antd";



import ViewLocationMap from "../../../../components/MapComponent";
import StaffActions from "../../../../redux/actions/staffs"
import TopHeader from "../../../../components/TopHeader/TopHeader";
import SideMenu from "../../../../components/SideMenu/SideMenu";
import ContainerHeader from "../../../../components/ContainerHeader/ContainerHeader";
import TableComponent, { ViewAction } from "../../../../components/Table/Table";
import Themes from "../../../../themes/themes"
import {onNavigate} from "../../../../util/navigation"
import { useDispatch, useSelector } from "react-redux";
import Router,{ useRouter } from "next/router";



const HistoryView = () =>{

const dispatch = useDispatch()
const router = useRouter();

const {error,staffHistoryDetail,fetchingStaffHistoryDetail,fetchStaffHistoryDetailSuccessfulll} = useSelector(state =>state.staffs)

const [taskId,setTaskId] = useState("");
const [mapView, setMapLoaded] = useState(false);
const [items,setItems] = useState([]);
const [markers,setMarkers] = useState([]);

useEffect(()=>{
	dispatch(StaffActions.stateReset({
		fetchingStaffHistoryDetail:false,
		fetchStaffHistoryDetailSuccessfulll:false
	}))
},[])

useEffect(()=>{
	dispatch(StaffActions.clearError())
},[])

useEffect(()=>{
	if(error){
		message.error(error)
		dispatch(StaffActions.clearError())
	}    
},[error])

useEffect(()=>{
	
	let {historyId} = router.query
	
	if(historyId){
		setTaskId(historyId)
	}
	else{
		onNavigate(Router,"staffs")
	}
	
},[])


useEffect(()=>{
	if(taskId)		
		dispatch(StaffActions.getStaffHistoryDetailRequest(taskId))
},[taskId])


useEffect(()=> {
	if(!fetchingStaffHistoryDetail && fetchStaffHistoryDetailSuccessfulll){
		
		// setHistoryDate(staffHistory.date);
		// setHistoryStartTime(staffHistory.startTime);
		// setHistoryEndTime(staffHistory.endTime);
		// setStatus(staffHistory.status);
		setMapLoaded(true)	
		staffHistoryDetail.forEach((item,index)=>{
			setItems([...items,{
				"lat":item.locationId.latitude,
				"lng":item.locationId.longitude,
			}])
			setMarkers([...markers,{
				"position":{
					"lat":item.locationId.latitude,
					"lng":item.locationId.longitude,
					"locationStatus": item.locationId.status
				}
			}])
			console.log(item.locationId.latitude)
		});

		

		dispatch(StaffActions.stateReset({
			fetchStaffHistoryDetailSuccessfulll : false
		}))
	}
},[fetchStaffHistoryDetailSuccessfulll])



	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"4"} />
			<div className="container">
				<div className="card-root">
					<Row gutter={24}>
						<Col span={12}>
							<ContainerHeader title="November 10" 
								onClickBackButton={() => Router.back()}
							/>
						<Table pagination={false} loading={false} columns={columns} dataSource={staffHistoryDetail.length != 0 ? staffHistoryDetail : []} />
						</Col>
						<Col span={12}>
							{mapView &&
									<ViewLocationMap  
										onClickMap={async () =>{}}
										initialCenter={{lat: staffHistoryDetail[0].locationId.latitude, lng: staffHistoryDetail[0].locationId.longitude}} 
										markers={markers}
										polylines={items}
										style={{height : 450, marginTop:30, marginRight : 30}}
									/>
							}
						</Col>	
					</Row>

				</div>
			</div>
		</Wrapper>
	);

}
export default HistoryView;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		width: 100vw;
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
		height: 80vh;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
    },
    .staff-history-headers{
        display: flex;
        flex-direction: row;
        h3{
            margin-left:15px
        }
    }
`;

const columns = [

	{
		title: "Locatoin Name",
		key: "locationId",
		dataIndex: "locationId",
		render: (text,record) => (
			<div>{text.name}</div>
		)
    },
	{
		title: "Assigned Time",
		dataIndex: "scheduledAt",
		key: "scheduledAt",
		render: (text, record) => {
			var res = text.slice(11,16)
			return(res)
		}
	},
	{
		title: "Collected Time",
		dataIndex: "time",
		key: "time",
	},
	// {
	// 	title: "Customer Name",
	// 	dataIndex: "staffName",
	// 	key: "staffName",
	// },
	// {
	// 	title: "Bin Type",
	// 	dataIndex: "staffName",
	// 	key: "staffName",
	// },
	// {
	// 	title: "Status",
	// 	dataIndex: "status",
	// 	key: "status",
	// 	render: (text, record) => {
	// 		if (text == "completed") {
	// 			return text
    //         } 
    //         else {
    //             return (<h5>Good</h5>)
    //         }
	// 	},
	// },
	// {
	// 	title: "View",
	// 	key: "action",
	// 	render: (text, record) => (
	// 		<ViewAction	
	// 			onView={() => message.success("copy is clicked")}
	// 		/>
	// 	),
	// },
];

const data = [
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "completed",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "inprogress",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "completed",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "inprogress",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "cancelled",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "complsvdseted",
	},
	{
		key: "1",
		date: "10-12-2020",
		staffName: "John Brown",
		status: "cancelled",
	},
];
