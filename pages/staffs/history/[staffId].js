import styled from "styled-components";
const { RangePicker } = DatePicker;
import { Button, message, DatePicker, Space , Select, Row } from "antd";


import {Images}  from '../../../components/StaffStatus/assets';


import TopHeader from "../../../components/TopHeader/TopHeader";
import SideMenu from "../../../components/SideMenu/SideMenu";
import ContainerHeader,{FilterOptions} from "../../../components/ContainerHeader/ContainerHeader";
import TableComponent, { ViewAction } from "../../../components/Table/Table";
import Themes from "../../../themes/themes"

import CenteredModal from "../../../components/CenteredModal";
import StaffActions from "../../../redux/actions/staffs"
import { useDispatch,useSelector } from "react-redux";
import Router,{ useRouter } from "next/router";
import {onNavigate} from "../../../util/navigation"
import { useEffect, useState } from "react";
import summaryGenerator from "../../../services/summaryGenerator";
import moment from 'moment';

const StaffHistory = () => {

	const dispatch = useDispatch();
	const router = useRouter();

	const {error,staffHistory,fetchingStaffHistory,fetchStaffHistorySuccessfulll} = useSelector(state =>state.staffs)

	const [staffId,setStaffId] = useState("");
	const [isDownloadModalVisible, setIsDownloadModalVisible] = useState(false);
	const [date, setDate] = useState()
	const [selectedValue, setSelectedValue] = useState();
	const options = [
		{name : "All", value: ""}, 
		{name : "Scheduled", value: "scheduled"}, 
		{name : "In Progress", value: "in_progress"},
		{name : "Completed", value: "completed"},
		{name : "Cancelled", value: "cancelled"},
	]
	// const [historyDate,setHistoryDate] = useState("");
	// const [historyStartTime,setHistoryStartTime] = useState("");
	// const [historyEndTime,setHistoryEndTime] = useState("");
	// const [status,setStatus] = useState("");

	useEffect(()=>{
		dispatch(StaffActions.stateReset({

			fetchingStaffHistory:false,
			fetchStaffHistorySuccessfulll:false

		}))
	},[])

	useEffect(()=>{
		dispatch(StaffActions.clearError())
	},[])

	useEffect(()=>{
		if(error){
            message.error(error)
            dispatch(StaffActions.clearError())
        }    
	},[error])

	useEffect(()=>{
        let {staffId} = router.query
        
        if(staffId){
            setStaffId(staffId)
        }
        else{
            onNavigate(Router,"staffs")
        }
		
    },[])


	useEffect(()=>{
		var queryParams = "";
        if(staffId)		
            dispatch(StaffActions.getStaffHistoryRequest(staffId, queryParams))
	},[staffId])

	useEffect(()=>{
		var queryParams = "";

		if (date) {
			var startDate = moment(date[0]).format("YYYY-MM-DD");
			var endDate = moment(date[1]).format("YYYY-MM-DD");
			queryParams += `&startDate=${startDate}&endDate=${endDate}`;
		}

		if (selectedValue) {
			queryParams += `&status=${selectedValue}`;
		}

		if (queryParams) {
			queryParams = queryParams.substring(1);	
		}

		if(staffId){
			dispatch(StaffActions.getStaffHistoryRequest(staffId, queryParams))
		}
	},[date, selectedValue])

	useEffect(()=> {
        
		if(!fetchingStaffHistory && fetchStaffHistorySuccessfulll){
            
            // setHistoryDate(staffHistory.date);
            // setHistoryStartTime(staffHistory.startTime);
            // setHistoryEndTime(staffHistory.endTime);
            // setStatus(staffHistory.status);

            dispatch(StaffActions.stateReset({
				fetchStaffHistorySuccessfulll : false
			}))
		}
	},[fetchStaffHistorySuccessfulll])
	
	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};	

	const columns = [
		{
			title: "Date",
			dataIndex: "scheduledAt",
			key: "scheduledAt",
			render: (text, record) => {
				var res = text.slice(0,10)
				return(res)
			}
		},
		{
			title: "Start Time",
			dataIndex: "startTime",
			key: "startTime",
		},
		{
			title: "End Time",
			dataIndex: "endTime",
			key: "endTime",
		},
		{
			title: "Status",
			dataIndex: "status",
			key: "status",
			render: (text, record) => {
				if (text == "completed") {
					return (<img src={Images.completed}/>)
				} 
				else if(text=="inprogress") {
					return (<img src={Images.inprogress}/>)
				}
				else if(text=="cancelled"){
					return (<img src={Images.cancelled}/>)     
				}
				else{
					return (<img src={Images.scheduled}/>)
				}
			},
		},
		{
			title: "View",
			key: "_id",
			render: (text, record) => (
				<ViewAction	
					onView={() => handleNavigation("/staffs/history/historyView/"+text._id)}
				/>
			),
		},
	];


	const onDownloadClick = async () => {
		const histories  = staffHistory
		const rows = [
			["Date", "Start Time", "End Time", "Status"]
		]
		await Promise.all(histories?.map(history =>{
			rows.push([moment(history.scheduledAt).format("YYYY-MM-DD"), history.startTime,history.endTime,history.status])
		}))

		let csvContent = "data:text/csv;charset=utf-8,"
			+ rows.map(e => e.join(",")).join("\n");
		var encodedUri = encodeURI(csvContent);
		var link = document.createElement("a");
		link.setAttribute("href", encodedUri);
		link.setAttribute("download", "staff_summary_"+staffId+".csv");
		document.body.appendChild(link); // Required for FF

		link.click(); // This will download the data file named "my_data.csv".
		setIsDownloadModalVisible(false)
	}

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"4"} />
			<div className="container">
				<div className="card-root">
				<CenteredModal 
						loading={false}
						visible={isDownloadModalVisible}
						handleSubmit={onDownloadClick}
						handleCancel={() =>setIsDownloadModalVisible(false)}
						cancel={"Cancel"}
						submit={"Download"}
						title={"Download confirmation"} >
							Are you want to download the staff summary?
						</CenteredModal>
				<ContainerHeader
						title="Staff History"
						onClickBackButton={() => Router.back()}
						// button="Create"
						// onClickButton={() => handleNavigation("/locations/create")}
					/>
						<FilterOptions 
							datePicker
							select
							options={options}
							onChangeDate={(value) => setDate(value)}
							date={date}
							selectedValue={selectedValue}
							selectkey={"name"}
							selectvalue={"value"}
							onChangeSelect={(value) => setSelectedValue(value)}
							downloadButton={"Download"}
							onClickDownloadButton={() => setIsDownloadModalVisible(true)}
							/>
					<TableComponent loading={false} data={staffHistory} columns={columns} />
				</div>
			</div>
		</Wrapper>
	);
};

export default StaffHistory;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;


// const data = [
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "completed",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "inprogress",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "completed",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "inprogress",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "cancelled",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "complsvdseted",
// 	},
// 	{
// 		key: "1",
// 		date: "10-12-2020",
// 		staffName: "John Brown",
// 		status: "cancelled",
// 	},
// ];