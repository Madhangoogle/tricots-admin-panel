import { useEffect, useState } from "react";
import styled from "styled-components"
import {message,Row,Col,Button,Typography,Input} from "antd"
const { Title } = Typography;

import TopHeader from "../../../components/TopHeader/TopHeader"
import SideMenu from "../../../components/SideMenu/SideMenu"
import ContainerHeader from "../../../components/ContainerHeader/ContainerHeader"
import MapComponent from "../../../components/MapComponent/MapComponent"
import InputContainer from "../../../components/InputContainer/InputContainer"

import StaffActions from "../../../redux/actions/staffs"
import Themes from "../../../themes/themes"
import {onNavigate} from "../../../util/navigation"
import Router,{useRouter} from "next/router"
import { useDispatch, useSelector } from "react-redux"
import StaffsActions from "../../../redux/actions/staffs";
// import Wrapper from "../../components/TopHeader/TopHeader.style"


const EditStaff = () =>{


    const dispatch = useDispatch()
    const router = useRouter()
    const {error,staffDetail,updatingStaff,updateStaffSuccessful,fetchingStaffDetail,fetchStaffDetailSuccessfull} = useSelector(state =>state.staffs)

    const [staffId,setStaffId] = useState("");
    const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [emailId, setEmailId] = useState("");
	const [phoneNumber, setPhoneNumber] = useState("");
    const [password,setPassword] = useState("");
    const [buildingNumber, setBuildingNumber] = useState("");
	const [zoneNumber, setZoneNumber] = useState("");
	const [streetNumber, setStreetNumber] = useState("");
    
    useEffect(()=>{
		dispatch(StaffActions.stateReset({
            
            fetchingStaffDetail : false,
            fetchStaffDetailSuccessfull : false,
            
            updatingStaff:false,
            updateStaffSuccessful:false

		}))
	},[])

    useEffect(()=>{
		dispatch(StaffActions.clearError())
	},[])

    useEffect(()=>{
		if(error){
            message.error(error)
            dispatch(StaffsActions.clearError())
        }    
	},[error])

    useEffect(()=>{
        let {staffId} = router.query
        
        if(staffId){
            setStaffId(staffId)
        }
        else{
            onNavigate(Router,"staffs")
        }
		
    },[])

    useEffect(()=>{
        if(staffId)		
            dispatch(StaffActions.getStaffDetailsRequest(staffId))
	},[staffId])
    
    useEffect(()=> {
        
            

		if(!fetchingStaffDetail && fetchStaffDetailSuccessfull){
            
            setFirstName(staffDetail.firstName);
            setLastName(staffDetail.lastName);
            setEmailId(staffDetail.email);
            setPhoneNumber(staffDetail.mobile);
            setBuildingNumber(staffDetail.buildingNumber);
            setZoneNumber(staffDetail.zoneNumber);
            setStreetNumber(staffDetail.streetNumber);
            setPassword(staffDetail.password)

            dispatch(StaffActions.stateReset({
				fetchStaffDetailSuccessfull : false
			}))
		}
    },[fetchStaffDetailSuccessfull])


    useEffect(()=>{
		if(!updatingStaff && updateStaffSuccessful){
			dispatch(StaffsActions.stateReset({
				updateStaffSuccessful : false
			}))
			message.success("Staff updated successfully");
		}
	},[updateStaffSuccessful])
    
    // useEffect(()=>{
    //     setFirstName(staffDetail.firstName);
    // },[staffDetail])


    const handleClickCancel = () => {
		onNavigate(Router, "/staffs");
	};
    const handleClickSave = () =>{
        if(firstName === "" || lastName === "" ){
			return message.warn("Please fill all the fields")
		}
        dispatch(StaffActions.updateStaffRequest(staffId,{
			firstName,
            lastName,
            buildingNumber,
            zoneNumber,
            streetNumber,
			email : emailId,
            mobile : phoneNumber,
            password
		}))
    }
    return(
        <Wrapper>
                <TopHeader/>
            <SideMenu selectedKey={"4"}/>
                <div className="container">
                    <div className="card-root">
                        <ContainerHeader title="Edit Staff"
                            onClickBackButton={() => Router.back()}
                        />
                        <div>
                            <Row gutter={24}>
                                <Col span={12}>
                                    <InputContainer
                                        title="First Name"
                                        value={firstName}
                                        onChange={(e) => setFirstName(e.target.value)}
                                    />
                                </Col>  
                                <Col span={12}>
                                    <InputContainer
                                        title="Last Name"
                                        value={lastName}
                                        onChange={(e) => setLastName(e.target.value)}
                                    />
                                </Col>    
                            </Row>
                            <Row gutter={24}>
                                <Col span={12}>
                                    <InputContainer
                                        title="E mail"
                                        value={emailId}
                                        onChange={(e) => setEmailId(e.target.value)}
                                    />
                                </Col>  
                                <Col span={12}>
                                    <InputContainer
                                        title="Mobile Number"
                                        value={phoneNumber}
                                        onChange={(e) => setPhoneNumber(e.target.value)}
                                    />
                                </Col>    
                            </Row>   
                            {/* <Col span={12}>
                                
			                        <Title level={5} className="title">
				                        Password
			                        </Title>
			                        <Input.Password
                                        className="inputBox"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                            </Col>       */}
                            <Col span={24}>
							
                            <Row>
								<InputContainer
									title="Bulding No."
									placeholder="Bulding No."
									value={buildingNumber}
									onChange={(e) => setBuildingNumber(e.target.value)}
								/>
								<Col>
								<Title level={5} className="title1">Zone Number</Title>
								<Input
									placeholder="Zone No"
									value={zoneNumber}
									onChange={(e) => setZoneNumber(e.target.value)}
									className="customInputBox"
								/>
								</Col>
								<Col>
								<Title level={5} className="title1">Street Number</Title>
								<Input
									placeholder="Street No"
									value={streetNumber}
									onChange={(e) => setStreetNumber(e.target.value)}
									className="customInputBox"
								/>
								</Col>
								</Row>
                        
                            </Col>
                            <div className="row">
                                <Button  className="create-btn" onClick={handleClickSave} >
                                    Save
                                </Button>
                                <Button className="cancel-btn" onClick={handleClickCancel} >
                                    Cancel
                                </Button>
						    </div>
                        </div>
                    </div>
                </div>    
        </Wrapper>    
    );
};

export default EditStaff

const Wrapper = styled.div`

    background-color:${Themes.backgroundMain};
    height:100vh;
    width:100vw;
    overflow:scroll;
    .container{
        margin-left:256px;
        padding: 20px;
        margin-top: 10vh;
        display:flex;
        justify-content:center;
        align-items:center;
        .card-root{
            width:80%;
            background-color: ${Themes.white};
            padding:40px;
        }
    }
    .create-btn{
        background-color:${Themes.primary};
        border:none;
        margin:20px;
        width:200px;
        height:40px;
        font-weight:bold;
        font-size:16px;
        border-radius:5px;
        color:${Themes.white}
    }
    .cancel-btn{
        background-color:${Themes.cancelButton};
        border:none;
        margin:20px;
        width:200px;
        height:40px;
        font-weight:bold;
        font-size:16px;
        border-radius:5px;
        color:${Themes.white};
    }
    .row{
        display:flex;
        flex-direction:row;
        justify-content:center;
        align-items:center;
    }
    .customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;
	}
    .title {
		margin-top: 20px;
    }
    .title1{
        margin-top:20px;
        margin-left:30px;
    }
	.inputBox {
        border-radius: 5px;
		background-color: ${Themes.inputBackground};
		border: none;
        height: 40px;

		input{
        
		background-color: ${Themes.inputBackground};
		

        }
	}

`;