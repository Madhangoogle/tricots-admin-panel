import Head from 'next/head'
import 'antd/dist/antd.css'
import { Input, Button,  Col, message } from 'antd'
import { UserOutlined, SecurityScanOutlined } from '@ant-design/icons'
import Themes from "../themes/themes";

import styled from 'styled-components'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AuthActions from '../redux/actions/auth'
import { onNavigate } from '../util/navigation'
import Router from 'next/router'


const Login = (props) => {
	const dispatch = useDispatch()
	const {error, loggingIn, logInSuccessful} = useSelector(state => state.auth)
	const [username, setUsername] = useState("")
	const [password, setPassword] = useState("")

	const onClickSubmit = () => {
		dispatch(AuthActions.loginRequest(username, password))
	}

	useEffect(()=>{
		dispatch(AuthActions.clearError())
	},[])

	useEffect(()=>{
		dispatch(AuthActions.stateReset({
			logInSuccessful : false,
			logInSuccessful : false
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
		}
	},[error])

	useEffect(()=>{
		if(!loggingIn && logInSuccessful) {
			dispatch(AuthActions.stateReset({
				logInSuccessful : false
			}))
			onNavigate(Router, "/dashboard")
			message.success("Logged in Successfully")
		}
	},[logInSuccessful])


	return (
		<LoginWrapper>
			<Head>
				<title>Tricots</title>
				<link rel="icon" className="logoImage" href={require('../Images/logo.png')} />
			</Head>
			<img
				className="logo-image"
				src={
					'https://img.freepik.com/free-photo/top-view-color-t-shirt-grey-wood-plank-background_30478-1351.jpg?size=626&ext=jpg'
				}
			/>
			<div className="overlay" />
			<Col
				className="login-container"
				xs={{ span: 16, offset: 0 }}
				lg={{ span: 8, offset: 0 }}
			>
				<img src={require('../Images/logo.png')} className="logoImage" />
				<Input
					prefix={<UserOutlined />}
					placeholder="Username"
					className="inputBox"
					size={'large'}
					value={username}
					onChange={(e)=> setUsername(e.target.value)}
				/>
				<Input
					prefix={<SecurityScanOutlined />}
					placeholder="Password"
					className="inputBox"
					type="password"
					size={'large'}
					value={password}
					onChange={(e)=> setPassword(e.target.value)}
				/>
				<Button
					loading={loggingIn}
					onClick={onClickSubmit}
					className="button"
					type={'primary'}
					size={'medium'}
				>
					Login
				</Button>
			</Col>
		</LoginWrapper>
	)
}

export default Login

const LoginWrapper = styled.div`
	.logo-image {
		position: absolute;
		height: 100vh;
		width: 100vw;
	}
	.overlay {
		position: absolute;
		background-color: rgba(0, 0, 0, 0.3);
		height: 100vh;
		width: 100vw;
		z-index: 2;
	}
	.login-container {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 50%;
		transform: translate(-50%, -50%);
		z-index: 3;
		padding: 20px;
		border-radius: 20px;
		background-color: #fff;
		flex-direction: column;
		align-items: center;
		justify-content: center;
		display: flex;
	}
	.inputBox {
		width: 70%;
		margin-top: 30px;
	}
	.button {
		width: 30%;
		background-color: ${Themes.primary};
		border: one;
		margin-top: 30px;
	}
	.logoImage {
		width: 140px;
		height: 70px;
	}
`
