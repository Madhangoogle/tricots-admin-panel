import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch, Row, Col  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import RatingActions from "../../redux/actions/ratings"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Ratings = () => {
	const dispatch = useDispatch()
	const {error, rating, ratings, fetchingRatings, fetchRatingSuccessful,deletingRating,deleteRatingSuccessful,  updatingRating, updateRatingSuccessful,} = useSelector(state => state.ratings)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(RatingActions.stateReset({
			fetchingRatings : false,
			fetchRatingSuccessful : false,

			deletingRating:false,
			deleteRatingSuccessful : false,

			updatingRating: false, 
			updateRatingSuccessful: false,
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(RatingActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(RatingActions.getRatingRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingRatings && fetchRatingSuccessful){
			dispatch(RatingActions.stateReset({
				fetchRatingSuccessful : false
			}))
		}
	},[fetchRatingSuccessful])

	useEffect(()=> {
		if(!deletingRating && deleteRatingSuccessful){
			dispatch(RatingActions.stateReset({
				deleteRatingSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Rating Deleted successfully");
			dispatch(RatingActions.getRatingRequest())
		}
	},[deleteRatingSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(RatingActions.getRatingRequest(queryParams))
	},[searchText])

	useEffect(()=>{
		if(!updatingRating && updateRatingSuccessful){
			dispatch(RatingActions.stateReset({
				updateRatingSuccessful : false
			}))
			message.success("Rating updated successfully");
			dispatch(RatingActions.getRatingRequest(""))
		}
	},[updateRatingSuccessful])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteRating = () => {
		dispatch(RatingActions.deleteRatingRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const columns = [
		{title: "Customer",key: "customerId",dataIndex: "customerId",  render: customerId => {
            return (
				<div>
					<Row gutter={24}>
						<Col span={4}>
							Name:
					</Col>
						<Col span={20}>
							<h4>{customerId?.firstName + " " + customerId?.lastName}</h4>
						</Col>
					</Row>
					<Row gutter={24}>
						<Col span={4}>
							Email:
					</Col >
						<Col span={20}>
							<h4>{customerId?.emailId}</h4>
						</Col>
					</Row>
				</div>
            )
        }},
		{title: "Product",key: "productId",dataIndex: "productId",  render: productId => {
            return (
                <div>
                     <img style={{width: 70, height: 70, borderRadius: 10}} src={productId?.imageId?.imageUrl}/>
                     <div style={{display: "inline", marginLeft: "2%"}}>
                        {productId?.name}
                    </div>
                </div>
            )
        }},
		{title: "Rating",key: "rating",dataIndex: "rating", width: '5%'},
        {title: "Review",key: "review",dataIndex: "review",  width: '30%',},
		// {title: "Action",key: "_id",
		// 	render: (text, record) => (
		// 		<UsualActions 
		// 			onEdit={() => handleNavigation("/ratings/edit/" + text._id)}
		// 			onView={() => handleNavigation("/ratings/view/" + text._id)}
		// 			onDelete={() => handleOpenDeleteModal(text._id)}
		// 			onView={false}
		// 			onEdit={false}
		// 			onDelete={false}
		// 		/>
		// 	)
		// },
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"13"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Ratings"
					/>
					{/* <FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/> */}
					<TableComponent loading={fetchingRatings} data={ratings} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingRating}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteRating}
						handleCancel={handleCancel}
						title="Delete Rating"
					>
						<p>Are you sure you want to delete rating ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Ratings;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
