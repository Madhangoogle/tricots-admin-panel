const withImages = require("next-images");
const withSass = require("@zeit/next-sass");
const withLess = require("@zeit/next-less");
const withCSS = require("@zeit/next-css");
const compose = require("next-compose");
const imagesConfig = {};
const cssConfig = {};

module.exports = compose([
	{ distDir: "build" },
	withCSS({
		cssModules: true,
		cssLoaderOptions: {
			importLoaders: 1,
			localIdentName: "[local]___[hash:base64:5]",
		},
		...withLess(
			withSass({
				lessLoaderOptions: {
					javascriptEnabled: true,
				},
			})
		),
	}),
	[withImages, imagesConfig],
	{
		webpack: (config) => {
			return config;
		},
	},
]);
