const Images = {
	ayonLogo: require('./images/ayon.png'),
	backButton : require('./images/back-button.png')
}
export { Images }

const ComponentAssets = {
	Images
}
export default ComponentAssets
