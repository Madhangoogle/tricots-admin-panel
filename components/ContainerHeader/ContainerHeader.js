import React from "react";
import Wrapper, { FilterWrapper } from "./ContainerHeader.style";
import { Input, Button, Typography } from "antd";
import { PlusCircleOutlined, SearchOutlined, DownloadOutlined } from "@ant-design/icons";
import { Select } from "antd";
import { Images } from './assets/index'
import { DatePicker, Space } from "antd";

const { RangePicker } = DatePicker;

const { Title } = Typography;

const { } = Input;

const { Option } = Select;

function onChange(date, dateString) {
	console.log(date, dateString);
}


const TopHeader = (props) => {
	const {
		title,
		options,
		date,
		search,
		button,
		downloadButton,
		onClickButton,
		onClickBackButton,
		onClickDownloadButton,
		onSearch,
		input,
		defaultSelected,
		inputValue,
		selectkey,
		selectvalue,
		optionsArrayOfObjects,
		onChangeSelect,
		onChangeInput,
	} = props;
	return (
		<div>
			<div>{onClickBackButton && (<img src={Images.backButton} style={{width:"20px",height:"20px",marginBottom:"10px",cursor:"pointer"}} onClick={onClickBackButton}/>)}</div>	
		<Wrapper>
			
			<div className="card-header-actions">

				
				<Title level={3}>{title}</Title>
				{input && (
					<Input
						placeholder={input === "Location 1" ? "" : input}
						value={inputValue}
						onChange={(e) => onChangeInput(e.target.value)}
						className="input-box"
					/>
				)}
				{date && <RangePicker onChange={onChange} />}
				{options && (
					<Select
						placeholder="Select customer"
						className="select"
						defaultValue={defaultSelected}
						value={defaultSelected}
						onChange={(value) => onChangeSelect(value)}
					>
						{options.map((option) => {
							return <Option value={optionsArrayOfObjects ? option[selectvalue] : option}>{optionsArrayOfObjects ? option[selectkey] : option}</Option>;
						})}
					</Select>
				)}
				{search && (
					<Input
						placeholder={search}
						onChange={onSearch}
						className="search-box"
					/>
				)}
				{button && (
					<Button
						type="primary"
						icon={<PlusCircleOutlined />}
						loading={false}
						className="create-btn"
						onClick={onClickButton}
					>
						{button}
					</Button>
				)}
				{downloadButton && (
					<Button
						type="primary"
						loading={false}
						className="create-btn"
						onClick={onClickDownloadButton}
					>
						{downloadButton}
					</Button>
				)}
			</div>
		</Wrapper>
		</div>
	);
};


export const FilterOptions = (props) => {
	const {
		datePicker,
		select,
		search,
		options,
		onChangeDate,
		date,
		selectedValue,
		inputValue,
		selectkey,
		selectvalue,
		placeholder,
		onChangeSelect,
		onChangeInput,
		downloadButton,
		onClickDownloadButton
	} = props;
	return <FilterWrapper>
		<div>
			{datePicker && <RangePicker className="time-picker" onChange={onChangeDate} />}
			{select && (
				<Select
					placeholder="Status"
					className="select"
					style={{ width: 250 }}
					value={selectedValue}
					onChange={onChangeSelect}
				>
					{options.map((option) => {
						return <Option value={selectvalue ? option[selectvalue] : option}>{selectkey ? option[selectkey] : option}</Option>;
					})}
				</Select>
			)}

			{search && (
				<Input
					placeholder={placeholder ? placeholder : "Enter the " + inputValue}
					onChange={onChangeInput}
					className="search-box"
					suffix={<SearchOutlined />}
				/>
			)}
		</div>

		{downloadButton && (
			<Button
				icon={<DownloadOutlined />}
				type="primary"
				loading={false}
				className="create-btn"
				onClick={onClickDownloadButton}
			>
				{downloadButton}
			</Button>
		)}
	</FilterWrapper>
}

export default TopHeader;
