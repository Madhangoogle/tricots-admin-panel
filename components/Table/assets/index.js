const Images = {
	ayonLogo: require("./images/ayon.png"),
	edit: require("./images/edit.png"),
	view: require("./images/view.png"),
	delete: require("./images/delete.png"),
	copy: require("./images/copy.png"),
};
export { Images };

const ComponentAssets = {
	Images,
};
export default ComponentAssets;
