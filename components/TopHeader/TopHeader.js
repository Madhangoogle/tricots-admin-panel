import React, { useState } from 'react'
import Wrapper from './TopHeader.style'
import { Images } from './assets/index'
import { UserOutlined } from '@ant-design/icons'

import CenteredModal from "../CenteredModal"
import { onNavigate } from '../../util/navigation'
import Router from "next/router";

const TopHeader = (props) => {
	const {} = props

	const [logoutConfirmationModal,setLogoutModalOpen]=useState(false);

	const handleOpenLogOutModal = ()=>{
		setLogoutModalOpen(true)
	}
	
	const handleLogOut = ()=>{
		localStorage.removeItem('accessToken');
		onNavigate(Router, '/login')
		setLogoutModalOpen(false)
	}

	const handleCancel = ()=>{
		setLogoutModalOpen(false)
	}

	return (
		<Wrapper>
			<div style={{display: "inline-block"}}>
				<img src={require("../../Images/logo.png")}  className="logoImage" />
				<div className="brand">TRICOTS</div>
			</div>
			<div className="rightContainer">
				<div className="username">Admin</div>
				<div className="userImageContainer" onClick={()=>handleOpenLogOutModal()} >
					<UserOutlined className="userLogo"/>
				</div>
				<CenteredModal
					visible={logoutConfirmationModal}
					loading={false}
					submit={"Yes"}
					cancel="No"
					handleSubmit={handleLogOut}
					handleCancel={handleCancel}
					title="Log Out"

				>
					Do you want to Logout?
				</CenteredModal>	
			</div>
		</Wrapper>
	)
}

export default TopHeader
