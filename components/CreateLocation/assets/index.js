const Images = {
	marker: require("./images/marker.png"),
};
export { Images };

const ComponentAssets = {
	Images,
};
export default ComponentAssets;
