const Images = {
	sampleProfile: require('./images/sampleProfile.png')
}
export { Images }

const ComponentAssets = {
	Images
}
export default ComponentAssets
