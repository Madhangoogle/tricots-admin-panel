import styled from 'styled-components'
import UGTheme from '../../themes/UGTheme'

const Wrapper = styled.section`
	width: 450px;
	padding: 20px;
`

export default Wrapper
