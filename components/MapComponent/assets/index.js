const Images = {
	blueMarker: require("./images/marker.png"),
	blackMarker: require("./images/black-marker.png"),
	orangeMarker: require("./images/orange-marker.png"),
};
export { Images };

const ComponentAssets = {
	Images,
};
export default ComponentAssets;
