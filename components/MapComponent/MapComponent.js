import React from "react";
import Wrapper from "./MapComponent.style";
import { GoogleApiWrapper, Map, InfoWindow, Marker , Polyline} from "google-maps-react";
import { Images } from "./assets/index";

// ...

export class MapContainer extends React.Component {
	constructor(props){
		super(props);
	}
	state= {}

	componentDidMount(){
		console.log(this.props.polylineData)
	}
	onClick = (t, map, coord) => {
		const { latLng } = coord;
		const lat = latLng.lat();
		const lng = latLng.lng();
		this.props.onClickMap({latitude: lat, longitude: lng })
		console.log("latitude :" , lat , "longitude:", lng )
	}

	onClickMarker =(data)=>{
		console.log(data)
		// const { latLng } = coord;
		// const lat = latLng.lat();
		// const lng = latLng.lng();
		if(this.props.dashboard){
			this.setState({selectedMarker : data, showingInfoWindow: true})
			this.props.onClickMarker(data)
			return
		}
		this.setState({selectedMarker : data, showingInfoWindow: true})
		this.props.onClickMarker(data.position)
		// console.log("latitude :" , lat , "longitude:", lng )
	}

	// componentDidUpdate(){
	// 	this.setState({polylines : this.props.polylineData})
	// }
	render() {
		return (
			<Wrapper>
				<Map
					google={this.props.google}
					zoom={14}
					streetView={false}
					draggable={true}
					streetViewControl={false}
					initialCenter={this.props.initialCenter}
					style={{ ...this.props.style }}
					onClick={this.onClick}
					
				>
					{this.props.markers.map(marker =>{
						return (<Marker
									onClick={()=> this.onClickMarker(marker)}
									name={marker.name}
									position={marker.position}
									title="Location"
									icon={{
										url: marker.position.locationStatus == "empty" 
											? Images.blackMarker : marker.position.locationStatus == "assigned" 
											? Images.blueMarker : Images.orangeMarker,
										anchor: new google.maps.Point(10, 10),
										scaledSize: new google.maps.Size(20, 20),
									}}
								/>)
					})}
					{this.props.polylineData &&
						<Polyline
						path={this.props.polylineData}
						strokeColor="#0000FF"
						strokeOpacity={0.8}
						strokeWeight={3} />
					}
					 {/* <InfoWindow
						marker={this.state.selectedMarker}
						visible={this.state.showingInfoWindow}
						>
						<div className={{height : 100, width : 100}}>
							<p>Click on the map or drag the marker to select location where the incident occurred</p>
						</div>
					</InfoWindow> */}
					{/* <Marker
						name={"Current location"}
						position={{ lat: 37.778519, lng: -122.40564 }}
						icon={{
							url: Images.marker,
							anchor: new google.maps.Point(32, 32),
							scaledSize: new google.maps.Size(20, 20),
						}}
					/>
					<Marker
						name={"SOMA"}
						position={{ lat: 37.778519, lng: -122.40564 }}
						icon={{
							url: Images.marker,
							anchor: new google.maps.Point(32, 32),
							scaledSize: new google.maps.Size(30, 30),
						}}
					/>
					<Marker
						name={"Dolores park"}
						position={{ lat: 37.759703, lng: -122.428093 }}
						icon={{
							url: Images.marker,
							anchor: new google.maps.Point(32, 32),
							scaledSize: new google.maps.Size(30, 30),
						}}
					/>
					<Marker
						name={"Your position"}
						position={{ lat: 37.762391, lng: -122.439192 }}
						icon={{
							url: Images.marker,
							anchor: new google.maps.Point(32, 32),
							scaledSize: new google.maps.Size(30, 30),
						}}
					/> */}
				</Map>
			</Wrapper>
		);
	}
}

export default GoogleApiWrapper({
	apiKey: "AIzaSyArlFk-HOh3cE5xgZf2CoS7qlgcIPZMiY4",
})(MapContainer);
