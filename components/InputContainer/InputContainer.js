import React from "react";
import { Input, Typography } from "antd";

import Wrapper from "./InputContainer.style";

const { Title } = Typography;

const InputContainer = (props) => {
	const { value, title, onChange, disabled, placeholder, hideInput } = props;
	return (
		<Wrapper>
			<Title level={5} className="title">
				{title}
			</Title>
			{!hideInput &&( title === "password" ? (
				<Input.Password
					disabled={disabled}
					className="inputBox1"
					value={value}
					onChange={onChange}
				/>
			) : (
				<Input
					disabled={disabled}
					className="inputBox"
					value={value}
					onChange={onChange}
					placeholder={placeholder}
				/>
			))}
		</Wrapper>
	);
};

export default InputContainer;
