import styled from "styled-components";
import Themes from "../../themes/themes";

const Wrapper = styled.section`
	.title {
		margin-top: 20px;
	}
	.inputBox {
		border-radius: 5px;
		background-color: ${Themes.inputBackground};
		border: none;
		height: 40px;
	}
	.inputBox1{
		border-radius: 5px;
		background-color: ${Themes.inputBackground};
		border: none;
		height: 40px;
		width: 98%;
	}
`;

export default Wrapper;
