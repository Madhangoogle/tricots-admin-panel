// services/reportGenerator.js

import jsPDF from "jspdf";
import "jspdf-autotable";

import { format } from "date-fns";


const LocationsummaryGenerator = data => {

  const doc = new jsPDF();

  const tableColumn = ["Date", "Time", "Staff Name", "Status", ];
  
  const tableRows = [];


  data.forEach(ticket => {
    var staffName = ticket.staffId.firstName +" "+ticket.staffId.lastName 
    const ticketData = [
      ticket.completedAt.slice(0,10),
      ticket.completedAt.slice(11,16),
      staffName,
      ticket.status,
    //   format(new Date(ticket.updated_at), "yyyy-MM-dd")
    ];
    tableRows.push(ticketData);
  });


  doc.autoTable(tableColumn, tableRows, { startY: 20 });
  const date = Date().split(" ");
  const dateStr = date[0] + date[1] + date[2] + date[3] + date[4];
  doc.text("Location History", 14, 15);
  doc.save(`report_${dateStr}.pdf`);
};

export default LocationsummaryGenerator;